package com.company;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите с.с.: ");
        int ns = in.nextInt();

        System.out.print("Введите число 1: ");
        String firstNum = in.next();

        System.out.print("Введите число 2: ");
        String secondNum = in.next();

        System.out.print("Введите знак операции [+,-,*,/] : ");
        char action = in.next().charAt(0);

        Calc help = new Calc(ns, firstNum, secondNum, action);
        System.out.print(help.calc());
    }
}