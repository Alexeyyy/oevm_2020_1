package com.company;

import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConvertCode {
    private String string;
    private String blockOfVars;
    private String blockOfCods;
    private Writer writer;

    private LinkedList<String> varStr = new LinkedList<>();
    private LinkedList<String> codeStr = new LinkedList<>();
    private LinkedList<String> vars = new LinkedList<>();

    public ConvertCode(Writer writer) throws IOException {
        this.string = read();
        this.writer = writer;
        analysisPartheBody();
        analysisVarsStr();
        analysisVarsInt();
        analysisCodsStr();
        writer.setVariables(vars);
        createCode();
    }

    private void analysisVarsStr() {
        Pattern patternForBlocOfVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherForBlocOfVariables = patternForBlocOfVariables.matcher(blockOfVars);

        while (matcherForBlocOfVariables.find()) {
            varStr.add(blockOfVars.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end()));
        }
    }

    private void analysisVarsInt() {
        Pattern patternForBlocOfVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherForBlocOfVariables;
        String var;

        for (String string : varStr) {

            matcherForBlocOfVariables = patternForBlocOfVariables.matcher(string);

            while (matcherForBlocOfVariables.find()) {
                var = string.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end());
                if (!var.equals("integer")) {
                    vars.add(var);
                }
            }
        }
        writer.setVariables(vars);
    }

    public void analysisPartheBody() {
        Pattern patternForBlocOfVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherForBlocOfVariables = patternForBlocOfVariables.matcher(string);

        while (matcherForBlocOfVariables.find()) {
            blockOfVars = string.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end());
        }

        Pattern patternForBlocOfCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherForBlocOfCode = patternForBlocOfCode.matcher(string);

        while (matcherForBlocOfCode.find()) {
            blockOfCods = string.substring(matcherForBlocOfCode.start(), matcherForBlocOfCode.end());
        }

    }

    private void createCode() {
        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternReadLine = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternNumericOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");
        Pattern patternWriteLine = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        for (String string : codeStr) {

            if (string.matches(patternWrite.toString())) {
                Matcher matcherForCode = patternWrite.matcher(string);

                if (matcherForCode.find()) {
                    writer.addWrite(matcherForCode.group(1));
                    continue;
                }
            } else if (string.matches(patternReadLine.toString())) {
                Matcher matcherForCode = patternReadLine.matcher(string);

                if (matcherForCode.find()) {
                    writer.addReadLine(matcherForCode.group(1));
                    continue;
                }
            } else if (string.matches(patternWriteLine.toString())) {
                Matcher matcherForCode = patternWriteLine.matcher(string);

                if (matcherForCode.find()) {
                    writer.addWriteLine(matcherForCode.group(1));
                    continue;
                }
            } else {
                Matcher matcherForCode = patternNumericOperation.matcher(string);

                if (matcherForCode.find()) {
                    writer.addNumericOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                    continue;
                }
            }
        }
    }

    public String read() throws IOException {
        StringBuilder str = new StringBuilder();

        FileReader reader = new FileReader("PascalCode.pas");
        int c;
        while ((c = reader.read()) != -1) {
            str.append((char) c);
        }
        str.toString().replaceAll(";", ";\n");
        return str.toString();
    }

    private void analysisCodsStr() {

        Pattern patternForStrings = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherForStrings = patternForStrings.matcher(blockOfCods);

        while (matcherForStrings.find()) {
            codeStr.add(blockOfCods.substring(matcherForStrings.start(), matcherForStrings.end()));
        }
        createCode();
    }
}

