import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {

    public static final int LENGTH = 4;

    public static final int ROWS = (int) Math.pow(2, LENGTH);

    public static int[][] matrix = new int[ROWS + 1][LENGTH + 1];

    private boolean KNF;
    private boolean DNF;

    public void setDNF() {
        DNF = true;
        KNF = false;
    }

    public void setKNF() {
        KNF = true;
        DNF = false;
    }

    public int [][] getMatrix() {
        return matrix;
    }

    public void paint(Graphics g) {
        int position = 10;
        int size = 30;

        super.paint(g);

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < LENGTH + 1; j++) {

                g.setColor(Color.WHITE);

                if (KNF) {
                    if (matrix[i][LENGTH] == 0) {
                        g.setColor(Color.YELLOW);
                    }
                }
                if (DNF) {
                    if (matrix[i][LENGTH] == 1) {
                        g.setColor(Color.PINK);
                    }
                }

                g.fillRect(position + j * size, position + i * size, size, size);

                g.setColor(Color.BLACK);
                g.drawString(matrix[i][j] + "",position  * 2 + j * size,position * 3  + i * size);
            }
        }
    }

    public void createMatrix() {
        DNF = false;
        KNF = false;

        int num, countRevers;

        for (int i = 0; i < ROWS; i++) {

            matrix[i][LENGTH] = (int) (Math.random() * 2);

            for (int j = LENGTH - 1; j >= 0; j--) {
                countRevers = LENGTH - 1 - j;
                num = (i / (int) Math.pow(2, j)) % 2;
                matrix[i][countRevers] = num;
            }
        }
    }
}
