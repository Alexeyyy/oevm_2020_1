//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Введите изначльную систему счисления: ");
        Scanner scanner = new Scanner(System.in);
        int startSystem = scanner.nextInt();
        System.out.print("Введите конечную систему счисления: ");
        Scanner scan = new Scanner(System.in);
        int lastSystem = scan.nextInt();
        System.out.print("Введите число для перевода: ");
        Scanner s = new Scanner(System.in);
        char[] number = s.nextLine().toCharArray();
        long decimalNotation = convertTo10(number, startSystem);
        System.out.println(ConvertToNotation(decimalNotation, lastSystem));
    }

    public static long convertTo10(char[] number, int startSystem) {
        long number10 = 0;

        for(int i = 0; i < number.length; ++i) {
            int сurDigit = 1;
            if (number[i] >= '0' && number[i] <= '9') {
                сurDigit = number[i] - '0';
            } else if (Character.isLetter(number[i])) {
                сurDigit = 10 + number[i] - 'A';
            }

            number10 = (long)((double)number10 + (double)сurDigit * Math.pow((double)startSystem, (double)(number.length - i - 1)));
        }

        return number10;
    }

    public static String ConvertToNotation(long number10, int lastSystem) {
        String result = "";
        long remainder = 1;

        for(String сurChar = ""; number10 > 0; number10 /= (long)lastSystem) {
            remainder = number10 % (long)lastSystem;
            сurChar = "";
            if (number10 % (long)lastSystem < 10) {
                сurChar = Long.toString(remainder);
            } else {
                сurChar = сurChar + (char)((int)('A' + remainder - 10));
            }

            result = сurChar + result;
        }

        return result;
    }
}
