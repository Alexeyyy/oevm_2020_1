package com.company;
import javax.swing.*;
import java.awt.*;

public class Table extends JTable {
    private int[][] array;
    private boolean KNF = false;
    private boolean DNF = false;

    public void setArray(int[][] arr) {
        this.array = arr;
    }
    public void paint(Graphics g) {
        super.paint(g);
        int stringSize = 16;
        for (int i = 0; i < stringSize; i++) {
            int columnSize = 5;
            for (int j = 0; j < columnSize; j++) {
                g.setColor(Color.BLACK);
                if(KNF){
                    if (array[i][columnSize -1] == 0){
                        g.setColor(Color.RED);
                    }
                }
                if(DNF) {
                    if (array[i][columnSize - 1] == 1) {
                        g.setColor(Color.RED);
                    }
                }
                int sizeSquare = 30;
                int bounce = 10;
                g.drawString(array[i][j] + "", bounce * 2 + j * sizeSquare, bounce * 3  + i * sizeSquare);
                g.setColor(Color.BLACK);
                int indent = 10;
                g.drawRect(indent + j * sizeSquare, indent + i * sizeSquare, sizeSquare, sizeSquare);
            }
        }
    }
    public void setDNF(){
        DNF = true;
        KNF = false;
    }
    public void setKNF(){
        KNF = true;
        DNF = false;
    }
}