package com.company;
import javax.swing.*;
import java.util.Random;

public class Frame {
    private Random random = new Random();
    JFrame frame;
    Table myTable;
    private JTextArea reducedFunction = new JTextArea();
    private int stringSize = 16;
    private int columnSize = 5;
    private int[][] array = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)}
    };
    Frame(){
        frame = new JFrame();
        frame.setBounds(100, 100, 680, 535);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(null);

        myTable = new Table();
        myTable.setArray(array);
        myTable.setBounds(0, 0, 180, 500);
        frame.getContentPane().add(myTable);

        JButton buttonMadeArray = new JButton("Добавить массив в таблицу");
        buttonMadeArray.addActionListener(e -> madeNewArray());
        buttonMadeArray.setBounds(185, 10, 250, 90);
        frame.getContentPane().add(buttonMadeArray);

        JButton buttonDNF = new JButton("ДНФ");
        buttonDNF.addActionListener(e -> drawDNF());
        buttonDNF.setBounds(325, 390, 100, 100);
        frame.getContentPane().add(buttonDNF);

        JButton buttonKNF = new JButton("КНФ");
        buttonKNF.addActionListener(e -> drawKNF());
        buttonKNF.setBounds(185, 390, 100, 100);
        frame.getContentPane().add(buttonKNF);

        reducedFunction.setBounds(450, 10, 200, 480);
        frame.getContentPane().add(reducedFunction);
    }

    private void madeNewArray(){
        array = new int[][] {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)}
        };
        myTable.setArray(array);
        frame.repaint();
    }
    private void drawKNF() {
        myTable.setKNF();
        myTable.repaint();
        reducedFunction.setText(BoolF.KNF(array, stringSize));
    }
    private void drawDNF() {
        myTable.setDNF();
        myTable.repaint();
        reducedFunction.setText(BoolF.DNF(array, stringSize));
    }
}