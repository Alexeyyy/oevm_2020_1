package My_Func;

public class Program {
    private static int X1=0, X2=1, X3=2, X4=3;
    /**
     *This method is used to make table
     */
    public static void Func (int [][] Table, int M, int N){
        for(int i = 0; i < N; i++){
            for( int j = 0; j < M; j++){
                if(j == 4){
                    System.out.printf(" ");
                }
                System.out.printf(String.valueOf(Table[i][j]));
            }
            System.out.printf("\n");
        }
    }
    /**
     *This method is used to display the function in DNF
     */
    public static String DNF(int[][] Table, int M, int N)
    {
        String result="";
        int counter = 0;
        for (int i = 0; i < N; i++) {
            if (Table[i][M - 1] == 1){
                if(counter > 0){ result += "+"; }
                if(Table[i][X1] == 0){ result += "(!X1"; }
                else { result += "(X1"; }
                if(Table[i][X2] == 0){ result += "*!X2"; }
                else { result += "*X2"; }
                if(Table[i][X3] == 0){ result += "*!X3"; }
                else { result += "*X3"; }
                if(Table[i][X4] == 0){ result += "*!X4)"; }
                else { result += "*X4)"; }
                counter++;
            }
        }
        System.out.print("DNF = "+result);
        return result;
    }
    /**
     *This method  is used to display the function in CNF
     */
    public static String CNF(int[][] Table, int M, int N)
    {
        String result="";
        int counter = 0;
        for (int i = 0; i < N; i++) {
            if (Table[i][M - 1] == 0){
                if(counter > 0){ result += "*"; }
                if(Table[i][X1] == 1){ result += "(!X1"; }
                else { result +=  "(X1"; }
                if(Table[i][X2] == 1){ result += "+!X2"; }
                else { result += "+X2"; }
                if(Table[i][X3] == 1){ result += "+!X3"; }
                else { result += "+X3"; }
                if(Table[i][X4] == 1){ result += "+!X4)"; }
                else { result += "+X4)"; }
                counter++;
            }
        }
        System.out.print("CNF = "+result);
        return result;
    }
}
