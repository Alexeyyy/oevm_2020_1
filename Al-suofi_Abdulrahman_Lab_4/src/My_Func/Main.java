package My_Func;
import java.util.Scanner;
import java.util.Random;

public class Main {
    public static boolean run() {
        Random random= new Random();
        Scanner scan = new Scanner(System.in);
        System.out.print("|==================================| \n");
        System.out.print("|* 1 - Press to displays the DNF. *| \n"+
                         "|* 2 - Press to displays the CNF. *| \n");
        System.out.print("|==================================| \n");
        System.out.print("Enter the number: ");
        int operation = scan.nextInt();
        int M=5, N=16;
        int[][] table = {
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)},
        };
        if(operation ==1) {
            System.out.print("ABCD F\n");
            Program.Func(table, M, N);
            System.out.print("\n");
            System.out.println(Program.DNF(table, M, N));
        }
        else if(operation ==2) {
            System.out.print("ABCD F\n");
            Program.Func(table, M, N);
            System.out.print("\n");
            System.out.println(Program.CNF(table, M, N));
        }
        else { System.out.println("You did not choose operation"); }

        System.out.print("\n");
        System.out.println();
        return true;
    }
    public static void main (String string[]) {
        Scanner scan = new Scanner(System.in);
        int back;
        do{
            System.out.print("|========================|\n");
            System.out.print("|+++++++++ Back +++++++++|\n");
            System.out.print("|========================|\n");
            System.out.print("|*  1 - Press to run    *|\n|*  2 - Press to exit   *|\n");
            System.out.print("|========================|\n");
            System.out.print("Enter the number: ");
            back =scan.nextInt();
            System.out.println();
            if ( back ==1){
                run();
            }
            else if(back == 2){
                System.exit(1);
            }
        }
        while (back !=2);
    }
}
