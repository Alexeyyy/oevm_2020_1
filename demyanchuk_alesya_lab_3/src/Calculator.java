public class Calculator {

    private String action;
    private String numberOne;
    private String numberTwo;

    public Calculator(String numberOne, String numberTwo, String action) {
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
        this.action = action;
    }

    /*получение нового числа*/
    String returnResult(int sourceSystem) {
        String newNumber1 = "";
        String newNumber2 = "";
        String newRez = "";
        if (sourceSystem != 2) {
            newNumber1 += ChangeNumber.transferToTwoDegree( sourceSystem, numberOne );
            newNumber2 += ChangeNumber.transferToTwoDegree( sourceSystem, numberTwo );
            newRez += Score( newNumber1, newNumber2, action );
        } else newRez += Score( numberOne, numberTwo, action );
        return newRez;
    }

    private int[] numberConvertToInt(String number, int length) {
        String[] number1 = number.split( "" );
        int[] numb = new int[length];
        int count = number1.length;
        for (int i = length - 1; i >= 0; i--) {
            if (count > 0) {
                count--;
                for (int j = count; j > count - 1; j--) {
                    numb[i] = Integer.parseInt( number1[j] );
                }
            }
        }
        return numb;
    }

    private int compareNumbers(int length1, int length2) {
        if (length1 > length2) {
            return length1;
        } else return length2;
    }

    private static String checkToZero(String result) {
        String[] check = result.split( "" );
        String rezNew = "";
        int i = 0;
        while (check[i].equals( "0" )) {
            if (i == check.length - 1) {
                rezNew = "0";
                return rezNew;
            }
            i++;
        }
        for (int j = i; j < check.length; j++) {
            rezNew += check[j];
        }
        return rezNew;
    }


    private String getResultOfAddition(int lengthNumb, int[] numOne, int[] numTwo, String result) {
        for (int i = lengthNumb - 1; i >= 0; i--) {
            int c = numOne[i] + numTwo[i];
            if (c > 1) {
                numOne[i] = numOne[i] % 2;
                if (i != 0) {
                    numOne[i - 1] += c / 2;
                }
            }
            result += c % 2;
            if (i == 0 && c > 1) {
                result += "1";
            }
        }
        result = new StringBuilder( result ).reverse().toString();
        return result;
    }

    private String getResultOfMinus(int lengthNumb, int[] numOne, int[] numTwo, String result) {
        for (int i = lengthNumb - 1; i >= 0; i--) {
            int c = numOne[i] - numTwo[i];
            if (c < 0) {
                int j = i;
                while (numOne[j] != 1 && j != 0) {
                    j--;
                }
                numOne[j]--;
                for (int k = j + 1; k < i; k++) {
                    numOne[k] = 1;
                }
                numOne[i] = 2;
                c = numOne[i] - numTwo[i];
            }
            result += c % 2;
        }
        result = new StringBuilder( result ).reverse().toString();
        result = checkToZero( result );
        return result;
    }

    private String getResultOfMultiply(int lengthNumb, int[] numOne, int[] numTwo, String result) {
        int longNums = numOne.length;
        int[][] forMultiply = new int[longNums][longNums * 2];
        //заполняем массив перемноженными слагаемыми как в столбик
        for (int i = 0; i < longNums; i++) {
            if (numTwo[longNums - i - 1] == 1) {
                int count = longNums - 1;
                for (int j = longNums * 2 - i - 1; j >= longNums - i; j--) {
                    if (count > -1) {
                        forMultiply[i][j] = numOne[count];
                        count--;
                    }
                }
            }
            System.out.print( "\n" );
        }
        //заводим новый массив под итоговое число и впоследствии результат
        int[] resultNum = new int[longNums * 2];
        for (int j = longNums * 2 - 1; j >= 0; j--) {
            for (int i = 0; i < longNums; i++) {
                resultNum[j] += forMultiply[i][j];
            }
            //проверяем если сумма столбика больше 2 то складываем по правилам
            int c = resultNum[j];
            if (resultNum[j] > 1 && j != 0) {
                resultNum[j] = c % 2;
                resultNum[j - 1] += c / 2;
            }
            result += resultNum[j];
            if (j == 0 && c > 1) {
                result += 1;
            }
        }
        result = new StringBuilder( result ).reverse().toString();
        result = checkToZero( result );
        return result;
    }

    private String getResultOfDivision(String str1, String str2) {
        String comparison = str2;
        String resultDiv = "0";

        if (str2.toCharArray().length == 1 && str2.toCharArray()[0] != '1') {
            return "Error!";
        }
        while (compareTheNumbersDivision( str1, comparison )) {
            resultDiv = addNumbForDivision( resultDiv, "1" );
            comparison = addNumbForDivision( comparison, str2 );
        }
        resultDiv = new StringBuilder( resultDiv ).reverse().toString();
        resultDiv = checkToZero( resultDiv );
        return resultDiv;

    }

    private String addNumbForDivision(String str1, String str2) {
        char[] firstDigitArray = str1.toCharArray();
        char[] secondDigitArray = str2.toCharArray();

        StringBuilder resultation = new StringBuilder();

        int transfer = 0;
        int i = 0;

        while (i < secondDigitArray.length) {
            boolean b = (firstDigitArray[i] == '0' && secondDigitArray[i] == '1') || (firstDigitArray[i] == '1' && secondDigitArray[i] == '0');
            if (transfer == 0) {
                if (firstDigitArray[i] == '0' && secondDigitArray[i] == '0') {
                    resultation.append( "0" );
                    transfer = 0;
                }
                if (b) {
                    resultation.append( "1" );
                    transfer = 0;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == '1') {
                    resultation.append( "0" );
                    transfer = 1;
                }
            } else {
                if (firstDigitArray[i] == '0' && secondDigitArray[i] == '0') {
                    resultation.append( "1" );
                    transfer = 0;
                }
                if (b) {
                    resultation.append( "0" );
                    transfer = 1;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == '1') {
                    resultation.append( "1" );
                    transfer = 1;
                }
            }
            i++;
        }

        while (i < firstDigitArray.length) {
            if (firstDigitArray[i] == '0' && transfer == 0) {
                resultation.append( "0" );
                transfer = 0;
            }
            if ((firstDigitArray[i] == '0' && transfer == 1) || (firstDigitArray[i] == '1' && transfer == 0)) {
                resultation.append( "1" );
                transfer = 0;
            }
            if (firstDigitArray[i] == '1' && transfer == 1) {
                resultation.append( "0" );
                transfer = 1;
            }
            i++;
        }

        if (transfer == 1) {
            resultation.append( "1" );
        }
        return resultation.toString();
    }

    private Boolean compareTheNumbersDivision(String str1, String str2) {
        char[] strArray1 = new StringBuilder( str1 ).reverse().toString().toCharArray();
        char[] strArray2 = new StringBuilder( str2 ).reverse().toString().toCharArray();

        if (strArray1.length > strArray2.length) {
            return true;
        }
        if (strArray1.length < strArray2.length) {
            return false;
        }
        if (strArray1.length == strArray2.length) {

            for (int i = 0; i < strArray1.length; i++) {

                if (strArray1[i] == '1' && strArray2[i] == '0') {
                    return true;
                }
                if (strArray1[i] == '0' && strArray2[i] == '1') {
                    return false;
                }
            }
        }
        return true;
    }

    private String Score(String numberOne, String numberTwo, String action) {
        int lengthNumb = compareNumbers( numberOne.length(), numberTwo.length() );
        int[] numOne = numberConvertToInt( numberOne, lengthNumb );
        int[] numTwo = numberConvertToInt( numberTwo, lengthNumb );
        String result = "";
        if (action.equals( "+" )) {
            result = getResultOfAddition( lengthNumb, numOne, numTwo, result );
        } else if (action.equals( "-" )) {
            if (numberOne.length() >= numberTwo.length())
                result = getResultOfMinus( lengthNumb, numOne, numTwo, result );
            else
                result = getResultOfMinus( lengthNumb, numTwo, numOne, result );
        } else if (action.equals( "*" )) {
            result = getResultOfMultiply( lengthNumb, numOne, numTwo, result );
        } else if (action.equals( "/" )) {
            result = getResultOfDivision( numberOne, numberTwo );
        }
        return result;
    }
}
