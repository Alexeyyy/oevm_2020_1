## Лабораторная работа №3
### Студент группы ПИбд-21
### Овсянников Кирилл

#### Техническое задание:
Разработать программный продукт для осуществления арифметических операций над двоичными числами. При запуске программы пользователь указывает:
* исходную систему счисления (от двоичной до шестнадцатеричной)
* два целых числа в исходной системе счисления целые
* арифметическую операцию

Все операции выполняются в двоичной системе счисления через сложение и необходимые преобразования. После расчетов программа выводит результат в двоичной системе счисления.

#### Пример работы программы:
| Входные данные | Выходные данные |
| :---: | :---: |
| 13 AA 12 * |100000110100|
| 12 10 8 -|10|
| 10 8 3 +|1011|
| 16 F13 AA / |10110|


##### Ссылка на видео: https://yadi.sk/i/iNSN5qOrA6ZzRg