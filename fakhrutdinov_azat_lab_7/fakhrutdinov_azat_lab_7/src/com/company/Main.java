package com.company;

public class Main {
    public static void main(String[] args) {
        Pascal PascalCode = new Pascal();
        ASMCode assemblerCode = new ASMCode();
        Trans Trans = new Trans(PascalCode.read(), assemblerCode);
        Trans.transBody();
        assemblerCode.create();
    }
}
