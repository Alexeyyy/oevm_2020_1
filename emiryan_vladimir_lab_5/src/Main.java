import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    private JFrame frame;
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                Main window = new Main();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public Main() {
        initialize();
    }

    private void initialize() {
        Minimize minimize = new Minimize();

        frame = new JFrame();
        frame.setBounds(100, 100, 603, 603);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel tablePanel = new TableOfTruePanel();
        tablePanel.setBounds(10, 11, 192, 542);
        frame.getContentPane().add(tablePanel);

        JPanel resultPanel = new ResultPanel(minimize);
        resultPanel.setPreferredSize(new Dimension(3920, 150));
        resultPanel.setOpaque(false);
        resultPanel.setLayout(null);

        JScrollPane spane = new JScrollPane();
        spane.setBounds(212, 45, 360, 100);
        spane.setOpaque(false);
        spane.getViewport().setOpaque(false);
        spane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        spane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        spane.setViewportView(resultPanel);
        frame.getContentPane().add(spane);
        JScrollPane scrollPane = new JScrollPane();

        JButton minimizeButton = new JButton("Minimize");
        minimizeButton.addActionListener((arg0) -> {
            minimize.fillTable();
            tablePanel.repaint();
        });
        minimizeButton.setBounds(212, 16, 89, 23);
        frame.getContentPane().add(minimizeButton);

        JButton dnfButton = new JButton("DNF");
        dnfButton.addActionListener((arg0) -> {
            minimize.makeDNF();
            resultPanel.repaint();
        });
        dnfButton.setBounds(306, 16, 89, 23);
        frame.getContentPane().add(dnfButton);

        JButton cnfButton = new JButton("CNF");
        cnfButton.addActionListener((arg0) -> {
            minimize.makeCNF();
            resultPanel.repaint();
        });
        cnfButton.setBounds(400, 16, 89, 23);
        frame.getContentPane().add(cnfButton);
    }
}
