package bolkunov_vladimir_lab4;

public class FunctionDefenition {
    private Boolean x1;

    public Boolean getX1() {
        return x1;
    }

    private Boolean x2;

    public Boolean getX2() {
        return x2;
    }

    private Boolean x3;

    public Boolean getX3() {
        return x3;
    }

    private Boolean x4;

    public Boolean getX4() {
        return x4;
    }

    private boolean f;

    public boolean getF() {
        return f;
    }

    public FunctionDefenition(Boolean x1, Boolean x2, Boolean x3, Boolean x4, boolean f) {
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
        this.f = f;
    }

    //GLUE
    public boolean canGlue(FunctionDefenition f) {
        return canGlue(x1, f.x1) &&
                canGlue(x2, f.x2) &&
                canGlue(x3, f.x3) &&
                canGlue(x4, f.x4);
    }

    private boolean canGlue(Boolean a, Boolean b) {
        if (a == null || b == null) {
            if (a == b) {
                return true;
            }
            return false;
        }
        return true;
    }

    public FunctionDefenition glue(FunctionDefenition f) {
        return new FunctionDefenition(glue(x1, f.x1), glue(x2, f.x2), glue(x3, f.x3), glue(x4, f.x4), this.f);
    }

    private Boolean glue(Boolean a, Boolean b) {
        if (a == null || b == null) {
            if (a == b) {
                return null;
            }
            throw new IllegalArgumentException("Пропущена проверка canGlue.");
        }
        if (a != b) {
            return null;
        } else {
            return a;
        }
    }

    //MERGE
    public boolean canMerge(FunctionDefenition f) {
        return canMerge(x1, f.x1) &&
                canMerge(x2, f.x2) &&
                canMerge(x3, f.x3) &&
                canMerge(x4, f.x4);
    }

    private boolean canMerge(Boolean a, Boolean b) {
        if (a != null && b != null) {
            if (a != b) {
                return false;
            }
        }
        return true;
    }

    public FunctionDefenition merge(FunctionDefenition f) {
        return new FunctionDefenition(merge(x1, f.x1), merge(x2, f.x2), merge(x3, f.x3), merge(x4, f.x4), this.f);
    }

    private Boolean merge(Boolean a, Boolean b) {
        return (a == null && b != null) || (b == null && a != null);
    }

    //HELPERS
    public boolean isEmpty() {
        return x1 == null && x2 == null && x3 == null && x4 == null;
    }

    public boolean equal(FunctionDefenition f) {
        return x1 == f.x1 && x2 == f.x2 && x3 == f.x3 && x4 == f.x4 && this.f == f.f;
    }

    public static boolean equal(FunctionDefenition f1, FunctionDefenition f2) {
        return f1.equal(f2);
    }

    private String booleanToNameString(Boolean bool, String name) {
        if (bool != null) {
            if (bool) {
                return name;
            } else {
                return '¬' + name;
            }
        } else {
            return "";
        }
    }

    public String toString(boolean DNF) {
        char c;
        if (DNF) {
            c = '∧';
        } else {
            c = '∨';
        }
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        sb.append(booleanToNameString(x1, "X1"));
        if (x1 != null && (x2 != null || x3 != null || x4 != null)) sb.append(c);
        sb.append(booleanToNameString(x2, "X2"));
        if (x2 != null && (x3 != null || x4 != null)) sb.append(c);
        sb.append(booleanToNameString(x3, "X3"));
        if (x3 != null && (x4 != null)) sb.append(c);
        sb.append(booleanToNameString(x4, "X4"));
        sb.append(')');
        return sb.toString();
    }
}
