import javax.swing.*;
import java.util.Random;

public class MyFrame {
    Reducer reducer = new Reducer();
    Random random= new Random();

    public static void main(String[] args) {
        MyFrame window = new MyFrame();
        window.frame.setVisible(true);
    }


    private JFrame frame;
    private MainPanel mainPanel;
    private ReduceFunctionPanel reduceFunctionPanel;

    private JButton buttonNewArray = new JButton("New array");
    private JButton buttonDNF = new JButton("DNF");
    private JButton buttonKNF = new JButton("KNF");
    private JTextArea reducedFunction = new JTextArea();

    private int sizeI = 16;
    private int sizeJ = 5;
    private int[][] array = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)},
    };

    MyFrame() {
        frame = new JFrame();
        frame.setBounds(100, 100, 700, 540);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        mainPanel = new MainPanel(sizeI,sizeJ);
        mainPanel.setNewArray(array);
        mainPanel.setBounds(0, 0, 180, 500);
        frame.getContentPane().add(mainPanel);

        reduceFunctionPanel = new ReduceFunctionPanel(sizeI,sizeJ);
        reduceFunctionPanel.setNewArray(array);
        reduceFunctionPanel.setBounds(340, 0, 180, 500);
        frame.getContentPane().add(reduceFunctionPanel);

        reducedFunction.setBounds(520, 10, 150, 480);
        frame.getContentPane().add(reducedFunction);

        buttonNewArray.addActionListener(e -> madeNewArray());
        buttonNewArray.setBounds(180, 10, 150, 30);
        frame.getContentPane().add(buttonNewArray);

        buttonDNF.addActionListener(e -> drawDNF());
        buttonDNF.setBounds(180, 50, 150, 30);
        frame.getContentPane().add(buttonDNF);

        buttonKNF.addActionListener(e -> drawKNF());
        buttonKNF.setBounds(180, 90, 150, 30);
        frame.getContentPane().add(buttonKNF);
    }

    private void madeNewArray() {
        int[][] newArray = {
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };
        array = newArray;
        reduceFunctionPanel.setNewArray(array);
        mainPanel.setNewArray(array);
        reducedFunction.setText("");
        mainPanel.repaint();
        reduceFunctionPanel.repaint();
    }

    private void drawKNF() {
        reducedFunction.setText(reducer.KNF(array,sizeI,sizeJ));
        reduceFunctionPanel.drawKNF();
        reduceFunctionPanel.repaint();
    }

    private void drawDNF() {
        reducedFunction.setText(reducer.DNF(array,sizeI,sizeJ));
        reduceFunctionPanel.drawDNF();
        reduceFunctionPanel.repaint();
    }
}
