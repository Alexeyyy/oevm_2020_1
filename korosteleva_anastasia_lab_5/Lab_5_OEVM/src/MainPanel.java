import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {

    private int[][] array;
    private int sizeI;
    private int sizeJ;
    private int indent = 10;
    private int sizeOfOneCell = 30;

    MainPanel(int sizeI, int sizeJ) {
        this.sizeI = sizeI;
        this.sizeJ = sizeJ;
    }

    public void paint(Graphics g) {
        super.paint(g);
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                g.drawRect(indent + j * sizeOfOneCell, indent + i * sizeOfOneCell, sizeOfOneCell, sizeOfOneCell);
                g.drawString(array[i][j] + "", indent * 2 + j * sizeOfOneCell, indent * 3 + i * sizeOfOneCell);
            }
        }
    }

    public void setNewArray(int[][] array) {
        this.array = array;
    }
}
