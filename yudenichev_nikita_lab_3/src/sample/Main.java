package sample;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Укажите исходную систему счисления (2-16): ");
        int initial = scanner.nextInt();
        System.out.print("Укажите первое число: ");
        char[] firstNumber = scanner.next().toCharArray();
        System.out.print("Укажите второе число: ");
        char[] secondNumber = scanner.next().toCharArray();
        System.out.print("Укажите арифметическую операцию (+, -, *, /): ");
        char arithmeticOperation = scanner.next().charAt(0);

        long firstNumberDec = Convertor.convertToDec(initial, firstNumber);
        long secondNumberDec = Convertor.convertToDec(initial, secondNumber);

        char[] firstNumberBinary = Convertor.convertToBinary(firstNumberDec).toCharArray();
        char[] secondNumberBinary = Convertor.convertToBinary(secondNumberDec).toCharArray();

        switch (arithmeticOperation) {
            case '+':
                System.out.print("Сумма: " + Operations.sumOfBinaryNum(firstNumberBinary, secondNumberBinary));
                break;
            case '-':
                System.out.print("Разность: " + Operations.differenceOfBinaryNum(firstNumberBinary, secondNumberBinary));
                break;
            case '*':
                System.out.print("Произведение: " + Operations.compositionOfBinaryNum(firstNumberBinary, secondNumberBinary));
                break;
            case '/':
                System.out.print("Частное: " + Operations.divisionOfBinaryNum(firstNumberBinary, secondNumberBinary));
                break;
            default:
                System.out.print("Введите другую арифмитическую операцию(+,-,*,/)!");
                break;
        }
    }
}

