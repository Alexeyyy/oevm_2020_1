package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] array = Helper.generateArray();

        System.out.println("Choose your operation\n DNF || KNF\n__________________");
        String result = scanner.next();

        try {
            if (result.equals("KNF")) {
                Helper.knf(array);
                for (int[] element : array) {
                    System.out.printf("%d %d %d %d = %d%n", element[0], element[1], element[2], element[3], element[4]);
                }
            } else if (result.equals("DNF")) {
                Helper.dnf(array);
                for (int[] element : array) {
                    System.out.printf("%d %d %d %d = %d%n", element[0], element[1], element[2], element[3], element[4]);
                }
            }
        } catch (Exception e) {
            System.out.println("Error");
        }
    }
}