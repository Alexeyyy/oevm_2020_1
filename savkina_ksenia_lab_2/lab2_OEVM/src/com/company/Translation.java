package com.company;

public class Translation {

    public boolean isNumOrgNS(long orgnlNS, String strNum) {
        int k = 0;
        for (int i = 0; i < strNum.length(); i++) {
            if (Character.isDigit(strNum.charAt(i))) {
                if ((strNum.charAt(i) - '0') < orgnlNS)
                    k++;
            } else {
                if (Character.isUpperCase(strNum.charAt(i))) {
                    if ((int) strNum.charAt(i) - 55 < orgnlNS)
                        k++;
                }
            }
            if (k == strNum.length())
                return true;
        }
        return false;
    }

    public long toNumber(long orgnlNS, String strNum) {
        if (orgnlNS != 10) {
            long num = 0;
            long k = 0;
            for (int i = (strNum.length() - 1); i >= 0; i--) {
                if (!Character.isDigit(strNum.charAt(i)))
                    num += ((long) (strNum.charAt(i) - 55)) * (long) Math.pow(orgnlNS, k);
                else
                    num += ((long) (strNum.charAt(i) - 48)) * (long) Math.pow(orgnlNS, k);
                k++;
            }
            return num;
        }
        return Integer.valueOf(strNum);
    }

    public String translation(long orgnlNS, long fntNS, String result) {
        long num = toNumber(orgnlNS, result);
        result = "";
        while (num != 0) {
            long k = num % fntNS;
            if (k >= 10) {
                char tmpNum = (char) (k + 55);
                result = tmpNum + result;
            } else {
                char tmpNum = (char) (k + 48);
                result = tmpNum + result;
            }
            num /= fntNS;
        }
        return result;
    }
}
