# Лабораторная работа №6

## ПИБД-21 Орлов Артем

Видео-пример работы программы
https://drive.google.com/file/d/1RRfCIQigb5ZYlKX9b569xkJfTh15C-Tg/view?usp=sharing

Консольная программа, написана на FASM

Пользователь, при запуске программы, должен ввести два числа X и Y.
По итогу работы программы, в консоль выводятся результаты арифметических операций над числами X и Y.

Пример работы программы:
```
X = 10
Y = -8
X + Y = 2
X - Y = 18
X * Y = -80
X / Y = -1
```