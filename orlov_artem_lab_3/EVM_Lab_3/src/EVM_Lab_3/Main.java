package EVM_Lab_3;

import java.util.*;

public class Main {

    public static String executeOperation(String operation, String num1, String num2) {
        switch (operation) {
            case "+":
                return Operations.performAddition(num1, num2);
            case "-":
                return Operations.performSubtaction(num1, num2);
            case "/":
                return Operations.performDivide(num1, num2);
            case "*":
                return Operations.performMultiply(num1, num2);
            default:
                return "";
        }
    }


    public static void main(String[] args) {
        byte not = 0;
        byte notNew = 2;
        String num1 = "";
        String num2 = "";
        String operation = "";
        not = Byte.parseByte(UserInput.getCorrectNotation(true));
        num1 = Converter.convertToNewNotation(not, notNew, UserInput.getCorrectNum(not));
        num2 = Converter.convertToNewNotation(not, notNew, UserInput.getCorrectNum(not));
        operation = UserInput.getCorrectOperation();
        System.out.println("Результат: " + executeOperation(operation, num1, num2));

    }
}
