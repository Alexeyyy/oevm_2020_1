package EVm_lab_2;

public class Function {

    static int lengthI = 16;

    static int lengthJ = 5;

    static int LastTableColumn = 3;

    static int FColumn = 4;

    public static void printTable(int[][] table) {

        for (int i = 0; i < lengthI; i++) {
            for (int j = 0; j < lengthJ; j++) {

                if (j == lengthJ - 1) {
                    System.out.print("\t");
                }

                System.out.print("\t" + table[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

    public static String DNF(int[][] table) {

        int count = 0;

        StringBuilder strBuilder = new StringBuilder();

        for (int ind = 0; ind < lengthI; ind++) {

            if (table[ind][FColumn] == 1) {
                count++;

                if (count > 1) {
                    strBuilder.append(" + ");
                }
                strBuilder.append("(");

                for (int j = 0; j <= LastTableColumn; j++) {
                    if (j != LastTableColumn) {

                        if (table[ind][j] == 1) {
                            strBuilder.append("X").append(j + 1).append(" * ");
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")").append(" * ");
                        }

                    } else {

                        if (table[ind][j] == 1) {
                            strBuilder.append("X").append(j + 1);
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")");
                        }

                    }
                }
                strBuilder.append(")");
            }
        }
        return strBuilder.toString();
    }

    public static String KNF(int[][] table) {

        int count = 0;

        StringBuilder strBuilder = new StringBuilder();

        for (int ind = 0; ind < lengthI; ind++)

            if (table[ind][FColumn] == 0) {
                count++;

                if (count > 1) {
                    strBuilder.append(" * ");
                }

                strBuilder.append("(");

                for (int j = 0; j <= LastTableColumn; j++) {

                    if (j != LastTableColumn) {

                        if (table[ind][j] == 0) {
                            strBuilder.append("X").append(j + 1).append(" + ");
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")").append(" + ");
                        }

                    } else {

                        if (table[ind][j] == 0) {
                            strBuilder.append("X").append(j + 1);
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")");
                        }

                    }
                }
                strBuilder.append(")");
            }
        return strBuilder.toString();
    }
}
