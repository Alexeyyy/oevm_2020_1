package EVm_lab_2;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Random random = new Random();

        Scanner input = new Scanner(System.in);

        System.out.println("    X1  X2  X3  X4      F");

        int[][] table = {
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };

        Function.printTable(table);

        System.out.print("ДНФ - введите 1\t КНФ - введите 0 \n");

        if (input.nextInt() == 1) {
            System.out.print(Function.DNF(table));
        } else {
            System.out.print(Function.KNF(table));
        }
    }
}
