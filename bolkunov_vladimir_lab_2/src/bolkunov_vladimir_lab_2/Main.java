package bolkunov_vladimir_lab_2;

import java.util.Scanner;

public class Main
{
    private static final int minNumSys = 2;
    private static final int maxNumSys = 16;

    public static void main(String[] args)
    {
        int repeat;
        do
        {
            repeat = loop();
        }
        while(repeat == 0);
    }

    static int loop()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите исходную систему счисления, конечную сисетму счисления и затем число:");
        int start = scanner.nextInt();
        int end = scanner.nextInt();
        String num = scanner.next();

        if(start < minNumSys || start > maxNumSys || end < minNumSys || end > maxNumSys)
        {
            System.out.println("Неверные входные данные.");
            return 0;
        }
        for (int i = 0; i < num.length(); i++)
        {
            if(charToNum(num.charAt(i)) >= start)
            {
                System.out.println("Неверные входные данные.");
                return 0;
            }
        }

        String result = "";
        if(start == end)
            result = num;
        else
        {
            int res10 = 0;
            for(int i = 0; i < num.length(); i++)
            {
                res10 += charToNum(num.charAt(i))*Math.pow(start,num.length()-1-i);
            }
            StringBuffer sb = new StringBuffer();
            while(res10 > 0)
            {
                sb.append(numToChar(res10%end));
                res10 /= end;
            }
            result = sb.reverse().toString();
        }
        System.out.println("Ваш результат = "+result);
        System.out.println("Введите '0' чтобы перевести еще одно число, или любое другое число для выхода");
        return scanner.nextInt();
    }

    private static int charToNum(char c)
    {
        switch (c)
        {
            case '0': return 0;
            case '1': return 1;
            case '2': return 2;
            case '3': return 3;
            case '4': return 4;
            case '5': return 5;
            case '6': return 6;
            case '7': return 7;
            case '8': return 8;
            case '9': return 9;
            case 'A': return 10;
            case 'B': return 11;
            case 'C': return 12;
            case 'D': return 13;
            case 'E': return 14;
            case 'F': return 15;
            default: return 0;
        }
    }

    private static char numToChar(int n)
    {
        switch (n)
        {
            case 0: return '0' ;
            case 1: return '1' ;
            case 2: return '2' ;
            case 3: return '3' ;
            case 4: return '4' ;
            case 5: return '5' ;
            case 6: return '6' ;
            case 7: return '7' ;
            case 8: return '8' ;
            case 9: return '9' ;
            case 10: return 'A' ;
            case 11: return 'B' ;
            case 12: return 'C' ;
            case 13: return 'D' ;
            case 14: return 'E' ;
            case 15: return 'F' ;
            default: return 0;
        }
    }
}
