import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		// The original number system
		System.out.print("Enter the original number system : ");
		int NowO = in.nextInt();
		in.nextLine();
		// Numbers to be calculated on
		System.out.print("Enter the number : ");
		String numbs = in.nextLine();
		String numbOne = numbs.split(" ")[0];
		String numbTwo = numbs.split(" ")[1];
		// Arithmetic operation
		System.out.print("Enter the operation : ");
		String operation = in.nextLine();
		System.out.print(Helper.calculation(Helper.toTwo(Helper.toTen(numbOne, NowO)),
				Helper.toTwo(Helper.toTen(numbTwo, NowO)), operation));
	}
}
