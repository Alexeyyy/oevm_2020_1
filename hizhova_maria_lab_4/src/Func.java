public class Func {

    private String func = "";

    public Func(String func) {
        this.func = func;
    }

    private String[] dev() {
        String[] ar = new String[8];
        String substr = "";
        boolean flag = false;
        int k = 0;
        for (int i = 0; i < func.length(); i++) {
            if (func.charAt(i) == '(') {
                flag = true;
            }
            if (func.charAt(i) == ')') {
                flag = false;
                ar[k] = substr;
                substr = "";
                k++;
            }
            if (flag && func.charAt(i) != ')' && func.charAt(i) != '(') {
                substr = substr + func.substring(i, i + 1);
            }
        }
        return ar;
    }
}