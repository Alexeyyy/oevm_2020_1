import java.util.Random;

public class Func {

    public static Random random = new Random();
    private static char RandomArray[]=new char[16];
    private static char[][] TruthMatrix =new char[16][5];
    private static String [] True={"X4","X3","X2","X1"};



    /**
     *This method is used to make the truth table and random values
     */

    public static void TruthTable(){

        /**
         *This function is used to fill the truth table with values
         */


        int n = 4;
        int rows = (int) Math.pow(2,n);
        String OneZero = "01";
        for (int i=0; i<rows; i++) {
            for (int j=n-1; j>=0; j--) {
                TruthMatrix[i][j]= (char) (i/(int) Math.pow(2, j)%2 +48);
            }
        }

        /**
         *This function is used to print the truth table and  random array
         */

        System.out.println("|  X1 X2 X3 X4  | f |");
        System.out.println("--------------------");

        for(int i = 0;i < 16; i++){
            System.out.print("|");
            for(int j = 4; j >= 0;j--){
                RandomArray[i]=OneZero.charAt(random.nextInt(OneZero.length()));
                System.out.print(TruthMatrix[i][j]+"  " );
            }
            System.out.print(" | "+ RandomArray[i]+" |\n");
        }
        System.out.println("---------------------");

        System.out.println();


    }

    /**
     *This method  is used to display the function in DNF
     */
    public static void DNF(){

        StringBuilder results = new StringBuilder();
        /**
         *This function  is used to find last '1' to put mathematical sign "∨"
         */

        int counter=0;

        for(int i = 0; i < RandomArray.length; i++){
            if(RandomArray[i] == '1'){
                counter=i;
            }
        }

        for(int i = 0;i < 16; i++){
            if(RandomArray[i]=='1') {
                results.append("( ");
                for (int j = 4; j >= 0; j--) {

                    if (TruthMatrix[i][j] == '0') {
                        results.append("!").append(String.valueOf(True[j]));
                    } else if (TruthMatrix[i][j] == '1') {
                        results.append(String.valueOf(True[j]));
                    }
                    if(j >0 && j<4){results.append(" ");}
                }
                results.append(" )");
                if(i >= 0&&i < counter)results.append("∨");
            }
        }
        System.out.print("[ DNF ]--> F(X1,X2,X3,X4)="+results+"\n");
    }

    /**
     *This method  is used to display the function in CNF
     */

    public static void CNF(){

        StringBuilder results = new StringBuilder();
        for(int i = 0;i < 16; i++){
            if(RandomArray[i]=='0') {
                results.append("( ");
                for (int j = 4; j >= 0; j--) {

                    if (TruthMatrix[i][j] == '1') {
                        results.append("!").append(String.valueOf(True[j]));
                    } else if (TruthMatrix[i][j] == '0') {
                        results.append(String.valueOf(True[j]));
                    }
                    if(j >0 && j<4){results.append(" ∨ ");}
                }
                results.append(" )");
            }
        }
        System.out.print("[ CNF ]--> F(X1,X2,X3,X4)="+results+"\n");
    }

}
