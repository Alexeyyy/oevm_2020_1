
import java.util.Scanner;

public class Main {

    Func func =new Func();

    public static boolean show(){

        Scanner scan = new Scanner(System.in);
        System.out.print("//  [1]Press to displays the function in the DNF \n//  [2]Press to displays the function in the CNF \n//  [3]Press to displays the function in the DNF and CNF \n(@user)-->") ;

        int action =scan.nextInt();

        if(action==1) {
            Func.TruthTable();
            Func.DNF();

        }
        else if(action==2) {
            Func.TruthTable();
            Func.CNF();
        }
        else if(action==3) {
            Func.TruthTable();
            Func.DNF();
            Func.CNF();
        }
        else {
            System.out.println("You did not choose operation");
        }


        System.out.print("\n");


        System.out.println();

        return true;

    }

    public static void main (String string[]){

        Scanner scan = new Scanner(System.in);
        int call;

        do{
            System.out.print("\n//==========( Menu )============//\n") ;
            System.out.print("//  [1]Press to start\n//  [2]Press to close \n") ;
            System.out.print(  "//==============================//\n(@user)--> ");
            call =scan.nextInt();
            if ( call ==1){  show();    }
            else if(call == 2){System.exit(1);}
            else{System.out.print("(@user)-->\n"); }

        }while (call !=2);

    }

}
