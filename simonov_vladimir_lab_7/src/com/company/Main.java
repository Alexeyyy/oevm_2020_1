package com.company;

public class Main {
    public static void main(String[] args){
        Pascal pascal = new Pascal("test1.pas");
        Assembler assembler = new Assembler("testassem1");
        Translator translator = new Translator(pascal.getPascalCode(), assembler);
        translator.startTranslate();
        assembler.createFile();
    }
}
