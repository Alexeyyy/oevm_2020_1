package EVM_Lab_5;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;


public class Controller {

    @FXML
    private TextArea textAreaTable;

    @FXML
    private TextArea textAreaOutput;

    static final int COLUMN = 16;

    static final int SYMBOL = 5;

    private int[][] table = new int[COLUMN][SYMBOL];

    private boolean canWork = false;

    @FXML
    void buttonGenerateClick() {
        textAreaTable.setText(Operations.printTable(table));
        canWork = true;
    }

    @FXML
    void buttonGetDNFClick() {
        if (canWork) {
            textAreaOutput.setText(Operations.getDNF(table));
        }
    }

    @FXML
    void setButtonGetKNFClick() {
        if (canWork) {
            textAreaOutput.setText(Operations.getKNF(table));
        }
    }

    @FXML
    void initialize() {


    }
}
