﻿# Лабораторная работа 5
## ИСЭбд-21
## Зиновьев Степан

## **[URL видео.](https://drive.google.com/file/d/1E3pgFZykSmcf9yHw2cru4T_bMofVdSdp/view?usp=sharing)**

## Задание: разработать программный продукт для визуализации булевых функций с простейших логических элементов. В качестве входных данных могут выступать результаты работы программы, разработанной в рамках 4-ой лабораторной работы.

## Пример работы:
##**[Картинка 1](https://drive.google.com/file/d/1PEFaIdsPqtoP2-OJrWnABj2478TaAOOT/view?usp=sharing)**
##**[Картинка 2](https://drive.google.com/file/d/15xx5bYfdvqrzcShD1MJzlCxmY7SsVKMS/view?usp=sharing)**

## Спасибо за внимание.