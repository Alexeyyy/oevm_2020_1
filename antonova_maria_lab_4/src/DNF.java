package com.company;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

public class DNF {
    private int [][] _truthTable;
    private int _tLength;
    private int _tWidth;
    int slagaemye = 0;

    public DNF (TruthTable table) {
        _truthTable = table._truthTable;
        _tLength = table._tLength;
        _tWidth = table._tWidth;
    }

    public void printResult (){
        System.out.print(formResult1());
        System.out.print(formResult2());
    }

    private String formResult1 (){
        String res1 = "";
        for (int i = 0; i < _tLength; i++) {
            if (_truthTable[i][_tWidth - 1] == 1) {
                slagaemye++;
                res1 += " ";
                for (int j = 0; j < _tWidth - 1; j++) {
                    if (_truthTable[i][j] == 0)
                        res1 += "_ ";
                    else
                        res1 += "  ";
                }
                res1 += "  ";
            }
        }
        return res1;
    }

    private String formResult2 (){
        String res2 = "";
        res2 += "\n";
        for (int i = 1; i <= slagaemye; i++) {
            res2 += "(x1x2x3x4)";
            if (i != slagaemye)
                res2 += "+" ;
        }
        res2 += "\n";
        return res2;
    }
}
