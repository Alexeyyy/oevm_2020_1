package saeed_abdullah_lab_5;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI {

	private JFrame frame;
	public static Random rand =new Random();
    private static char RandomArray[]=new char[16];
    private static char[][] TruthMatrix =new char[16][5];
    private static String [] True={"X4","X3","X2","X1"};
    public static StringBuilder RES_1 = new StringBuilder();
    public static StringBuilder RES_2 = new StringBuilder();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		
		frame.setBounds(100, 100, 565, 830);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton Greate = new JButton("Make New table");

		Greate.setBounds(278, 30, 171, 41);
		frame.getContentPane().add(Greate);
		
		JButton CNF = new JButton("CNF");

		CNF.setBounds(278, 82, 171, 41);
		frame.getContentPane().add(CNF);
		
		JButton DNF = new JButton("DNF");

		DNF.setBounds(278, 135, 171, 45);
		frame.getContentPane().add(DNF);
		
		JTextArea ShowText = new JTextArea();
	
		ShowText.setRows(16);
		ShowText.setFont(new Font("Monospaced", Font.BOLD, 18));
		ShowText.setBounds(278, 191, 261, 589);
		
		frame.getContentPane().add(ShowText);
		
		JTextPane textPane = new JTextPane();
		textPane.setForeground(Color.black);
		textPane.setFont(new Font("Tahoma", Font.PLAIN, 32));
		textPane.setBounds(10, 11, 258, 769);
		frame.getContentPane().add(textPane);





        
		Greate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			       int n = 4;
			        int rows = (int) Math.pow(2,n);
			        String OneZero = "01";
			        for (int i=0; i<rows; i++) {
			            for (int j=n-1; j>=0; j--) {
			                TruthMatrix[i][j]= (char) (i/(int) Math.pow(2, j)%2 +48);
			            }
			        }
				
				String str ="";
		        for(int i = 0;i < 16; i++){
		           str+="|";
		            for(int j = 4; j >= 0;j--){
		                RandomArray[i]=OneZero.charAt(rand.nextInt(OneZero.length()));
		               str+=TruthMatrix[i][j]+"  " ;
		            }
		            str+=" | "+ RandomArray[i]+" |\n";
		        }
				
				textPane.setText(str);
								

		        
		        

			}
		});


        

  
		CNF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {


				for(int i = 0;i < 16; i++){
					if(RandomArray[i]=='0') {

						RES_1.append("(");
						for (int j = 4; j >= 0; j--) {

							if (TruthMatrix[i][j] == '1') {

								RES_1.append("!").append(String.valueOf(True[j]));
							} else if (TruthMatrix[i][j] == '0') {
								RES_1.append(String.valueOf(True[j]));
							}
							if(j >0 && j<4){RES_1.append("∨");}
						}
						RES_1.append(")\n");
					}
				}

		      ShowText.setText("Conjunctive normal form\nF(X1,X2,X3,X4)=\n"+RES_1+"\n");

		      RES_1.delete(0,  RES_1.length());
		      while(RES_1.length()==0) {
		    	  JOptionPane.showMessageDialog(frame, "IF YOU WANT TO REUSE PLEASE CLICK ON (MAKE NEW TABLE BUTTON)");
		    	  break;
		      }			
			}
		});

		/**
		 *This method  is used to display the function in DNF
		 */




		/**
		 *This function  is used to find last '1' to put mathematical sign "∨"
		 */
		
		DNF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {




				int counter=0;
				for(int i = 0; i < RandomArray.length; i++){
					if(RandomArray[i] == '1'){
						counter=i;
					}
				}


				for(int i = 0;i < 16; i++){
					if(RandomArray[i]=='1') {
						RES_2.append("(");
						for (int j = 4; j >= 0; j--) {

							if (TruthMatrix[i][j] == '0') {
								RES_2.append("!").append(String.valueOf(True[j]));
							} else if (TruthMatrix[i][j] == '1') {
								RES_2.append(String.valueOf(True[j]));
							}
							if(j >0 && j<4){RES_2.append(" ");}
						}
						RES_2.append(")");
						if(i >= 0&&i < counter)RES_2.append("∨\n");
					}
				}

				ShowText.setText("Disjunctive normal form\nF(X1,X2,X3,X4)=\n"+RES_2+"\n");
			      RES_2.delete(0,  RES_2.length());
			      while(RES_2.length()==0) {
			    	  JOptionPane.showMessageDialog(frame, "IF YOU WANT TO REUSE PLEASE CLICK ON (MAKE NEW TABLE BUTTON)");
			    	  break;
			      }
				
			}
		});




	}
}
