# Лабораторная работа №7
### ПИбд-21 Овсянников Кирилл

##### Техническое задание:
Разработать транслятор программ Pascal-FASM с проверкой синтаксиса и семантики исходной программы




Код Assemler:
```
format PE console

entry start

include 'win32a.inc'

section '.idata' import data readable

	library kernel, 'kernel32.dll',\
	msvcrt, 'msvcrt.dll'

	import kernel,\
	ExitProcess, 'ExitProcess'

	import msvcrt,\
	printf, 'printf',\
	scanf, 'scanf',\
	getch, '_getch'\

section '.data' data readable writable
	spaceStr db '%d', 0
	dopStr db '%d', 0ah, 0
	x dd ?
	y dd ?
	res1 dd ?
	res2 dd ?
	res3 dd ?
	res4 dd ?

section '.code' code readable executable

	start:
		mov ecx, [x]
		add ecx, [y]
		mov [res1], ecx

		mov ecx, [x]
		sub ecx, [y]
		mov [res2], ecx

		mov ecx, [x]
		imul ecx, [y]
		mov [res3], ecx

		mov eax, [x]
		mov ecx, [y]
		div ecx
		mov [res4], eax

		mov ecx, [x]
		add ecx, [y]
		mov [res1], ecx

		mov ecx, [x]
		sub ecx, [y]
		mov [res2], ecx

		mov ecx, [x]
		imul ecx, [y]
		mov [res3], ecx

		mov eax, [x]
		mov ecx, [y]
		div ecx
		mov [res4], eax

	call [getch]
	push NULL
	call [ExitProcess]
  ```

Код Pascal:
```
var
        x, y: integer;
        res1, res2, res3, res4: integer;
        begin
        write('input x: '); readln(x);
        write('input y: '); readln(y);
        res1 := x + y; write('x + y = '); writeln(res1);
        res2 := x - y; write('x - y = '); writeln(res2);
        res3 := x * y; write('x * y = '); writeln(res3);
        res4 := x / y; write('x / y = '); writeln(res4);
        end.
```

ссылка на видео: https://yadi.sk/d/vAOpCLiOgMSTFw