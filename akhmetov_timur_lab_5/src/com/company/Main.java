package com.company;
import java.awt.EventQueue;
import javax.swing.*;

public class Main {

    private JFrame frame;
    private final JButton btnGenRand = new JButton("Random");
    private final JButton btnDNF = new JButton("DNF");
    private final JButton btnKNF = new JButton("KNF");
    private final JTextArea textArea = new JTextArea();
    private final MinKDNF MKDNF = new MinKDNF();
    private final JPanel MyPanel = new Panel(MKDNF);

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                Main window = new Main();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public Main() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1900, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        MyPanel.setBounds(0, 0, 170, 500);
        frame.getContentPane().add(MyPanel);
        textArea.setEditable(false);
        textArea.setBounds(490, 10, 1200, 600);
        frame.getContentPane().add(textArea);

        btnGenRand.addActionListener(e -> {
            MKDNF.fillRand();
            btnDNF.setEnabled(true);
            textArea.setText("");
            MyPanel.repaint();
        });
        btnGenRand.setBounds(180, 10, 90, 30);
        frame.getContentPane().add(btnGenRand);

        btnDNF.addActionListener(e -> {
            String str = new String(MKDNF.createDNF());
            textArea.append(str);
            MyPanel.repaint();
        });
        btnDNF.setBounds(280, 10, 90, 30);
        frame.getContentPane().add(btnDNF);

        btnKNF.addActionListener(e -> {
            String str = new String(MKDNF.createKNF());
            textArea.append(str);
            MyPanel.repaint();
        });
        btnKNF.setBounds(380, 10, 90, 30);
        frame.getContentPane().add(btnKNF);
    }
}

