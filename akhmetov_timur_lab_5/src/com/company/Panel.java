package com.company;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
    MinKDNF MKDNF;
    public Panel(MinKDNF MKDNF) {
        this.MKDNF = MKDNF;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        MKDNF.draw(g);
    }
}

