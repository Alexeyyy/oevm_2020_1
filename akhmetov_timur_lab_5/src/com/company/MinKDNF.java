package com.company;

import java.awt.*;
import java.util.Random;

public class MinKDNF {
    private final int[][] form = {
            {0, 0, 0, 0},
            {0, 0, 0, 1},
            {0, 0, 1, 0},
            {0, 0, 1, 1},
            {0, 1, 0, 0},
            {0, 1, 0, 1},
            {0, 1, 1, 0},
            {0, 1, 1, 1},
            {1, 0, 0, 0},
            {1, 0, 0, 1},
            {1, 0, 1, 0},
            {1, 0, 1, 1},
            {1, 1, 0, 0},
            {1, 1, 0, 1},
            {1, 1, 1, 0},
            {1, 1, 1, 1},
    };

    private final int[] randArr = new int[form.length];

    public void fillRand(){
        for(int i = 0; i < randArr.length; i++) {
            Random random = new Random();
            randArr[i] = random.nextInt(2);
        }
    }

    public StringBuilder createDNF(){
        int cnt = 0;
        StringBuilder sbDNF = new StringBuilder();
        sbDNF.append("ДНФ");
        sbDNF.append("\n");

        for(int i = 0; i < form.length; i++){
            if(randArr[i] == 1){
                if(cnt > 0){
                    sbDNF.append(" + ");
                }
                sbDNF.append("(");
                for(int j = 0; j < form[i].length; j++){
                    if(form[i][j] == 0) {
                        sbDNF.append("!");
                    }
                    sbDNF.append("x" + (j + 1));
                    if(j < form[i].length - 1){
                        sbDNF.append(" * ");
                    }
                }
                sbDNF.append(")");
                cnt++;
            }
        }
        sbDNF.append('\n');
        return sbDNF;
    }

    public StringBuilder createKNF(){
        int cnt = 0;
        StringBuilder sbKNF = new StringBuilder();
        sbKNF.append("КНФ");
        sbKNF.append("\n");

        for(int i = 0; i < form.length; i++){
            if(randArr[i] == 0){
                if(cnt > 0){
                    sbKNF.append(" * ");
                }
                sbKNF.append("(");
                for(int j = 0; j < form[i].length; j++){
                    if(form[i][j] == 1){
                        sbKNF.append("!");
                    }
                    sbKNF.append("x" + (j + 1));
                    if(j < form[i].length - 1){
                        sbKNF.append(" + ");
                    }
                }
                sbKNF.append(")");
                cnt++;
            }
        }
        sbKNF.append('\n');
        return sbKNF;
    }

    private int size_i = 16;
    private int size_j = 4;
    private int x = 10;
    private int y = 10;
    private int width = 30;
    private int height = 30;


    public void draw(Graphics g) {
        for (int i = 0; i <= size_j - 1; i++) {
            g.drawRect(x + width * i, y, width, height);
            g.drawString("x" + (i + 1), x * 2 + width * i, y * 3);
        }

        g.drawRect(x + width * (size_j), y, width, height);
        g.drawString("F", x + 10 + width * (size_j), y + 20);

        for (int i = 0; i < size_i; i++) {
            for (int j = 0; j < size_j; j++) {
                g.drawRect(x + width * j, y + height * i + 30, width, height);
                g.drawString(form[i][j] + "", x + 10 + width * j, y + 20 + height * i + 30);
            }
            g.drawRect(x + width * 4, y + height * i + 30, width, height);
            g.drawString(randArr[i] + "", x + 10 + width * 4, y + 20 + height * i + 30);
        }
    }


}
