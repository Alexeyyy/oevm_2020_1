# Лабораторная работа №8 "Физические компоненты ЭВМ" 

* При разборке компьютера было обнаружено много пыли и он после сборки обратно кое как включился
Но вроде бы работает!!!

* Видеокарта - Radeon 570 4GB
* Процессор - AMD FX8320
* ОЗУ - KINGSTON 8GB DDR3

### До разборки
![ДО](https://sun9-34.userapi.com/impg/ooBaxfZdkQExCqeEitYS9KJ-SUuo3kHncznz1A/m97r8UmdoS8.jpg?size=1620x2160&quality=96&proxy=1&sign=5ceca207faf3852217a1cd135fce8770)

### После разборки
![ПОСЛЕ](https://sun9-74.userapi.com/impg/456sdAFH1F-amt0aTmzVaD9txbuQrPU58MaUlQ/RnCq4a9bJl4.jpg?size=1620x2160&quality=96&proxy=1&sign=be1bb12b5203a478e4720d6010a7a9ad)