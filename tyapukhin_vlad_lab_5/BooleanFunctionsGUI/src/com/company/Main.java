package com.company;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class Main {

    private Random random = new Random();
    private int[] randomArray = new int[16];

    private int[][] array = {

            {0, 0, 0, 0},
            {0, 0, 0, 1},
            {0, 0, 1, 0},
            {0, 0, 1, 1},
            {0, 1, 0, 0},
            {0, 1, 0, 1},
            {0, 1, 1, 0},
            {0, 1, 1, 1},
            {1, 0, 0, 0},
            {1, 0, 0, 1},
            {1, 0, 1, 0},
            {1, 0, 1, 1},
            {1, 1, 0, 0},
            {1, 1, 0, 1},
            {1, 1, 1, 0},
            {1, 1, 1, 1},
    };

    private JFrame frame;
    private JPanel Panel = new Panel(array, randomArray);
    public static boolean dnf, knf;
    public static int step = -1;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    Main() {

        initialize();
        filling();
    }

    private void filling() {
        for(int i = 0; i < 16; i++) {
            randomArray[i] = random.nextInt(2);
        }
    }

    private void initialize() {

        frame = new JFrame("BooleanFunctionsGUI");
        frame.setBounds(100, 100, 437, 592);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        Panel.setBounds(0, 0, 170, 540);
        frame.getContentPane().add(Panel);

        Font BigFontTR = new Font(Font.SANS_SERIF, Font.BOLD, 15);
        JButton btnNewButtonStep = new JButton("Step");
        JButton btnNewButtonDNF = new JButton("DNF");
        JButton btnNewButtonKNF = new JButton("KNF");

        btnNewButtonDNF.setContentAreaFilled(false);
        btnNewButtonDNF.setFocusPainted(false);
        btnNewButtonDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                btnNewButtonStep.setEnabled(true);
                btnNewButtonKNF.setEnabled(false);
                btnNewButtonDNF.setEnabled(false);
                step = 0;
                dnf = true;
                Panel.repaint();
            }
        });
        btnNewButtonDNF.setBounds(180, 447, 100, 25);
        frame.getContentPane().add(btnNewButtonDNF);

        JTextArea textArea = new JTextArea();
        textArea.setBounds(180, 11, 225, 400);
        textArea.setFont(BigFontTR);
        textArea.setEditable(false);
        frame.getContentPane().add(textArea);

        btnNewButtonKNF.setContentAreaFilled(false);
        btnNewButtonKNF.setFocusPainted(false);
        btnNewButtonKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                btnNewButtonStep.setEnabled(true);
                btnNewButtonKNF.setEnabled(false);
                btnNewButtonDNF.setEnabled(false);
                step = 0;
                knf = true;
                Panel.repaint();

            }
        });
        btnNewButtonKNF.setBounds(305, 447, 100, 25);
        frame.getContentPane().add(btnNewButtonKNF);

        btnNewButtonStep.setContentAreaFilled(false);
        btnNewButtonStep.setFocusPainted(false);
        btnNewButtonStep.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                step++;
                if(dnf) {
                    textArea.setText(BooleanFunctionsGUI.dnf(array, randomArray));
                } else if(knf) {
                    textArea.setText(BooleanFunctionsGUI.knf(array, randomArray));
                }
                if (step > 15) {
                    btnNewButtonStep.setEnabled(false);
                }
                Panel.repaint();
            }
        });
        btnNewButtonStep.setBounds(240, 495, 100, 25);
        frame.getContentPane().add(btnNewButtonStep);
    }
}

