package com.company;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {

    private int array[][];
    private int randomArray[];

    BooleanFunctionsGUI functions = new BooleanFunctionsGUI();

    public Panel(int array[][], int randomArray[]) {
        this.array = array;
        this.randomArray = randomArray;
    }

    public void paint(Graphics g) {
        super.paint(g);
        functions.draw(array, randomArray, g);
    }
}


