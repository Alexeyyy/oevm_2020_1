import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Reader reader = new Reader();
        Writer writer = new Writer();
        String row = reader.read();
        Analyzer parseString = new Analyzer(row, writer);
        parseString.parseParthBody();
        writer.write();

    }
}
