package com.company;

import java.util.Arrays;

public class BinaryOperations {
    public static String sumOfBinaryNumbers(char[] firstDigit, char[] secondDigit) {
        StringBuilder sb = new StringBuilder();
        char currentDigit;
        //остаток для переноса в след разряд
        int remnant = 0;
        for (int i = firstDigit.length - 1, j = secondDigit.length - 1; i >= 0 || j >= 0 || remnant == 1; --i, --j) {
            int firstBinaryDigit;
            int secondBinaryDigit;
            if (i < 0) {
                firstBinaryDigit = 0;
            } else if (firstDigit[i] == '0') {
                firstBinaryDigit = 0;
            } else {
                firstBinaryDigit = 1;
            }
            if (j < 0) {
                secondBinaryDigit = 0;
            } else if (secondDigit[j] == '0') {
                secondBinaryDigit = 0;
            } else {
                secondBinaryDigit = 1;
            }
            int currentSum = firstBinaryDigit + secondBinaryDigit + remnant;
            if (currentSum == 0) {
                currentDigit = '0';
                remnant = 0;
            } else if (currentSum == 1) {
                currentDigit = '1';
                remnant = 0;
            } else if (currentSum == 2) {
                currentDigit = '0';
                remnant = 1;
            } else {
                currentDigit = '1';
                remnant = 1;
            }
            sb.insert(0, currentDigit);
        }
        return sb.toString();
    }

    public static String subtractionOfNumbers(char[] minuend, char[] subtrahend) {
        if (Converter.convertToDecimal(2, minuend)
                < Converter.convertToDecimal(2, subtrahend)) {
            char[] tmp = subtrahend;
            subtrahend = minuend;
            minuend = tmp;
        } else if (Converter.convertToDecimal(2, minuend)
                == Converter.convertToDecimal(2, subtrahend)) {
            return "0";
        }
        StringBuilder resultOfSubstraction = new StringBuilder();
        int differenceDischarges = minuend.length - subtrahend.length;
        char[] reverseSubtrahend = new char[minuend.length];
        for (int i = 0; i < reverseSubtrahend.length; i++) {
            if (i < differenceDischarges) {
                reverseSubtrahend[i] = '1';
            } else if (subtrahend[i - differenceDischarges] == '0') {
                reverseSubtrahend[i] = '1';
            } else {
                reverseSubtrahend[i] = '0';
            }
        }
        //дополнительный код, который содержит в себе обратный код + 1
        char[] additionalCode = sumOfBinaryNumbers(reverseSubtrahend, new char[]{'1'}).toCharArray();
        //сложение дополнительного кода и уменьшаемого
        char[] additionalSum = sumOfBinaryNumbers(additionalCode, minuend).toCharArray();
        //проверка наличия незначащих нулей
        boolean insignificantZeros = true;
        for (int i = 1; i < additionalSum.length; i++) {
            if (additionalSum[i] == '1') {
                insignificantZeros = false;
            } else if (insignificantZeros) {
                continue;
            }
            resultOfSubstraction.append(additionalSum[i]);
        }
        return resultOfSubstraction.toString();
    }

    public static String multiplicationOfNumbers(char[] firstBinaryDigit, char[] secondBinaryDigit) {
        char[] resultOfMultiplication = {};
        for (int i = 0; i < Converter.convertToDecimal(2, secondBinaryDigit); i++) {
            resultOfMultiplication = (sumOfBinaryNumbers(resultOfMultiplication, firstBinaryDigit).toCharArray());
        }
        return new String(resultOfMultiplication);
    }

    public static String divisionOfNumbers(char[] helpOfDividend, char[] helpOfDivider) {
        int resultOfDivision = 0;
        while (Converter.convertToDecimal(2, helpOfDividend) >=
                Converter.convertToDecimal(2, helpOfDivider) &&
                !Arrays.equals(helpOfDividend, new char[]{'0'})) {
            helpOfDividend = (subtractionOfNumbers(helpOfDividend, helpOfDivider).toCharArray());
            resultOfDivision++;
        }
        return Converter.convertToBinary(resultOfDivision);
    }
}
