package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String[] system = in.nextLine().split(" ");
        //Исходнаяя СС
        int a = Integer.parseInt(system[0]);
        //Конечная СС
        int b = Integer.parseInt(system[1]);
        //Число, преобразованное в массив символов
        char[] arr = in.nextLine().toCharArray();

        NumberSystems transfer = new NumberSystems(a, b, arr);
        transfer.number_Systems();
    }
}
