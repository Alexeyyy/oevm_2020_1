**Лабораторная работа: №2**  
**Выполнил: Эмиря́н Владимир**

**Задача:**  
Написать программу конвертирующую системы счисления
- исходную систему счисления (2-16);
- конечную систему счисления (2-16);
- число в исходной системе счисления.

[Видео с работой программы](https://yadi.sk/i/H1v4mUsdqjVi5w)
