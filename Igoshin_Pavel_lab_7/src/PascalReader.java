import java.io.FileReader;
import java.io.IOException;

public class PascalReader {
    public String readPascalFile(){
        StringBuilder string = new StringBuilder();

        try(FileReader reader = new FileReader("PascalProgram.pas")){
            int tempChar;
            while((tempChar =reader.read())!=-1){
                string.append((char) tempChar);
            }
        }

        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        return string.toString().replaceAll(";", ";\n");
    }
}