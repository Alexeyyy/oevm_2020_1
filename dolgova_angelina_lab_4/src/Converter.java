public class Converter {

    //Метод для перевода в 2 с.с.
    public static String convertToBinary (int originalNumber) {
        StringBuilder finalNumber = new StringBuilder();
        while (originalNumber >= 2) {
            finalNumber.append(originalNumber % 2);
            originalNumber /= 2;
        }
        finalNumber.append(originalNumber);
        finalNumber.reverse();
        while (finalNumber.length() < 4) {
            finalNumber.insert(0, '0');
        }
        return finalNumber.toString();
    }
}
