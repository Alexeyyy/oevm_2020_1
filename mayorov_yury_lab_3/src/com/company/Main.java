package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your number system: ");
        int firstSystem = scanner.nextInt();
        System.out.print("Enter your first digit: ");
        char[] firstDigit = scanner.next().toCharArray();
        System.out.print("Enter your second digit: ");
        char[] secondDigit = scanner.next().toCharArray();
        System.out.print("Choose your operation: \n_________\n + \n_________ \n - \n_________\n * \n_________\n / \n_________\n");
        char arithmeticOperation = scanner.next().charAt(0);
        int firstNumberDecimal = ConvertingToNumberSystem.convertingToDecimalSystem(firstSystem, firstDigit);
        int secondNumberDecimal = ConvertingToNumberSystem.convertingToDecimalSystem(firstSystem, secondDigit);
        char[] firstNumberBinary = ConvertingToNumberSystem.convertingToBinarySystem(firstNumberDecimal).toCharArray();
        char[] secondNumberBinary = ConvertingToNumberSystem.convertingToBinarySystem(secondNumberDecimal).toCharArray();

        switch (arithmeticOperation) {
            case '+':
                System.out.print("Your sum = " + BinaryOperations.sumOfBinaryNumbers(firstNumberBinary, secondNumberBinary));
                break;
            case '-':
                System.out.print("Your difference = " + BinaryOperations.differenceBetweenBinaryNumbers(firstNumberBinary, secondNumberBinary));
                break;
            case '*':
                System.out.print("Your multiplication = " + BinaryOperations.multiplicationOfBinaryNumbers(firstNumberBinary, secondNumberBinary));
                break;
            case '/':
                System.out.print("Your divide = " + BinaryOperations.divideOfBinaryNumbers(firstNumberBinary, secondNumberBinary));
                break;
            default:
                System.out.print("Unknown symbol");
                break;
        }
    }
}