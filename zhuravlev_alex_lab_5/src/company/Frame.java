package company;

import javax.swing.*;

public class Frame {

    private JFrame frame;
    private final int frameWidth = 730;
    private final int frameHeight = 900;

    private JFrame frameInit() {
        JFrame frame = new JFrame("Converter");
        frame.setSize(frameWidth, frameHeight);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(null);
        return frame;
    }

    private void addElementsToFrame() {
        DrawPanel drawPanel = new DrawPanel(frame);
        JButton buttonRefresh = new JButton("New table");
        JButton buttonStartAnimation = new JButton("Minimizing");

        frame.getContentPane().add(buttonRefresh);
        frame.getContentPane().add(buttonStartAnimation);
        frame.getContentPane().add(drawPanel);

        buttonRefresh.setBounds(350, 10, 130, 30);
        buttonStartAnimation.setBounds(480, 10, 220, 30);
        drawPanel.setBounds(0, 0, frameWidth, frameHeight);

        buttonRefresh.addActionListener(e -> {
            drawPanel.refreshTable();
            buttonStartAnimation.setEnabled(true);
            drawPanel.stopAnimation();
            frame.repaint();
        });

        buttonStartAnimation.addActionListener(e -> {
            drawPanel.startAnimation();
            buttonStartAnimation.setEnabled(false);
        });

        frame.repaint();
    }

    public void showFrame() {
        frame = frameInit();
        addElementsToFrame();
    }
}
