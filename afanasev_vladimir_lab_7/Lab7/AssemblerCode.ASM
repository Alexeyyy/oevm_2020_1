format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable
	spaceStr db '%d', 0
	dopStr db '%d', 0ah, 0
	x dd ?
	y dd ?
	res1 dd ?
	res2 dd ?
	res3 dd ?
	res4 dd ?
section '.code' code readable executable

	 start:
		 push x
		 push spaceStr
		 call [scanf]

		 push y
		 push spaceStr
		 call [scanf]

		 mov ecx, [x]
		 add ecx, [y]
		 mov [res1], ecx
		 push [res1]
		 push dopStr
		 call [printf]

		 mov ecx, [x]
		 sub ecx, [y]
		 mov [res2], ecx
		 push [res2]
		 push dopStr
		 call [printf]

		 mov ecx, [x]
		 imul ecx, [y]
		 mov [res3], ecx
		 push [res3]
		 push dopStr
		 call [printf]

		 mov eax, [x]
		 mov ecx, [y]
		 div ecx
		 mov [res4], eax
		 push [res4]
		 push dopStr
		 call [printf]

	 finish:

		 call [getch]

		 call [ExitProcess]

section '.idata' import data readable

	 library kernel, 'kernel32.dll',\
	 msvcrt, 'msvcrt.dll'

	 import kernel,\
	 ExitProcess, 'ExitProcess'

	 import msvcrt,\
	 printf, 'printf',\
	 scanf, 'scanf',\
	 getch, '_getch'