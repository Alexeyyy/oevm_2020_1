package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class AssemblerWriter {
    HashMap<String, String> stringVariables = new HashMap<>();

    ArrayList<String> arrayVariables = new ArrayList<>();

    String starting = "format PE console\n" +
            "\n" +
            "entry start\n" +
            "\n" +
            "include 'win32a.inc'\n" +
            "\n" +
            "section '.data' data readable writable\n";

    String variables = "\tspaceStr db '%d', 0\n" +
            "\tdopStr db '%d', 0ah, 0\n";

    String code = "section '.code' code readable executable\n" +
            "\n" +
            "\tstart:\n";

    String ending = "\tfinish:\n" +
            "\n" +
            "\t\tcall [getch]\n" +
            "\n" +
            "\t\tcall [ExitProcess]\n" +
            "\n" +
            "section '.idata' import data readable\n" +
            "\n" +
            "\tlibrary kernel, 'kernel32.dll',\\\n" +
            "\t\tmsvcrt, 'msvcrt.dll'\n" +
            "\n" +
            "\timport kernel,\\\n" +
            "\t\tExitProcess, 'ExitProcess'\n" +
            "\n" +
            "\timport msvcrt,\\\n" +
            "\t\tprintf, 'printf',\\\n" +
            "\t\tscanf, 'scanf',\\\n" +
            "\t\tgetch, '_getch'";

    public void write() {
        addVariables();

        try (FileWriter writer = new FileWriter("Assebmler.ASM", false))
        {
            writer.write(starting);
            writer.write(variables);
            writer.write(code);
            writer.write(ending);
            writer.flush();
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void addToVariables(ArrayList<String> arrayList) {
        arrayVariables = arrayList;
    }

    public void addVariables() {
        for (String string : arrayVariables) {
            variables += "\t" + string + " dd ?\n";
        }

        for (String key: stringVariables.keySet()) {
            variables += "\t" + key + " db '" + stringVariables.get(key) + "', 0\n";
        }
    }

    public void addReadLn(String string) {
        if (arrayVariables.contains(string)) {
            code += "\t\tpush " + string + "\n" +
                    "\t\tpush spaceStr\n" +
                    "\t\tcall [scanf]\n\n";
        }
    }

    public void addWrite(String string) {
        stringVariables.put("str" + (stringVariables.size() + 1), string);

        code += "\t\tpush " + "str" + stringVariables.size() + "\n" +
                "\t\tcall [printf]\n";
    }

    public void addOperation(String res, String firstNum, String operator, String secondNum) {
        if (arrayVariables.contains(res) && arrayVariables.contains(firstNum) && arrayVariables.contains(secondNum)) {
            switch (operator) {
                case "+":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\tadd ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "-":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\tsub ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "*":
                    code += "\t\tmov ecx, [" +  firstNum + "]\n" +
                            "\t\timul ecx, [" +  secondNum + "]\n" +
                            "\t\tmov [" + res + "], ecx\n";
                    break;
                case "/":
                    code += "\t\tmov eax, [" +  firstNum + "]\n" +
                            "\t\tmov ecx, [" +  secondNum + "]\n" +
                            "\t\tdiv ecx\n" +
                            "\t\tmov [" + res + "], eax\n";
                    break;
            }
        }
    }

    public void addWriteLn(String string) {
        if (arrayVariables.contains(string)) {
            code += "\t\tpush [" + string + "]\n" +
                    "\t\tpush dopStr\n" +
                    "\t\tcall [printf]\n\n";
        }
    }
}
