package com.company;

public class HelpClass {
    public static char[] convertToBinary(int numberTen) {
        String result = "";
        int remainder = 1;
        int binaryNotation = 2;
        int  numberWFourDigits = 4;

        for (String сurChar = ""; numberTen > 0; numberTen /= binaryNotation) {
            remainder = numberTen % binaryNotation;
            сurChar = "";
            if (numberTen % binaryNotation < 10) {
                сurChar = Integer.toString(remainder);
            } else {
                сurChar = сurChar + (char)((int)('A' + remainder - 10));
            }

            result = сurChar + result;
        }
        while (result.length() < numberWFourDigits) {
            result = '0' + result;
        }
        return result.toCharArray();
    }

    public static int[][] fillingTruthTable(int n, int m, int[][] truthTable) {
        for (int i = 0; i < n; ++i) {
            char[] strX = convertToBinary(i);
            for (int j = 0; j < m - 1; ++j) {
                truthTable[i][j] = strX[j] - '0';
            }
            truthTable[i][m - 1] = (int)(Math.random() * 2); // filling
        }
        return truthTable;
    }

    public static void printTruthTable(int n, int m, int[][] truthTable) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m - 1; ++j) {
                System.out.print(truthTable[i][j] + " ");
            }
            System.out.println("= " + truthTable[i][m - 1]);
        }
    }
}


