package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final int N = 16;                   // количество строк
        final int M = 5;                    // количество столбцов
        int[][] truthTable = new int[N][M]; // таблица истинности
        int form = 0;
        Scanner in = new Scanner(System.in);

        while (form != 1 && form != 2) {
            System.out.printf("1 - Дизъюнктивная нормальная форма\n" +
                              "2 - Конъюнктивная нормальная форма\n" +
                              "Выбор формы для расчета: ");
            form = in.nextInt();
        }

        truthTable = HelpClass.fillingTruthTable(N, M, truthTable); // заполняем таблицу истинности
        HelpClass.printTruthTable(N, M, truthTable);             // выводим результат на экран

        if (form == 1) {
            System.out.print("\n ДНФ: ");
            NForm.toDisjunctiveNForm(N, M, truthTable);

        } else if (form == 2) {
            System.out.print("\n КНФ: ");
            NForm.toConjunctiveNForm(N, M, truthTable);
        }
    }
}
