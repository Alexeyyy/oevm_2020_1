package com.company;

public class HelpClass {
    public static char[] convertToBinary(int numberTen) {
        String result = "";
        int remainder = 1;
        int binaryNumber = 2;

        for (String сurChar = ""; numberTen > 0; numberTen /= binaryNumber) {
            remainder = numberTen % binaryNumber;
            сurChar = "";
            if (numberTen % binaryNumber < 10) {
                сurChar = Integer.toString(remainder);
            } else {
                сurChar = сurChar + (char)((int)('A' + remainder - 10));
            }

            result = сurChar + result;
        }
        int numberWFourDigits = 4;
        while (result.length() < numberWFourDigits) {
            result = '0' + result;
        }
        return result.toCharArray();
    }

    public static int[][] fillingTruthTable(int n, int m, int[][] truthTable) {
        for (int i = 0; i < n; ++i) {
            char[] strX = convertToBinary(i);
            for (int j = 0; j < m - 1; ++j) {
                truthTable[i][j] = strX[j] - '0';
            }
            truthTable[i][m - 1] = (int)(Math.random() * 2);
        }
        return truthTable;
    }
}
