package com.company;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Main {
    Drawing method = new Drawing();
    StringBuilder strBuilder = new StringBuilder();

    static boolean _DNF = false;
    static boolean _CNF = false;

    static Property prop = new Property();

    final static int N = prop.getPropertyInt("N");
    final static int M = prop.getPropertyInt("M");

    public static int[][] truthTable = new int[N][M]; // таблица истинности

    public static void main(String[] args) {

        truthTable = HelpClass.fillingTruthTable(N, M, truthTable); // заполняем таблицу истинности

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
    private JPanel Panel = new Panel(truthTable);
    private JFrame frame;
    private JButton btnDNF = new JButton("ДНФ");
    private JButton btnCNF = new JButton("КНФ");
    private JTextArea txtFunction = new JTextArea();

    public Main() {
        printOnMyPanel();
    }

    private void printOnMyPanel() {
        frame = new JFrame();
        frame.setBounds(100, 100, 700, 720);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        Panel.setBounds(0, 0, 270, 720);
        frame.getContentPane().add(Panel);

        txtFunction.setEditable(false);
        txtFunction.setBounds(330, 100, 300, 100);
        frame.getContentPane().add(txtFunction);

        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnCNF.setEnabled(false);
                btnDNF.setEnabled(false);
                _DNF = true;
                txtFunction.setText(strBuilder.append(method.convertToDNF(truthTable, N, M)).toString());
                Panel.repaint();
            }
        });
        btnDNF.setBounds(400, 10, 70, 50);
        frame.getContentPane().add(btnDNF);


        btnCNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnCNF.setEnabled(false);
                btnDNF.setEnabled(false);
                _CNF = true;
                txtFunction.setText(strBuilder.append(method.convertToCNF(truthTable, N, M)).toString());
                Panel.repaint();
            }
        });
        btnCNF.setBounds(500, 10, 70, 50);
        frame.getContentPane().add(btnCNF);
    }
}
