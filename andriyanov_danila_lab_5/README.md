### Лабораторная работа номер 5
### Выполнил студент ИСЭбд-21 Андриянов Данила

*Использованные технологии*: язык программирования Java

Программа выполняет следующие задачи:
Визуализирует лабороторную работу 4.

Ссылка на видео: https://drive.google.com/file/d/14znhxfd_XrmR-RQmBDgzvt9E1h-Sllch/view?usp=sharing

Скриншоты программы: 

https://drive.google.com/file/d/1aWRR7oR68cgD3J3SPloewePJHitx7CLq/view?usp=sharing

https://drive.google.com/file/d/1BF5TlbJHcO0kl5HSX-C9DxWugh3eySXx/view?usp=sharing