package com.company;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	    System.out.println("Укажите исходную систему счисления: ");
	    int in = sc.nextInt();
        System.out.println("Укажите конечную систему счисления: ");
        int out = sc.nextInt();
        System.out.println("Укажите число в исходной системе счисления: ");
        String num = sc.next();

        String res = new BigInteger(String.valueOf(num), in).toString(out);
        System.out.println(res);
    }

}
