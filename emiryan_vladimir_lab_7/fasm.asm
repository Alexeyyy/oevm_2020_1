format PE console

entry start

include 'win32a.inc'
section '.data' data readable writable
	empty db '%d', 0
	NULL = 0

	x dd ?
	y dd ?
	res1 dd ?
	res2 dd ?
	res3 dd ?
	res4 dd ?
	str1 db 'input x: ',0dh, 0ah, 0
	str2 db 'input y: ',0dh, 0ah, 0
	str3 db 'x + y =  %d',0dh, 0ah, 0
	str4 db 'x - y =  %d',0dh, 0ah, 0
	str5 db 'x * y =  %d',0dh, 0ah, 0
	str6 db 'x / y =  %d',0dh, 0ah, 0

section '.code' code readable executable
        start:
	push str1
	call[printf]

	push x
	push empty 
	call[scanf]

	push str2
	call[printf]

	push y
	push empty 
	call[scanf]

	mov ecx, [x]
	add ecx, [y]
	push ecx
	push str3
	call[printf]

	push res1
	call[printf]

	mov ecx, [x]
	sub ecx, [y]
	push ecx
	push str4
	call[printf]

	push res2
	call[printf]

	mov ecx, [x]
	imul ecx, [y]
	push ecx
	push str5
	call[printf]

	push res3
	call[printf]

	mov eax, [x]
	mov ecx, [y]
	mov edx, 0
	div ecx
	push eax
	push str6
	call[printf]

	push res4
	call[printf]


	call[getch]
        push NULL
        call [ExitProcess]

section '.idata' import data readable
        library kernel, 'kernel32.dll',\
                 msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvcrt,\
              printf, 'printf',\
              getch, '_getch', \
              scanf, 'scanf'
