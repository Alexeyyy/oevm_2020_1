package lab_7;

import java.io.*;
import java.util.ArrayList;

public class Translator {
    File out;
    public Translator(File out) {
        this.out = out;
    }

    String mode = "";
    String varZone = "";
    String codeZone = "";
    int name = 1;
    String varWrite = "str";
    String result = "";
    public void getString(String string) throws IOException {
        if(string.indexOf("'") > -1) {
            for(int j = string.indexOf("'") + 1; string.charAt(j) != '\''; j++) {
                if(string.charAt(j) == ' ') {
                    char[] t = string.toCharArray();
                    t[j] = '~';
                    String temp = new String(t);
                    string = temp;
                }
            }
        }
        string = string.replaceAll("\\s+","");
        if(string.indexOf("var") > -1) {
            mode = "var";
            return;
        } else if(string.indexOf("begin") > -1) {
            mode = "run";
            return;
        }
        if(mode == "var") {
            String vars = "";
            String varString = "";
            String type = "";
            varString = string.split(":")[0];
            type = string.split(":")[1].replaceAll(";", "");

            for(int i = 0; i < varString.split(",").length; i++) {
                vars += "\t" + varString.split(",")[i] + " dd ?\n";
            }

            varZone += vars;
        } else if(mode == "run") {
            for(int i = 0; i < string.split(";").length; i++) {
                String operation = string.split(";")[i];
                if(operation.indexOf("write") > -1) {
                    String write = "";
                    for(int j = operation.indexOf("'"); operation.indexOf('\'') > -1 && operation.charAt(j) != ')'; j++) {
                        if(j > operation.indexOf("'") && operation.charAt(j) == '\'' && operation.indexOf("input") == -1) {
                            write += " %d";
                        }
                        write += operation.charAt(j);
                        operation = operation.replaceAll("~"," ");
                    }
                    if(write != "") {
                        varZone += "\t" + "str" + name + " db " + write + ",0dh, 0ah, 0\n";
                        codeZone += "\t" + "push " + "str" + name++ + "\n"  + "\t" + "call[printf]\n\n";
                    } else {
                        write = "";
                        for(int j = operation.indexOf("(") + 1; operation.charAt(j) != ')'; j++) {
                            write += operation.charAt(j);
                        }
                        codeZone += "\t" + "push " + write + "\n" + "\t" +"call[printf]\n\n";
                    }

                } else if(operation.indexOf("read") > -1) {
                    String read = "";
                    for(int j = operation.indexOf("(") + 1; operation.charAt(j) != ')'; j++) {
                        read += operation.charAt(j);
                    }
                    codeZone += "\t" + "push " + read + "\n" + "\tpush empty \n" + "\t" +"call[scanf]\n\n";
                } else if(operation.indexOf(":=") > -1) {
                    if(operation.indexOf("+") > -1) {
                        codeZone += "\tmov ecx, [x]\n" +
                                    "\tadd ecx, [y]\n" +
                                    "\tpush ecx\n";
                    } else if(operation.indexOf("-") > -1) {
                        codeZone += "\tmov ecx, [x]\n" +
                                    "\tsub ecx, [y]\n" +
                                    "\tpush ecx\n";
                    } else if(operation.indexOf("*") > -1) {
                        codeZone += "\tmov ecx, [x]\n" +
                                    "\timul ecx, [y]\n" +
                                    "\tpush ecx\n";
                    } else if(operation.indexOf("/") > -1) {
                        codeZone += "\tmov eax, [x]\n" +
                                    "\tmov ecx, [y]\n" +
                                    "\tmov edx, 0\n" +
                                    "\tdiv ecx\n" +
                                    "\tpush eax\n";
                    }

                }
            }
        }
    }

    public String getCode() throws IOException {
        String str = "";
        File head = new File("D:\\Projects\\Java\\OEVM_labs\\OEVM_lab_7\\emiryan_vladimir_lab_7\\patterns\\head.txt");
        File code = new File("D:\\Projects\\Java\\OEVM_labs\\OEVM_lab_7\\emiryan_vladimir_lab_7\\patterns\\code.txt");
        File data = new File("D:\\Projects\\Java\\OEVM_labs\\OEVM_lab_7\\emiryan_vladimir_lab_7\\patterns\\data.txt");
        File idata = new File("D:\\Projects\\Java\\OEVM_labs\\OEVM_lab_7\\emiryan_vladimir_lab_7\\patterns\\idata.txt");
        BufferedReader br1 = new BufferedReader(new FileReader(head));
        while(str != null) {
            str = br1.readLine();

            if(str != null) {
                result += str + "\n";
            }
        }
        str="";
        BufferedReader br2 = new BufferedReader(new FileReader(data));
        while(str != null) {
            str = br2.readLine();
            if(str != null) {
                result += str + "\n";
            }
        }
        str="";

        result += varZone + "\n";

        BufferedReader br3 = new BufferedReader(new FileReader(code));
        while(str != null) {
            str = br3.readLine();
            if(str != null) {
                result += str + "\n";
            }
        }

        result += codeZone + "\n";

        str="";
        BufferedReader br4 = new BufferedReader(new FileReader(idata));
        while(str != null) {
            str = br4.readLine();
            if(str != null) {
                result += str + "\n";
            }
        }
        return result;
    }
}