package lab_7;

import java.io.*;
import java.lang.annotation.Target;

public class Main {
    public static void main(String[] args) throws IOException {
        File in = new File("D:\\Projects\\Java\\OEVM_labs\\OEVM_lab_7\\emiryan_vladimir_lab_7\\pascal.pas");
        File out = new File("D:\\Projects\\Java\\OEVM_labs\\OEVM_lab_7\\emiryan_vladimir_lab_7\\fasm.asm");

        Translator translator = new Translator(out);
        FileWriter writer = new FileWriter(out);
        BufferedReader br = new BufferedReader(new FileReader(in));
        String str = br.readLine();
        while(str != null) {
            translator.getString(str);
            System.out.println(str);
            str = br.readLine();
        }

        writer.write(translator.getCode());
        writer.close();
    }
}
