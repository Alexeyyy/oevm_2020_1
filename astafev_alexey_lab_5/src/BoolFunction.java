import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class BoolFunction {

    private static int sizeI = 16;
    private static int sizeJ = 4;
    private static int x = 10;
    private static int y = 10;
    private static int width = 30;
    private static int height = 30;

    public void draw(int array[][], int randomArray[], Graphics g) {

        Graphics2D g2 = (Graphics2D) g;
        BasicStroke pen = new BasicStroke(1);
        g2.setStroke(pen);
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 13));

        for (int j = 0; j < sizeJ; j++) {
            g.drawRect(x + width * j, y, width, height);
            g.drawString("X" + (j + 1), x * 2 + width * j - 3, y * 3);
        }

        g.drawRect(x + width * sizeJ, y, width, height);
        g.drawString("F", x + 10 + width * sizeJ, y + 20);

        y += 30;

        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                g.drawRect(x + width * j, y + height * i, width, height);
                g.drawString(array[i][j] + "", x + 10 + width * j, y + 20 + height * i);
            }
            g.drawRect(x + width * 4, y + height * i, width, height);
            g.drawString(randomArray[i] + "", x + 10 + width * 4, y + 20 + height * i);
        }

        drawDNForKNF(array, randomArray, g);

        y -= 30;
    }

    public static void drawDNForKNF(int array[][], int randomArray[], Graphics g) {
        if (Main.knf) {
            for (int i = 0; i < Main.step; i++) {
                if (randomArray[i] == 0) {
                    g.setColor(new Color(0, 255, 0));
                } else {
                    g.setColor(new Color(255, 0, 0));
                }
                for (int j = 0; j < sizeJ; j++) {
                    g.drawRect(x + width * j, y + height * i, width, height);
                    g.drawString(array[i][j] + "", x + 10 + width * j, y + 20 + height * i);
                }
                g.drawRect(x + width * sizeJ, y + height * i, width, height);
                g.drawString(randomArray[i] + "", x + 10 + width * sizeJ, y + 20 + height * i);
            }
        } else if (Main.dnf) {
            for (int i = 0; i < Main.step; i++) {
                if (randomArray[i] == 1) {
                    g.setColor(new Color(0, 255, 0));
                } else {
                    g.setColor(new Color(255, 0, 0));
                }
                for (int j = 0; j < sizeJ; j++) {
                    g.drawRect(x + width * j, y + height * i, width, height);
                    g.drawString(array[i][j] + "", x + 10 + width * j, y + 20 + height * i);
                }
                g.drawRect(x + width * sizeJ, y + height * i, width, height);
                g.drawString(randomArray[i] + "", x + 10 + width * sizeJ, y + 20 + height * i);
            }
        }
    }

    public static String dnf(int array[][], int randomArray[]) {

        StringBuilder strBuilder = new StringBuilder();
        int counter = 0;

        for (int i = 0; i < Main.step; i++) {
            if (randomArray[i] == 1) {
                if (counter > 0) {
                    strBuilder.append(" +\n");
                }
                strBuilder.append(" (");
                for (int j = 0; j < sizeJ; j++) {
                    if (array[i][j] == 0) {
                        strBuilder.append("-");
                    }
                    strBuilder.append("X" + (j + 1));
                    if (j < sizeJ - 1) {
                        strBuilder.append(" * ");
                    }
                }
                strBuilder.append(")");
                counter++;
            }
        }
        return strBuilder.toString();
    }

    public static String knf(int array[][], int randomArray[]) {

        StringBuilder strBuilder = new StringBuilder();
        int counter = 0;

        for (int i = 0; i < Main.step; i++) {
            if (randomArray[i] == 0) {
                if (counter > 0) {
                    strBuilder.append(" *\n");
                }
                strBuilder.append(" (");
                for (int j = 0; j < sizeJ; j++) {
                    if (array[i][j] == 1) {
                        strBuilder.append("-");
                    }
                    strBuilder.append("X" + (j + 1));
                    if (j < sizeJ - 1) {
                        strBuilder.append(" + ");
                    }
                }
                strBuilder.append(")");
                counter++;
            }
        }
        return strBuilder.toString();
    }
}