package com.company;

public class HelperClass {
    public static void checkForBugs(long ss1, char number2[], int i) { // Функция проверки на соответствие числа системе счисления
        long digit;
        if (number2[i] >= 'A' && number2[i] <= 'F') {
            digit = (long) (number2[i] - 'A' + 10);
        } else {
            digit = (long) (number2[i] - '0');
        }
        if (digit >= ss1) {
            System.out.println("Некоректный ввод данных");
            System.exit (0);
        }
    }


    public static long convertTo10Base(long ss1, char[] number2) { // Фуннкция перевода чисел в десятичную систему счисления
        long k = 0;
        long sum = 0;
        long l = number2.length-1;
        for (int i = 0; i < number2.length; i++) {
            checkForBugs (ss1, number2, i); // вызов функции проверки
            if (number2[i] >= '0' && number2[i] <= '9') {
                k = (long) ((number2[i] - '0') * Math.pow(ss1, l));
                sum = sum + k;
                l--;
            }
            if (number2[i] >= 'A' && number2[i] <= 'F') {
                k = (long) ((number2[i] - 'A' + 10)* Math.pow(ss1, l));
                sum = sum + k;
                l--;
            }
        }
        return sum;
    }

    public static char[] convertToFinalBase(long sum, long ss2, char rezult[]) { // Функция перевода чисел в конечную систему счисления
        int i2 = 1;
        while (sum >= ss2) { //Перевод в нужную систему
            long symbol = sum % ss2;
            sum = sum / ss2;

            if (symbol >= 0 && symbol <= 9) {
                rezult[i2] = (char) (symbol + '0');
                i2++;
            }
            if (symbol > 9 && symbol < 16) {
                rezult[i2] = (char) (symbol + 'A' - 10);
                i2++;
            }
        }
        if (sum<ss2) {
            rezult[i2] = (char) (sum + '0');
            rezult[0] = (char) (i2);
        }
        return rezult;
    }


    public static void outputTheRezult(int count, char rezult[]) { //Функция вывода
        for (int i = count; i > 0; i--) {
            System.out.print(rezult[i]);
        }
    }
}
