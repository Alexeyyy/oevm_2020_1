import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {

    BooleanTable table;
    public int YSize;
    public int XSize;

    public MyPanel(BooleanTable tmp){
        table = tmp;
        XSize=table.checkXSize();
        YSize=table.checkYSize();
    }

    public int getXSize(){
        return XSize;
    }

    public int getYSize(){
        return YSize;
    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
        table.draw(g);
    }
}
