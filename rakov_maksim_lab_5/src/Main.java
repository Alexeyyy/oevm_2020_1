import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {

    private JFrame frame;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public Main() {
        initialize();
    }

    private void initialize() {
        final int button_length = 100;
        final int text_area_size = 250;

        BooleanTable table = new BooleanTable();
        MyPanel panel = new MyPanel(table);

        frame = new JFrame();
        frame.setBounds(100, 100, panel.getXSize() + 20 + button_length + 20 + text_area_size,
                panel.getYSize() + 40);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        panel.setBounds(10, 10, panel.getXSize(), panel.getYSize());
        frame.getContentPane().add(panel);

        JTextArea textArea = new JTextArea();
        textArea.setBounds(panel.getXSize() + 30 + button_length, 10, text_area_size - 20, panel.getYSize());
        frame.getContentPane().add(textArea);

        JButton tableCreateButton = new JButton("Создать");
        tableCreateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.changeNumbers();
                panel.repaint();
                textArea.setText("");
            }
        });
        tableCreateButton.setBounds(panel.getXSize() + 20, 10, button_length, 30);
        frame.getContentPane().add(tableCreateButton);

        JButton formKButton = new JButton("КНФ");
        formKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.formKColor();
                panel.repaint();
                String str = table.makeKForm();
                textArea.setText(str);
            }
        });
        formKButton.setBounds(panel.getXSize() + 20, 50, button_length, 30);
        frame.getContentPane().add(formKButton);

        JButton formDButton = new JButton("ДНФ");
        formDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.formDColor();
                panel.repaint();
                String str = table.makeDForm();
                textArea.setText(str);
            }
        });
        formDButton.setBounds(panel.getXSize() + 20, 90, button_length, 30);
        frame.getContentPane().add(formDButton);

    }
}
