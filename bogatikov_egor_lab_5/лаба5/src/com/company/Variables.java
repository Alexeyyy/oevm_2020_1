package com.company;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.lang.Integer;

class Variables {
    public int getPropertyValue(String propertyName) {
        int propertyValue = 0;
        Properties prop = new Properties();

        try (InputStream inputStream = this.getClass().getResourceAsStream("config.properties")) {
            prop.load(inputStream);
            propertyValue = Integer.valueOf(prop.getProperty(propertyName));
        } catch (IOException e) {
            System.out.print(e);
        }
        return propertyValue;
    }
}
