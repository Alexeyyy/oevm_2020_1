import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {

    private Func func;

    public void updatePanel(Func func){
        this.func=func;
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if(func!=null){
            func.paintFunc(g);
        }
    }
}
