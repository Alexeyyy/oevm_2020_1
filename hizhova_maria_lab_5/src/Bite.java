import java.util.Random;

public class Bite {

    boolean dnf = true;

    int nvar = 4;
    int tableSize = 1 << nvar;
    int[][] trueTable = new int[tableSize][nvar + 1];
    Random rand = new Random();
    int nmin = 0;
    int nmin1 = 0;
    int[][] a;
    int[][] a1;
    int[] mint;
    int[] mint1;

    int[][] b;
    int[][] pi;
    int[] checker;
    int flag2 = 0;

    int[] dash;

    int count;
    int flag;

    public String globalF1 = "F=";

    public String globalF2 = "F=";

    public static String decode(int a[][], int row, int nvar, String bitvar[],
                                boolean DNF) {
        String s = "";
        int m = 0;
        for (int i = 0; i < a[row].length; i++) {
            if (a[row][i] != 9)
                m++;
        }
        for (int i = 0; i < a[row].length; i++) {
            if (a[row][i] == 9)
                continue;
            else if (a[row][i] == 1) {
                s += bitvar[i];
                if (!DNF && m > 1) {
                    s += "+";
                    m--;
                }
            } else {
                s += bitvar[i] + "'";
                if (!DNF && m > 1) {
                    s += "+";
                    m--;
                }
            }

        }
        return s;
    }

    public static boolean match(int min, int a[][], int row, int nvar) {
        int b[] = new int[nvar], i = nvar - 1, c = 0;
        fillArray(b, 0);
        while (min > 0) {
            b[i] = min % 2;
            min /= 2;
            i--;
        }

        for (i = 0; i < nvar; i++) {
            if (a[row][i] == 9)
                continue;
            if (a[row][i] != b[i])
                c++;
        }
        if (c == 0)
            return true;
        return false;
    }

    public static void fillMatrix(int a[][], int val) {
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < a[i].length; j++)
                a[i][j] = val;
    }

    public static void fillArray(int a[], int val) {
        for (int i = 0; i < a.length; i++)
            a[i] = val;
    }

    public void exit(boolean DNF, int[] s) {
        for (int i = 0; i < tableSize; i++) {
            int bit = 1;
            for (int j = nvar - 1; j >= 0; j--) {
                trueTable[i][j] = (i & bit) == 0 ? 0 : 1;
                bit <<= 1;
            }
            trueTable[i][nvar] = s[i];
        }

        if (DNF) {
            for (int j = 0; j < nvar; j++) {
                System.out.print("X" + (j + 1) + " ");
            }
            System.out.println("F");
            for (int i = 0; i < tableSize; i++) {
                for (int j = 0; j < nvar + 1; j++) {
                    System.out.print(trueTable[i][j] + "  ");

                }
                System.out.println();
            }
        }
        for (int i = 0; i < tableSize; i++) {
            int bit = 1;
            for (int j = nvar - 1; j >= 0; j--) {
                trueTable[i][j] = (i & bit) == 0 ? 0 : 1;
                bit <<= 1;
            }
            if (!DNF) {
                if (trueTable[i][nvar] == 0) {
                    trueTable[i][nvar] = 1;
                } else
                    trueTable[i][nvar] = 0;
            }
        }
    }

    public void searchminterm() {
        for (int i = 0; i < tableSize; i++) {
            int f = trueTable[i][nvar];
            if (f == (dnf ? 1 : 0)) {
                nmin++;
            } else {
                nmin1++;
            }
        }
    }

    public void term() {
        mint = new int[nmin];
        mint1 = new int[nmin1];
        int m = 0;
        for (int i = 0; i < tableSize; i++) {
            int f = trueTable[i][nvar];
            if (f == 1) {
                mint[m++] = i;
            }
        }

        int m1 = 0;
        for (int i = 0; i < tableSize; i++) {
            int f = trueTable[i][nvar];
            if (f == 0) {
                mint1[m1++] = i;
            }
        }
    }

    public void loadterm() {
        a = new int[nmin * (nmin + 1) / 2][nvar];
        a1 = new int[nmin * (nmin + 1) / 2][nvar];
        fillMatrix(a, -1);
        for (int i = 0; i < nmin; i++)
            for (int j = 0; j < nvar; j++)
                a[i][j] = 0;

        for (int i = 0; i < nmin; i++) {
            int x = mint[i];
            int pos = nvar - 1;
            while (x > 0) {
                a[i][pos] = x % 2;
                pos--;
                x /= 2;
            }
        }
    }

    public void searhimplec() {
        b = new int[nmin * (nmin + 1) / 2][nvar];
        pi = new int[nmin * (nmin + 1) / 2][nvar];
        checker = new int[nmin * (nmin + 1) / 2];
        while (true) {
            count = 0;
            flag = 0;
            fillMatrix(b, -1);
            fillArray(checker, -1);
            int i;
            for (i = 0; i < a.length; i++) {
                if (a[i][0] == -1)
                    break;
                for (int j = i + 1; j < a.length; j++) {
                    int c = 0;
                    int pos = 0;
                    if (a[j][0] == -1)
                        break;
                    for (int k = nvar - 1; k >= 0; k--)
                        if (a[i][k] != a[j][k]) {
                            pos = k;
                            c++;
                        }
                    if (c == 1) {
                        count++;
                        checker[i]++;
                        checker[j]++;
                        for (int k = nvar - 1; k >= 0; k--)
                            b[flag][k] = a[i][k];
                        b[flag][pos] = 9;
                        flag++;
                    }
                }
            }
            for (int j = 0; j < i; j++) {
                if (checker[j] == -1) {
                    for (int k = 0; k < nvar; k++)
                        pi[flag2][k] = a[j][k];
                    int c3 = 0;
                    for (int x = flag2 - 1; x >= 0; x--) {
                        int c1 = 0;
                        for (int y = 0; y < nvar; y++) {
                            if (pi[x][y] != pi[flag2][y])
                                c1++;
                        }
                        if (c1 == 0) {
                            c3++;
                            break;
                        }
                    }
                    if (c3 == 0)
                        flag2++;
                }
            }

            if (count == 0)
                break;

            for (i = 0; i < b.length; i++)
                for (int j = 0; j < b[i].length; j++)
                    a[i][j] = b[i][j];
        }
        dash = new int[nvar];
        fillArray(dash, -1);
        a = new int[flag2][nmin];
        fillMatrix(a, 0);
        for (int i = 0; i < flag2; i++) {
            for (int j = 0; j < nmin; j++) {
                if (match(mint[j], pi, i, nvar))
                    a[i][j] = 1;
            }
        }

        checker = new int[flag2];
        dash = new int[nmin];
        fillArray(checker, -1);
        fillArray(dash, -1);
        for (int j = 0; j < nmin; j++) {
            int count = 0;
            int pos = 0;
            for (int i = 0; i < flag2; i++) {
                if (a[i][j] == 1) {
                    pos = i;
                    count++;
                }
            }
            if (count == 1)
                checker[pos]++;
        }

        for (int i = 0; i < flag2; i++) {
            if (checker[i] != -1) {
                for (int j = 0; j < nmin; j++) {
                    if (a[i][j] == 1)
                        dash[j]++;
                }

                for (int j = 0; j < nmin; j++)
                    a[i][j] = -1;
            }
        }

        for (int j = 0; j < nmin; j++) {
            if (dash[j] != -1) {
                for (int i = 0; i < flag2; i++)
                    a[i][j] = -1;
            }
        }

        while (true) {
            int count = 0;

            for (int j = 0; j < nmin; j++) {
                for (int k = j + 1; k < nmin; k++) {
                    int c1 = 0;
                    int c2 = 0;
                    int c3 = 0;
                    for (int i = 0; i < flag2; i++) {
                        if (a[i][j] == 1 && a[i][k] == 1)
                            c1++;
                        if (a[i][j] == 1 && a[i][k] == 0)
                            c2++;
                        if (a[i][j] == 0 && a[i][k] == 1)
                            c3++;
                    }
                    if (c2 > 0 && c3 > 0) {
                        break;
                    }
                    if (c1 > 0 && c2 > 0 && c3 == 0) {
                        for (int no9 = 0; no9 < flag2; no9++)
                            a[no9][j] = -1;
                        count++;
                    }
                    if (c1 > 0 && c3 > 0 && c2 == 0) {
                        for (int no9 = 0; no9 < flag2; no9++)
                            a[no9][k] = -1;
                        count++;
                    }
                    if (c1 > 0 && c2 == 0 && c3 == 0) {
                        for (int no9 = 0; no9 < flag2; no9++)
                            a[no9][j] = -1;
                        count++;
                    }
                }
            }

            for (int i = 0; i < flag2; i++) {
                for (int j = i + 1; j < flag2; j++) {
                    int c1 = 0;
                    int c2 = 0;
                    int c3 = 0;
                    for (int k = 0; k < nmin; k++) {
                        if (a[i][k] == 1 && a[j][k] == 1)
                            c1++;
                        if (a[i][k] == 1 && a[j][k] == 0)
                            c2++;
                        if (a[i][k] == 0 && a[j][k] == 1)
                            c3++;
                    }

                    if (c2 > 0 && c3 > 0)
                        break;
                    if (c1 > 0 && c2 > 0 && c3 == 0) {
                        for (int no9 = 0; no9 < nmin; no9++)
                            a[j][no9] = -1;
                        count++;
                    }
                    if (c1 > 0 && c3 > 0 && c2 == 0) {
                        for (int no9 = 0; no9 < nmin; no9++)
                            a[i][no9] = -1;
                        count++;
                    }
                    if (c1 > 0 && c2 == 0 && c3 == 0) {
                        for (int no9 = 0; no9 < nmin; no9++)
                            a[j][no9] = -1;
                        count++;
                    }
                }
            }
            if (count == 0)
                break;
        }
    }

    public void ENDexit(boolean DNF) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < nmin; j++)
                if (a[i][j] == 1)
                    checker[i]++;
        }

        String bitvar[] = new String[nvar];
        for (int i = 0; i < nvar; i++)
            bitvar[i] = "X" + (i + 1);
        if (DNF) {
            System.out.print("Минимальная функция DNF: ");
            boolean first = true;
            for (int i = 0; i < flag2; i++) {
                if (checker[i] != -1) {
                    if (first) {
                        globalF1 += "(";
                        System.out.print("(");
                        first = false;
                    } else {
                        globalF1 += ") + (";
                        System.out.print(") + (");
                    }
                    globalF1 += decode(pi, i, nvar, bitvar, DNF);
                    System.out.print(decode(pi, i, nvar, bitvar, DNF));
                }

            }
            globalF1 += ")";
            System.out.print(")");
            System.out.println();
        } else {
            System.out.print("Минимальная функция KNF: ");
            boolean first = true;
            for (int i = 0; i < flag2; i++) {
                if (checker[i] != -1) {
                    if (first) {
                        globalF2 += "(";
                        System.out.print("(");
                        first = false;
                    } else {
                        globalF2 += ")(";
                        System.out.print(")(");
                    }
                    globalF2 += decode(pi, i, nvar, bitvar, DNF);
                    System.out.print(decode(pi, i, nvar, bitvar, DNF));
                }
            }
            globalF2 += ")";
            System.out.print(")");
            System.out.println();
        }
    }

    public int check(int[] s) {
        int k = 0;
        int m = 0;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == 1) {
                k++;
            } else
                m++;
        }
        if (m == s.length || k == s.length) {
            return s.length;
        }
        return m;
    }

    public String global1() {
        return globalF1;
    }

    public String global2() {
        return globalF2;
    }

    public void END(int[] s) {
        boolean DNF = true;
        if (check(s) != s.length) {
            exit(DNF, s);
            searchminterm();
            term();
            loadterm();
            searhimplec();
            ENDexit(DNF);
            exit(!DNF, s);
            searchminterm();
            term();
            loadterm();
            searhimplec();
            ENDexit(!DNF);
        } else {
            exit(DNF, s);
            if (s[0] == 0) {
                System.out.print("Минимальная функция DNF: ---\n");
                System.out.print("Минимальная функция KNF: 0");
                globalF2 += "0";
            }
            if (s[0] == 1) {
                System.out.print("Минимальная функция DNF: 1\n");
                globalF1 += "1";
                System.out.print("Минимальная функция KNF: ---");
            }
        }
    }
}
