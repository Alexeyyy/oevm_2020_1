package com.company;

public class Helper {
    static final char[] SYMBOL = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static long numberToTen(String result, long number, int initialNotation) { // перевод в десятеричную систему счисления
        for (int i = 0; i < result.length(); i++) {
            for (int j = 0; j <= 15; j++) {
                if (result.charAt(i) == SYMBOL[j]) { // проверяю, если символы сходятся, считаю число в десятичной системе
                    number += j * Math.pow(initialNotation, i);
                }
            }
        }
        return number;
    }

    public static String convert(long finalNumber, String notFinalResult) { // перевод в двоичную систему
        while (((finalNumber / 2 != 0)) || (finalNumber % 2 != 0)) {
            notFinalResult += finalNumber % 2; // прибавляется остаток от деления
            finalNumber = finalNumber / 2;
        }
        return notFinalResult;
    }

    public static String addition(String secondSSResultOne, String secondSSResultTwo) { // сложение
        String tmp = "";
        String sum = "";
        char numberOne = ' ';
        char numberTwo = ' ';
        char help = '0'; // символ, который отвечает за разряд
        if (secondSSResultOne.length() < secondSSResultTwo.length()) {  // если второе число больше первого по длине, то меняем их местами
            tmp = secondSSResultOne;
            secondSSResultOne = secondSSResultTwo;
            secondSSResultTwo = tmp;
        }

        for (int i = 0; i < secondSSResultOne.length(); i++) { // цикл для посимвольного сложения

            numberOne = secondSSResultOne.charAt(i); // первая цифра
            if (i < secondSSResultTwo.length()) numberTwo = secondSSResultTwo.charAt(i); // вторая цифра
            else {
                numberTwo = '0'; // вторая цифра
            }

            if (i == secondSSResultOne.length() - 1 && help == '1' && numberOne == '1' && numberTwo == '1') {
                sum = "11" + sum;
                break;
            } else if (i == secondSSResultOne.length() - 1 && numberOne == '1' && numberTwo == '1' && help == '0') {
                sum = "10" + sum;
                break;
            } else if (i == secondSSResultOne.length() - 1 && numberOne == '1' && numberTwo == '0' && help == '1') {
                sum = "10" + sum;
                break;
            } else if (numberOne == '1' && numberTwo == '1' && help == '1') {
                sum = "1" + sum;
                help = '1';
            } else if ((((numberOne == '1' && numberTwo == '0') || (numberOne == '0' && numberTwo == '1')) && help == '1') || (numberOne == '1' && numberTwo == '1' && help == '0')) {
                sum = "0" + sum;
                help = '1';
            } else if (((numberOne == '1' && numberTwo == '0') || (numberOne == '0' && numberTwo == '1') && help == '0') || (numberOne == '0' && numberTwo == '0' && help == '1')) {
                sum = "1" + sum;
                help = '0';
            } else if (numberOne == '0' && numberTwo == '0' && help == '0') {
                sum = "0" + sum;
                help = '0';
            }
        }
        return sum;
    }

    public static String subtraction(String notFinalResultOne, String notFinalResultTwo, Integer firstN, Integer secondN) {
        String tmp = "";
        String misstake = ""; // массив символов, для удаления нулей
        boolean flag = false; // для удаления нулей
        char symbol = ' '; // для удаления нулей
        char numberOne = ' ';
        char numberTwo = ' ';
        char help = '0'; // переменная для запоминмания вычитания единицы из нуля
        char mark = ' '; //для отр чисел
        String difference = "";

        if (notFinalResultOne.length() < notFinalResultTwo.length() || firstN < secondN) {  // меняем места
            tmp = notFinalResultOne;
            notFinalResultOne = notFinalResultTwo;
            notFinalResultTwo = tmp;
            mark = '-';
        }

        for (int i = 0; i < notFinalResultOne.length(); i++) {
            numberOne = notFinalResultOne.charAt(i); // первая цифра
            if (i < notFinalResultTwo.length()) numberTwo = notFinalResultTwo.charAt(i); // вторая цифра
            else {
                numberTwo = '0';
            }

            if (numberOne == '0' && numberTwo == '1' && help == '1') {
                difference = "0" + difference;
                help = 1;
            } else if ((numberOne == '0' && numberTwo == '0' && help == '1') || (numberOne == '1' && numberTwo == '1' && help == '1') || (numberOne == '0' && numberTwo == '1' && help == '0')) {
                difference = "1" + difference;
                help = '1';
            } else if ((numberOne == '1' && numberTwo == '1' && help == '0') || (numberOne == '1' && numberTwo == '0' && help == '1') || (numberOne == '0' && numberTwo == '0' && help == '0')) {
                difference = "0" + difference;
                help = '0';
            } else if (numberOne == '1' && numberTwo == '0' && help == '0') {
                difference = "1" + difference;
                help = '0';
            }

        }
        for (int j = 0; j < difference.length(); j++) { // удаления незначащих нулей
            symbol = difference.charAt(j);
            if (symbol == '1') {
                flag = true;
            }
            if (flag) {
                misstake = misstake + symbol;
            }
        }
        misstake = mark + misstake;
        return misstake;
    }
}

