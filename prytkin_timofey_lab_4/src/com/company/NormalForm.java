package com.company;

public class NormalForm {

    private final int countOfRows;
    private final int countOfColumns;
    public final String[] variables= {"X1", "X2", "X3", "X4" };

    public NormalForm(int countOfRows, int countOfColumns){
        this.countOfRows = countOfRows;
        this.countOfColumns = countOfColumns;
    }

    public void convertToDisjunctiveNormalForm(int[][] truthTable) {
        boolean printPlus = false;

        for (int i = 0; i < countOfRows; ++i) {
            if (truthTable[i][countOfColumns - 1] == 1) {

                if (printPlus) { System.out.print(" + "); }

                System.out.print("(");
                printPlus = true;

                for (int j = 0; j < countOfColumns - 1; ++j) {
                    if (truthTable[i][j] == 0) { System.out.print("!"); }
                    System.out.print(variables[j]);
                    if (j < countOfColumns - 2) { System.out.print(" * "); }
                    else { System.out.print(")"); }
                }
            }
        }
    }

    public void convertToConjunctiveNormalForm(int[][] truthTable) {
        boolean printStar = false;

        for (int i = 0; i < countOfRows; ++i) {
            if (truthTable[i][countOfColumns - 1] == 0) {

                if (printStar) { System.out.print(" * "); }

                System.out.print("(");
                printStar = true;

                for (int j = 0; j < countOfColumns - 1; ++j) {
                    if (truthTable[i][j] == 1) { System.out.print("!"); }
                    System.out.print(variables[j]);
                    if (j < countOfColumns - 2) { System.out.print(" + "); }
                    else { System.out.print(")"); }
                }
            }
        }
    }
}
