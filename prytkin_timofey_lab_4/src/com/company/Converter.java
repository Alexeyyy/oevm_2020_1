package com.company;

public class Converter {

    /**
     * Этот метод вернет число в двоичной системе счисления дополненный спереди нулями до 4-разрядного
     * @param numberDecimal Число в десятичной системае счисления
     */
    public static String convertToBinary (int numberDecimal) {
        int finalSystem = 2;
        StringBuilder result = new StringBuilder();
        long remainder;

        for (String currentChar; numberDecimal > 0; numberDecimal /= finalSystem) {
            remainder = numberDecimal % (long) finalSystem;
            currentChar = Long.toString(remainder);
            result.insert(0, currentChar);
        }

        //Дополним нулями
        while (result.length() < 4) { result.insert(0, '0'); }
        return result.toString();
    }
}
