/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_5;
import java.util.Random;

/**
 *
 * @author Ной
 */
public class MyClass {
    public static int[][] generateArray() {
        Random r = new Random();
        return new int[][]{
            {0, 0, 0, 0, r.nextInt(2)},
            {0, 0, 0, 1, r.nextInt(2)},
            {0, 0, 1, 0, r.nextInt(2)},
            {0, 0, 1, 1, r.nextInt(2)},
            {0, 1, 0, 0, r.nextInt(2)},
            {0, 1, 0, 1, r.nextInt(2)},
            {1, 1, 1, 0, r.nextInt(2)},
            {0, 1, 1, 1, r.nextInt(2)},
            {1, 0, 0, 0, r.nextInt(2)},
            {1, 0, 0, 1, r.nextInt(2)},
            {1, 0, 1, 0, r.nextInt(2)},
            {1, 0, 1, 1, r.nextInt(2)},
            {1, 1, 0, 0, r.nextInt(2)},
            {1, 1, 0, 1, r.nextInt(2)},
            {1, 1, 1, 0, r.nextInt(2)},
            {1, 1, 1, 1, r.nextInt(2)},};
    }

    public static String KNForm(int[][] RMatriz) {
        String output = new String();
        for (int[] n : RMatriz) {
            if (n[4] == 0) {
                if (output.isEmpty()) {
                    output += ((n[0] == 0) ? "  (x1 + " : " (!x1 + ");
                } else {
                    output += ((n[0] == 0) ? "\n * (!x1 + " : "\n * (!x1 + ");
                }
                output += ((n[1] == 0) ? " x2 + " : " !x2 + ");
                output += ((n[2] == 0) ? " x3 + " : " !x3 + ");
                output += ((n[3] == 0) ? " x4)  " : " !x4)  ");
            }
        }
        return output;
    }

    public static String DNForm(int[][] RMatriz) {
        String output = new String();
        for (int[] n : RMatriz) {
            if (n[4] == 1) {
                if (output.isEmpty()) {
                    output += ((n[0] == 0) ? " ( !x1 * " : "( x1 * ");
                } 
                else {
                    output += ((n[0] == 0) ? "\n + ( !x1 * " : "\n + ( x1 * ");
                }
                output += ((n[1] == 0) ? " !x2 * " : " x2 * ");
                output += ((n[2] == 0) ? " !x3 * " : " x3 * ");
                output += ((n[3] == 0) ? " !x4) " : " x4)  ");
            }
        }
        return output;
    }
}
