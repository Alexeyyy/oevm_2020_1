/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_5;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Ной
 */
public class FormView {

    public static boolean knform;
    public static boolean dnform;
    public final JTextArea output = new JTextArea();
    public final int[][] arrayForVisualization = MyClass.generateArray();
    public final JPanel Tab = new Draw(arrayForVisualization);
    public JFrame jf;
    public final JButton button_KNForm = new JButton("0");
    public final JButton button_DNForm = new JButton("1");

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    FormView window = new FormView();
                    window.jf.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public FormView() {
        initialize();
    }

    /**
     * Initialize the contents of the jf.
     */
    private void initialize() {

        jf = new JFrame();
        jf.setBounds(120, 120, 600, 680);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.getContentPane().setLayout(null);
        jf.setVisible(true);
        Tab.setBounds(0, 40, 180, 638);
        jf.getContentPane().add(Tab);
        JTextArea textArea = new JTextArea();
        Tab.add(textArea);
        output.setBounds(200, 30, 200, 220);
        output.setEditable(false);
        jf.getContentPane().add(output);

        button_KNForm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                button_KNForm.setEnabled(true);
                button_DNForm.setEnabled(false);
                knform = true;
                output.setText((MyClass.DNForm(arrayForVisualization)));
                Tab.repaint();
            }
        });

        button_KNForm.setBounds(226, 325, 50, 42);
        jf.getContentPane().add(button_KNForm);
        button_DNForm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                button_DNForm.setEnabled(true);
                button_KNForm.setEnabled(false);
                dnform = true;
                output.setText((MyClass.KNForm(arrayForVisualization)));
                Tab.repaint();
            }
        });
        button_DNForm.setBounds(282, 325, 50, 42);
        jf.getContentPane().add(button_DNForm);
    }
}
