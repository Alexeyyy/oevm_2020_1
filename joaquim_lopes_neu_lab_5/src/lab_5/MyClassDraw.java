/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_5;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Ной
 */
public class MyClassDraw {
    public static int x = 12;
    public static int y = 12;
    public static int wCell = 30;
    public static int hCell = 30;
    public static final int tRow = 16;
    public static final int tColumn = 5;

    public static void drawKNForm(int[][] matriz, Graphics g) {
        for (int i = 0; i < tRow; i++) {
            if (matriz[i][tColumn - 1] == 0) {
                g.setColor(Color.black);
            } 
            else {
                g.setColor(Color.red);
            }
            for (int k = 0; k < tColumn; k++) {
                g.drawRect(x + wCell * k, y + hCell * i, wCell, hCell);
                g.drawString(matriz[i][k] + "", x + 10 + wCell * k, y + 20 + hCell * i);
            }
        }
    }

    public static void drawDNForm(int[][] matriz, Graphics g) {
        for (int i = 0; i < tRow; i++) {
            if (matriz[i][tColumn - 1] == 1) {
                g.setColor(Color.black);
            } 
            else {
                g.setColor(Color.red);
            }
            for (int k = 0; k < tColumn; k++) {
                g.drawRect(x + wCell * k, y + hCell * i, wCell, hCell);
                g.drawString(matriz[i][k] + "", x + 10 + wCell * k, y + 20 + hCell * i);
            }
        }
    }

    public static void dTable(int[][] matriz, String signal, Graphics g) {
        for (int i = 0; i < tColumn - 1; i++) {
            g.drawRect(x + wCell * i, y, wCell, hCell);
            g.drawString("X" + (i + 1), x * 2 + wCell * i, y * 3);
        }
        g.drawRect(x + wCell * (tColumn - 1), y, wCell, hCell);
        g.drawString("F", x + 10 + wCell * (tColumn - 1), y + 20);
        y += 30;
        for (int i = 0; i < tRow; i++) {
            for (int k = 0; k < tColumn; k++) {
                g.drawRect(x + wCell * k, y + hCell * i, wCell, hCell);
                g.drawString(matriz[i][k] + "", x + 10 + wCell * k, y + 20 + hCell * i);
            }
        }
        if (FormView.knform) {
            drawKNForm(matriz, g);
        } 
        else if (FormView.dnform) {
            drawDNForm(matriz, g);
        }
        y -= 30;
    }
}
