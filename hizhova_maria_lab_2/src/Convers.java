import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Convers {
    private static final String symbols = "0123456789ABCDEF";

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int error = 0;
        int originalNumberSistem = 0;
        originalNumberSistem=inputOfInitial(br,error,originalNumberSistem);
        char[] symbolsChar = symbols.substring(0, originalNumberSistem).toCharArray();
        String convertibleNumber = "";
        convertibleNumber=numberInput(br,error,convertibleNumber,symbolsChar);
        int resultingNumberSystem = 0;
        resultingNumberSystem=inputReceived(br,error,resultingNumberSystem);
        String S = "";
        long char10 = rebase(convertibleNumber, originalNumberSistem);
        long charInt = (int) char10;
        System.out.print("Получилось число в выбранной СС:" + convert(charInt, resultingNumberSystem, 0, ""));
    }

    public static int inputOfInitial(BufferedReader br,int error,int originalNumberSistem){
        do {
            try {
                System.out.print("Введите основание исходной системы счисления: ");
                originalNumberSistem = Integer.parseInt(br.readLine());
                if (originalNumberSistem > 1 && originalNumberSistem < 17) {
                    error = 0;
                } else {
                    System.err.println("Основание системы должно находиться в диапазоне от 2 до 16 включительно");
                    error = 1;
                }
            } catch (Exception e) {
                System.err.println("Неверный формат основания системы");
                error = 1;
            }
        }
        while (error == 1);
        return originalNumberSistem;
    }
    public static String numberInput(BufferedReader br,int error,String convertibleNumber,char[] symbolsChar) throws IOException {
        do {
            System.out.print("Введите число для конвертирования: ");
            convertibleNumber = br.readLine().toUpperCase();
            char[] convertibleNumberArray =convertibleNumber.toCharArray();
            for (char Num :convertibleNumberArray) {
                if ((Arrays.binarySearch(symbolsChar, Num) >= 0)) {
                    error = 0;
                }
            }
            if (convertibleNumber.length() <= 0) {
                System.err.println("Введите значение");
                error = 1;
            }
        }
        while (error == 1);
        return convertibleNumber;
    }

    public static int inputReceived(BufferedReader br,int error,int  resultingNumberSystem){
        do {
            System.out.print("Введите основание получаемой системы счисления: ");
            try {
                resultingNumberSystem = Integer.parseInt(br.readLine());
                if (resultingNumberSystem > 1 &&resultingNumberSystem < 17) {
                    error = 0;
                } else {
                    System.err.println("Основание системы должно находиться в диапазоне от 2 до 16 включительно");
                    error = 1;
                }
            } catch (Exception e) {
                System.err.println("Неверный формат основания системы");
                error = 1;
            }
        }
        while (error == 1);
        return resultingNumberSystem;
    }
    public static long rebase(String number, int base) {
        long result = 0;
        int position = number.length();
        for (char ch : number.toCharArray()) {
            int value = symbols.indexOf(ch);
            result += value * pow(base, --position);
        }
        return result;
    }

    private static long pow(int value, int x) {
        if (x == 0) return 1;
        return value * pow(value, x - 1);
    }

    private static String convert(long charint, int base, int position, String result) {
        char[] symbolsC = symbols.toCharArray();
        if (charint < Math.pow(base, position + 1)) {
            return symbolsC[(int) (charint / (int) Math.pow(base, position))] + result;
        } else {
            int remainder = (int) (charint % (int) Math.pow(base, position + 1));
            return convert(charint - remainder, base, position + 1, symbolsC[remainder / (int) (Math.pow(base, position))] + result);
        }
    }

}
