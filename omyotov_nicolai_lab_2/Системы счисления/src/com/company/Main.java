package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Converter converter = new Converter();
        Scanner sc = new Scanner(System.in);
        Helper helper = new Helper();
        helper.checkBase("Укажите исходную систему счисления (2-16):", sc, converter, true);
        helper.checkBase("Укажите конечную систему счисления (2-16):", sc, converter, false);
        helper.checkNum("Укажите число в исходной системе счисления (целое):", sc, converter);
        converter.convert();
        System.out.println("Исходная с/с: " + converter.getBaseIn());
        System.out.println("Конечная с/с: " + converter.getBaseOut());
        System.out.println("Исходное число в " + converter.getBaseIn() +" с/с: "+converter.getNumIn());
        System.out.println("Конечное число в " + converter.getBaseOut() +" с/с: "+converter.getNumOut());
    }
}
