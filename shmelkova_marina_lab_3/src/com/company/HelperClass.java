package com.company;

public class HelperClass {
    public static void checkForBugs(long ss1, char number[]) { // Функция проверки на соответствие числа системе счисления
        int i = 0;
        long digit = 0;
        while(i < number.length) {
            if(number[i] >= '0' && number[i] <= '9') {
                digit = (long) (number[i] - '0');
            }
            else if(number[i] >= 'A' && number[i] <= 'Z'){
                digit = (long) (number[i] - 'A' + 10);
            }
            if (digit >= ss1 ) {
                System.out.println("Некоректный ввод данных");
                System.exit(0);
            }
            i++;
        }
    }

    public static long convertTo10Base(char[] number, long system) { // Фуннкция перевода чисел в десятичную систему счисления
        long result = 0;
        int count = number.length - 1;
        int symbol = 0;
        for (int i = 0; i < number.length; i++) {
            if (number[i] >= '0' && number[i] <= '9') {
                symbol = number[i] - '0';
            } else if (Character.isLetter(number[i])) {
                symbol = number[i] - 'A' + 10;
            }
            result = (long) (result + symbol * Math.pow(system, count));
            count--;
        }
        return result;
    }

    public static String convertToFinalBase(long number) { // Функция перевода чисел в двоичную систему счисления
        String result = "";
        long balance = 0;
        while(number > 0){
            balance = number % 2;
            number = number / 2;
            if(number % 2 < 10){
                result = Long.toString(balance) + result;
            }
            else {
                result = (char)((int)('A' + balance - 10)) + result;
            }
        }
        return result;
    }
}
