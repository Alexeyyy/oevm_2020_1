package com.company;
import java.awt.*;
import java.io.IOException;
import java.util.Scanner;
import java.lang.StringBuffer;
import java.lang.Integer;

public class Main {
    public static void main(String[] args)  throws IOException{
        // write your code here
        System.out.println("Ввелите изначальное основание: "); // обычный вывод на консоль, ничего необычного
        Scanner original = new Scanner(System.in); // сканер сканирует (считывает)
        int initialNotation = original.nextInt(); // вводим изначальное основание

        System.out.println("Первое число: "); // обычный вывод на консоль, ничего необычного
        Scanner number = new Scanner(System.in); // сканер сканирует (считывает)
        String firstNumber = number.nextLine(); // ввожу первое число
        String firstResult = new StringBuffer(firstNumber).reverse().toString(); // строка, в которой запишу перевёрнутую изначальную строку

        System.out.println("Второе число: ");
        Scanner secNumber = new Scanner(System.in);
        String secondNumber = secNumber.nextLine(); // ввожу второе число
        String secondRusult = new StringBuffer(secondNumber).reverse().toString(); // переворачиваю второе число

        System.out.println("Введите операцию: "); // ввожу операцию
        char operation = (char) System.in.read();

        long firstFinalNumber = 0; // число для перевода в двоичную
        long secondFinalNumber = 0; // число для перевода в двоичную
        String notFinalResultOne = ""; // число, переведённое в двоичную систему
        String notFinalResultTwo = ""; // число, переведённое в двоичную систему
        String composition = notFinalResultOne; // произведение
        String divide = notFinalResultOne; // деление
        String division = ""; // результат деления нацело
        long cnt = 0; // счётчик для деления

        firstFinalNumber = Calculations.numberToTen(firstResult, firstFinalNumber, initialNotation);// первое число в 10-ной системе
        secondFinalNumber = Calculations.numberToTen(secondRusult, secondFinalNumber, initialNotation);// второе число в 10-ной

        notFinalResultOne = Calculations.convert(firstFinalNumber, notFinalResultOne); // первое число в двоичной системе
        String s1 = new StringBuffer(notFinalResultOne).reverse().toString();;
        System.out.println("Первое число: "+s1);

        notFinalResultTwo = Calculations.convert(secondFinalNumber, notFinalResultTwo); // второе число в двоичной системе
        String s2 = new StringBuffer(notFinalResultTwo).reverse().toString();;
        System.out.println("Второе число: "+s2);

        Integer firstN = Integer.valueOf(s1); // 1 число для проверки
        Integer secondN = Integer.valueOf(s2); // 2 число для проверки


        if (operation == '+') {
            String finalSum = Calculations.addition(notFinalResultOne, notFinalResultTwo);
            System.out.println("Сумма = "+finalSum);
        }
        else if (operation == '-') {
            String finalDifference = Calculations.subtraction(notFinalResultOne, notFinalResultTwo, firstN, secondN);
            System.out.println("Разность = "+finalDifference);
        }

        else if(operation == '*') {
            for (long j = 0; j < secondFinalNumber; j++) {
                composition = Calculations.addition(composition, notFinalResultOne);
                composition = new StringBuffer(composition).reverse().toString();
            }
            composition = new StringBuffer(composition).reverse().toString();
            System.out.println("Умножение = "+composition);
        }

        else if(operation == '/') {
            for (long j = firstFinalNumber; j >= secondFinalNumber; j -= secondFinalNumber) {
                divide = Calculations.subtraction(divide, notFinalResultOne, firstN, secondN);
                divide = new StringBuffer(divide).reverse().toString();
                cnt++;
            }
            division = Calculations.convert(cnt, division);
            division = new StringBuffer(division).reverse().toString();
            System.out.println("Деление = "+division);
        }
    }
}
