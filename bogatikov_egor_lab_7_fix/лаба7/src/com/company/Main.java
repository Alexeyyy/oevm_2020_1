package com.company;

public class Main {

    public static void main(String[] args) {
        Pascal Pascal = new Pascal();

        Assembler assembler = new Assembler();

        Parser Parser = new Parser(Pascal.read(), assembler);
        Parser.parseBody();
        assembler.create();
    }
}
