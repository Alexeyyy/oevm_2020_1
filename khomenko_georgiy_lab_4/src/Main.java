import java.util.*;

public class Main {
    public static void main(String[] args) {
        {
            McClusky calc = new McClusky();
            System.out.println("Введите 1 если нада днф 2 если кнф");
            Scanner scanner = new Scanner(System.in);
            String answer = "";
            int temp = scanner.nextInt();
            scanner.close();
            long e = System.nanoTime();
            if (1 == temp) {
                answer = calc.calcDNF();
            } else if (2 == temp) {
                answer = calc.calcKNF();
            } else {
                System.out.println("Вы ошиблись попробуйте снова");
                System.exit(1);
            }
            System.out.println(answer);
            System.out.println((System.nanoTime() - e) + "");
        }
    }
}