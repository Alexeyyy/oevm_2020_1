import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {

    private int[][] array;
    private int stringSize = 16;
    private int columnSize = 5;
    private int indent = 10;
    private int sizeSquare = 30;
    private int bounce = 10;

    private boolean KNF = false;
    private boolean DNF = false;

    public void setArray(int[][] arr) {
        this.array = arr;
    }

    public void paint(Graphics g) {
        super.paint(g);
        for (int i = 0; i < stringSize; i++) {
            for (int j = 0; j < columnSize; j++) {
                g.setColor(Color.BLACK);
                if(KNF){
                    if (array[i][columnSize-1] == 0){
                        g.setColor(Color.GREEN);
                    }
                }
                if(DNF) {
                    if (array[i][columnSize - 1] == 1) {
                        g.setColor(Color.GREEN);
                    }
                }
                g.drawString(array[i][j] + "",bounce  * 2 + j * sizeSquare,bounce * 3  + i * sizeSquare);
                g.setColor(Color.BLACK);
                g.drawRect(indent + j * sizeSquare, indent + i * sizeSquare, sizeSquare, sizeSquare);
            }
        }
    }

    public void setDNF(){
        DNF = true;
        KNF = false;
    }

    public void setKNF(){
        KNF = true;
        DNF = false;
    }
}