package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите начальную систему счисления:");
        int initialNotation = sc.nextInt();
        System.out.println("Введите конечную систему счисления:");
        int finiteNotation = sc.nextInt();
        System.out.println("Введите Число:");
        sc.nextLine();
        String number = sc.nextLine();
        Convertor convertor = new Convertor();
        convertor.convertToFiniteNotation(initialNotation, finiteNotation, number);
    }
}