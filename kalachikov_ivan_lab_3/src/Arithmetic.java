public class Arithmetic {

    //суммировать
    public static String summarize(String digit1, String digit2) {
        boolean minus1 = false;
        boolean minus2 = false;

        if (digit1.toCharArray()[0] == '(') {
            digit1 = convertToAdditionalCode(digit1);
            minus1 = true;
        }
        if (digit2.toCharArray()[0] == '(') {
            digit2 = convertToAdditionalCode(digit2);
            minus2 = true;
        }

        return sumRealize(digit1, digit2, minus1, minus2);
    }

    //вычитать
    public static String subtract(String digit1, String digit2) {
        boolean minus1 = false;
        boolean minus2 = false;

        if (digit1.toCharArray()[0] == '(') {
            digit1 = convertToAdditionalCode(digit1);
            minus1 = true;
        }
        if (digit2.toCharArray()[0] == '(') {
            digit2 = removeBrackets(digit2);
        } else {
            digit2 = convertToAdditionalCode(digit2);
            minus2 = true;
        }

        return sumRealize(digit1, digit2, minus1, minus2);
    }

    //умножить
    public static String multiply(String digit1, String digit2) {
        boolean minus1 = false;
        boolean minus2 = false;

        if (digit1.toCharArray()[0] == '(') {
            digit1 = removeBrackets(digit1);
            minus1 = true;
        }
        if (digit2.toCharArray()[0] == '(') {
            digit2 = removeBrackets(digit2);
            minus2 = true;
        }

        char[] factor = new StringBuilder(digit2).reverse().toString().toCharArray();

        String result = "0";
        StringBuilder zeros = new StringBuilder();

        for (char c : factor) {
            if (c == '1') result = sumRealize(result, digit1 + zeros.toString(), false, false);
            zeros.append('0');
        }
        if ((minus1 || minus2) && !(minus1 && minus2)) {
            return '-' + result;
        }

        return result;
    }

    //разделить
    public static String divide(String digit1, String digit2) {
        boolean minus1 = false;
        boolean minus2 = false;

        if (digit1.toCharArray()[0] == '(') {
            digit1 = removeBrackets(digit1);
            minus1 = true;
        }
        if (digit2.toCharArray()[0] == '(') {
            digit2 = removeBrackets(digit2);
            minus2 = true;
        }

        if (digit2.equals("0")) return "Деление на 0";

        char[] digit1char = digit1.toCharArray();
        StringBuilder temp = new StringBuilder();
        StringBuilder result = new StringBuilder();

        for (char c : digit1char) {
            temp.append(c);
            String residue = subtract(temp.toString(), digit2);
            if (residue.toCharArray()[0] == '-') {
                result.append('0');
            } else {
                result.append('1');
                temp = new StringBuilder(subtract(temp.toString(), digit2));
            }
        }

        String resultString;
        if (result.indexOf("1") != -1) {
            resultString = result.substring(result.indexOf("1"), result.length());
            if ((minus1 || minus2) && !(minus1 && minus2)) {
                resultString =  '-' + resultString;
            }
        } else resultString = "0";

        return resultString;
    }

    private static String removeBrackets(String digit) {
        StringBuilder stringBuilder = new StringBuilder(digit);
        stringBuilder.delete(0, 1);
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    private static String convertToAdditionalCode(String digit) {
        if (digit.toCharArray()[0] == '(') {
            digit = removeBrackets(digit);
        }
        if (digit.equals("0")) return digit;

        digit = digit.replace('1', 't');
        digit = digit.replace('0', '1');
        digit = digit.replace('t', '0');
        digit = sumRealize(digit, "1", false, false);
        digit = new StringBuilder("11111111").insert(8 - digit.length(), digit).delete(8, 16).toString();

        return digit;
    }

    private static String convertToNormalForm(String digit) {
        StringBuilder stringBuilder = new StringBuilder(digit);
        if (digit.length() > 8)
            stringBuilder.deleteCharAt(0);
        if (stringBuilder.indexOf("1") != -1)
            digit = stringBuilder.substring(stringBuilder.indexOf("1"), stringBuilder.length());
        else digit = "0";

        return digit;
    }

    private static String convertFromAddToDirect(String digit) {
        digit = convertToNormalForm(summarize(convertToNormalForm(digit), "(-1)"));
        digit = digit.replace('1', 't');
        digit = digit.replace('0', '1');
        digit = digit.replace('t', '0');
        digit = digit.substring(digit.indexOf('1'));
        return digit;
    }

    private static String sumRealize(String digit1, String digit2, boolean minus1, boolean minus2) {
        int lengthSmallerDigit = Math.min(digit1.length(), digit2.length());
        int lengthBiggerDigit = Math.max(digit1.length(), digit2.length());

        char[] digit1charArray = new StringBuilder(digit1).reverse().toString().toCharArray();
        char[] digit2charArray = new StringBuilder(digit2).reverse().toString().toCharArray();

        StringBuilder result = new StringBuilder();
        boolean plusone = false;

        for (int i = 0; i < lengthSmallerDigit; i++) {
            if (digit1charArray[i] == '1' && digit2charArray[i] == '1') {
                if (plusone) {
                    result.append('1');
                } else {
                    result.append('0');
                    plusone = true;
                }
            } else if (digit1charArray[i] == '1' || digit2charArray[i] == '1') {
                if (plusone) {
                    result.append('0');
                } else {
                    result.append('1');
                    plusone = false;
                }
            } else if (digit1charArray[i] == '0' && digit2charArray[i] == '0') {
                if (plusone) {
                    result.append('1');
                    plusone = false;
                } else {
                    result.append('0');
                }
            }
        }

        char[] biggerDigit;
        if (digit1.length() > digit2.length()) {
            biggerDigit = digit1charArray;
        } else {
            biggerDigit = digit2charArray;
        }

        for (int i = lengthSmallerDigit; i < lengthBiggerDigit; i++) {
            if (biggerDigit[i] == '1') {
                if (plusone) {
                    result.append('0');
                } else {
                    result.append('1');
                }
            } else if (biggerDigit[i] == '0') {
                if (plusone) {
                    result.append('1');
                    plusone = false;
                } else {
                    result.append('0');
                }
            }
        }

        if (plusone) result.append('1');
        String resultString = result.reverse().toString();

        if (resultString.length() > 8 && (minus1 && minus2)) {
            resultString = '-' + convertFromAddToDirect(resultString);
        } else if (minus1 || minus2) {
            if (resultString.length() == 8) {
                resultString = '-' + convertFromAddToDirect(convertToNormalForm(resultString));
            } else {
                resultString = convertToNormalForm(resultString);
            }
        }

        return resultString;
    }
}
