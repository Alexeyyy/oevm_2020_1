package com.volodya

fun isOver(str1: String, str2: String): String {
    if (str1.length > str2.length) {
        return str1
    }
    return str2
}

fun isBigger(str1: String, str2: String): Boolean {
    if(toDecimal(str1, 2) > toDecimal(str2, 2)) {
        return true
    }
    return false
}