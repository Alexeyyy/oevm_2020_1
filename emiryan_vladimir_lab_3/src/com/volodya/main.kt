package com.volodya

import java.util.*

fun main() {
    println("Введите систему счисления:")
    val scanner = Scanner(System.`in`)
    val systemNumber: Int = scanner.nextInt()
    println("Введите два числа:")
    val digitString = readLine()?.split(' ')
    var firstDigit = digitString?.get(0)
    var secondDigit = digitString?.get(1)
    println("Выбрите опреацию:")
    println("1: Сложение")
    println("2: Вычитание")
    println("3: Умножение")
    println("4: Деление")
    val choice: Int = scanner.nextInt()
    when(choice) {
        1 -> print("Сумма чисел = " + sum(toX(toDecimal(firstDigit.toString(), systemNumber), 2), toX(toDecimal(secondDigit.toString(), systemNumber), 2)))
        2 -> print("Разность чисел = " + diff(toX(toDecimal(firstDigit.toString(), systemNumber), 2), toX(toDecimal(secondDigit.toString(), systemNumber), 2)))
        3 -> print("Произведение чисел = " + multiply(toX(toDecimal(firstDigit.toString(), systemNumber), 2), toX(toDecimal(secondDigit.toString(), systemNumber), 2)))
        4 -> print("Частное чисел = " + divide(toX(toDecimal(firstDigit.toString(), systemNumber), 2), toX(toDecimal(secondDigit.toString(), systemNumber), 2)))
    }
}