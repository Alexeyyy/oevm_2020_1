format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable
		number db 'ENTER X: ', 0
		number1 db 'ENTER Y: ', 0



	enterNum db '%d', 0


	X dd ?
	Y dd ?

	sum db 'X + Y = %d', 0dh, 0ah, 0
	subtract  db 'X - Y = %d', 0dh, 0ah, 0
	mut db 'X * Y = %d', 0dh, 0ah, 0
	dividing db 'X / Y = %d', 0dh, 0ah, 0

	NULL = 0

section '.idata' import data readable
	library kernel, 'kernel32.dll',\
		msvctr, 'msvcrt.dll'



	import msvctr,\
	       printf, 'printf',\
	       getch, '_getch',\
	       scanf, 'scanf'


		import kernel,\
	       ExitProcess, 'ExitProcess'




section '.code' code readable executable

	start:
		push number
		call [printf]
		push X
		push enterNum
		call [scanf]




		push number1
		call [printf]
		push Y
		push enterNum
		call [scanf]



		mov ecx, [X]
		add ecx, [Y]
		push ecx
		push  sum
		call [printf]


		mov ecx, [X]
		sub ecx, [Y]
		push ecx
		push subtract
		call [printf]


		mov ecx, [X]
		imul ecx, [Y]
		push ecx
		push  mut
		call [printf]






		mov eax, [X]
		cdq
		mov ecx, [Y]
		idiv ecx
		push eax
		push dividing
		call [printf]





		call [getch]

		push NULL
		call [ExitProcess]
				   
