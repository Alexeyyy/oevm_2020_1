# Лабораторная работа №8

## ПИбд-21 Овсянников Кирилл

##### Техническое задание: 

Физические компоненты ЭВМ.

Разобрать и собрать системный блок ЭВМ.

_Таблица комплектующих_

| № | Комплектующие пк | 
|:-:|:----------------:|
| 1 | **Процессор:** QuadCore AMD Phenom II X4 Black Edition 955, 3200 MHz (16 x 200). |
| 2 | **Материнская плата:** Gigabyte GA-880GM-UD2H (2 PCI, 1 PCI-E x1, 1 PCI-E x16, 4 DDR3 DIMM, Audio, Video, Gigabit LAN, IEEE-1394). |
| 3 | **Видеокарта:** NVIDIA GeForce GTX 1050 (2 ГБ). |
| 4 | **Оперативная память:** x2 * Kingston 9905471-001.A00LF 2 ГБ DDR3-1333 DDR3 SDRAM (9-9-9-24 @ 666 МГц) (8-8-8-22 @ 609 МГц) (7-7-7-20 @ 533 МГц) (6-6-6-17 @ 457 МГц). |
| 5 | **Жесткий диск:** ST3500418AS ATA Device (500 ГБ, 7200 RPM, SATA-II). |

 ### До разбора 
https://yadi.sk/i/eHutpKr11JlLMg

https://yadi.sk/i/XfTE-3VTx211-w

 ### После разбора
 
https://yadi.sk/i/wgdd4wvE5EGAMg

 ### Сборка

https://yadi.sk/i/sXYz8ohJtCqi2g


 ### Всё работает

https://yadi.sk/i/7KyjfmtiU_VRlA
