import java.util.Scanner;

public class TransferNotation {
    private int startNotation = 0;
    private int endNotation = 0;
    private String numStartString = "";

    public TransferNotation(int startNotation, int endNotation, String numStartString ){
        this.startNotation = startNotation;
        this.endNotation = endNotation;
        this.numStartString = numStartString;
    }

    private int transferNumIn10Notation(char[] arrChar){
        int cash = 0;
        int degree = 0;
        int numIn10Notation = 0;
        int minNotation = 2;
        int maxNotation = 16;
        int assistan = 10;

        if(startNotation < minNotation || startNotation > maxNotation || endNotation < minNotation || endNotation > maxNotation){
            return -1;
        }

        for(int i = arrChar.length - 1; i >= 0; i--){
            if(arrChar[i] >= '0' && arrChar[i] <= '9'){
                cash = Character.getNumericValue(arrChar[i]);
            } else{
                if(arrChar[i] >= 'A' && arrChar[i] <= 'F'){
                    cash = arrChar[i] - 'A' + assistan;
                } else{
                    return -1;
                }
            }

            if(cash >= startNotation){
                return -1;
            }

            numIn10Notation += cash * Math.pow(startNotation, degree);
            degree++;
        }

        return numIn10Notation;
    }

    private StringBuilder transferNumInEndSS(int numIn10Notation){
        int assistan = 10;
        int assistan2 = 9;
        StringBuilder endNumStr = new StringBuilder();

        if(numIn10Notation == 0){
            endNumStr.append(0);
            return endNumStr;
        }

        while(numIn10Notation != 0){
            if(numIn10Notation % endNotation > assistan2) {
                endNumStr.append((char)(numIn10Notation - assistan + 'A'));
            } else{
                endNumStr.append(numIn10Notation % endNotation);
            }
            numIn10Notation = numIn10Notation / endNotation;
        }

        endNumStr.reverse();
        return endNumStr;
    }

    public void transferNotation(){
        char[] arrChar = numStartString.toCharArray();
        int numIn10Notation = 0;
        StringBuilder endNumStr;

        numIn10Notation = transferNumIn10Notation(arrChar);

        if(numIn10Notation == -1){
            System.out.println("Ошибка");
            return;
        }

        endNumStr = transferNumInEndSS(numIn10Notation);
        System.out.println(endNumStr);
    }
}