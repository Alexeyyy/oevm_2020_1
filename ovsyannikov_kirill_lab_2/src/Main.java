import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int startNotation;
        int endNotation;
        String numStartString;

        Scanner in = new Scanner(System.in);

        System.out.print("Введите начальную систему счисления ");
        startNotation = in.nextInt();

        System.out.print("Введите конечную систему счисления ");
        endNotation = in.nextInt();

        System.out.print("Введите число в начальной системе счисления ");
        numStartString = in.next();

        TransferNotation transfer = new TransferNotation(startNotation, endNotation, numStartString);
        transfer.transferNotation();
    }
}
