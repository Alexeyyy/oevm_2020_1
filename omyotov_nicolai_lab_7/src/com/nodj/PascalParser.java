package com.nodj;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class PascalParser {
    private String code = "";
    private final HashMap<String, ArrayList<String>> varsByType = new HashMap<>();
    private final ArrayList<String> programLines = new ArrayList<>();

    PascalParser(String filepath) {
        try {
            code = new String(Files.readAllBytes(Paths.get(filepath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!code.equals("")) {
            parseVars();
            parseProgram();
        }
    }

    private void parseVars() {
        String strNamesVars = code.substring(code.indexOf("var") + 3, code.indexOf("begin"));
        String[] lines = strNamesVars.split(";");
        for (String line : lines) {
            String[] raw = line.trim().split(" ");
            ArrayList<String> vars;
            if (varsByType.containsKey(raw[raw.length - 1])) {
                vars = varsByType.get(raw[raw.length - 1]);
            } else {
                vars = new ArrayList<>();
            }
            for (int i = 0; i < raw.length - 1; i++) {
                String var = raw[i].substring(0, raw[i].length() - 1);
                if (Pattern.matches("[a-zA-Z_][a-zA-Z0-9]*", var)) {
                    vars.add(var);
                }
            }
            if (!raw[raw.length - 1].equals("")) {
                varsByType.put(raw[raw.length - 1], vars);
            }
        }
    }

    public void parseProgram() {
        String strProgram = code.substring(code.indexOf("begin") + 5, code.indexOf("end")).strip();
        String[] raw = strProgram.split(";");
        for (String s : raw) {
            String line = s.trim();
            if (Pattern.matches("[a-zA-Z_].*", line)) {
                programLines.add(line);
            }
        }
    }

    public ArrayList<String> getProgramLines() {
        return programLines;
    }

    public HashMap<String, ArrayList<String>> getVarsByType() {
        return varsByType;
    }
}
