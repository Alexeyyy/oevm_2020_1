package com.nodj;

import java.util.ArrayList;

public class Calculator {
    private char operation;
    private final ArrayList<Integer> firstNum;
    private final ArrayList<Integer> secondNum;
    private String result;
    private boolean isNegative = false;

    Calculator(String firstNum, String secondNum) {
        this.firstNum = parseStringToArrayOfIntegers(firstNum);
        this.secondNum = parseStringToArrayOfIntegers(secondNum);
    }

    private ArrayList<Integer> parseStringToArrayOfIntegers(String s) {
        ArrayList<Integer> arr = new ArrayList<>(s.length());
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '0') {
                arr.add(0);
            } else if (s.charAt(i) == '1') {
                arr.add(1);
            }
        }
        return arr;
    }

    String arrToString(ArrayList<Integer> arr) {
        StringBuilder sb = new StringBuilder();
        for (Integer integer : arr) {
            sb.append(integer);
        }
        return sb.toString();
    }

    public String toString() {
        return new StringBuilder(arrToString(firstNum))
                .append(" ")
                .append(operation)
                .append(" ")
                .append(arrToString(secondNum))
                .append(" = ")
                .append(result)
                .toString();
    }

    public void setOperation(char operation) {
        this.operation = operation;
    }

    public void calculate() {
        switch (operation) {
            case '+':
                Summation summation = new Summation(firstNum, secondNum);
                result = arrToString(summation.sum());
                break;
            case '-':
                Subtraction sub = new Subtraction(firstNum, secondNum);
                result = arrToString(sub.sub());
                if (sub.isNegative) {
                    result = "-" + result;
                }
                break;
            case '*':
                result = arrToString(multi(firstNum, secondNum));
                break;
            case '/':
                result = div(firstNum, secondNum);
                break;
        }
    }

    private String div(ArrayList<Integer> firstNum, ArrayList<Integer> secondNum) {
        ArrayList<Integer> fNum = new ArrayList<>(firstNum);
        ArrayList<Integer> sNum = new ArrayList<>(secondNum);
        boolean isEqual = true;
        if (fNum.get(0) == 0 && sNum.get(0) == 0) {
            return "Error";
        }
        if (sNum.get(0) == 0) {
            return "∞";
        }
        if (fNum.size() == sNum.size()) {
            for (int j = 0; j < firstNum.size(); j++) {
                if (fNum.get(j) > sNum.get(j)) {
                    isEqual = false;
                    break;
                } else if (fNum.get(j) < sNum.get(j)) {
                    return "0";
                }
            }
            if (isEqual) {
                return "1";
            }
        } else if (fNum.size() < sNum.size()) {
            return "0";
        }

        int counter = 0;
        while (!isNegative && fNum.get(0) != 0) {
            Subtraction sub = new Subtraction(fNum, sNum);
            fNum = sub.sub();
            isNegative = sub.isNegative;
            if (!isNegative)
                counter++;
        }
        Converter converter = new Converter();
        converter.setBaseOut(2);
        converter.setBaseIn(10);
        converter.setNumIn(counter + "");
        return converter.convert();
    }

    private ArrayList<Integer> multi(ArrayList<Integer> firstNum, ArrayList<Integer> secondNum) {
        ArrayList<Integer> maxFigs = firstNum.size() > secondNum.size() ? firstNum : secondNum;
        ArrayList<Integer> minFigs = firstNum.size() > secondNum.size() ? secondNum : firstNum;
        ArrayList<Integer> multi = new ArrayList<>();
        multi.add(0);

        for (int j = 0; j < minFigs.size(); j++) {
            if (minFigs.get(j) == 1) {
                ArrayList<Integer> temp = new ArrayList<>(maxFigs);

                for (int i = j; i < minFigs.size() - 1; i++) {
                    temp.add(0);
                }
                Summation summation = new Summation(multi, temp);
                multi = summation.sum();
            }
        }
        return multi;
    }
}
