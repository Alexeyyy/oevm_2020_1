public class NotValidateException extends RuntimeException {
    public NotValidateException() {
        super("Проверьте корректность введенных данных.\nВы можете использовать только символы цифры и буквы от A (a) до J(j)");
    }
}
