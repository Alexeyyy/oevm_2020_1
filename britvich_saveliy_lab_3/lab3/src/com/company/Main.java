package com.company;

import java.util.Scanner;

public class Main {
    static int scale;
    static String firstNumber;
    static String secondNumber;
    static String operation;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        if(input()) {

        }
        else {
            return;
        }

        convertNumber();
    }

    public static boolean input() {

        System.out.println("Введите систему счисления (2-16):");

        scale = scanner.nextInt();

        System.out.println("Введите первое число:");
        firstNumber = scanner.next();

        System.out.println("Введите второе число:");
        secondNumber = scanner.next();

        System.out.println("Введите нужную операцию (+ - * /):");
        operation = scanner.next();
        if(operation.equals("+")  || operation.equals("-") || operation.equals("*") || operation.equals("/")){

        }
        else {
            System.out.println("Вы ввели неправильную операцию :(");
            System.out.println("Не делай этого больше :(");
            return false;
        }
        return true;
    }

    public static void convertNumber() {
        Convertor convertFirstNumber = new Convertor(scale, firstNumber);
        Convertor convertSecondNumber = new Convertor(scale, secondNumber);
        int firstDecimalNumber = convertFirstNumber.convertToDecimal();
        int secondDecimalNumber = convertSecondNumber.convertToDecimal();

        if (firstDecimalNumber == -1 || secondDecimalNumber == -1) {
            System.out.println("Неправильно");
            return;
        }

        String firstBinaryNumber = convertFirstNumber.convertToBinaryScale(firstDecimalNumber).toString();
        String secondBinaryNumber = convertSecondNumber.convertToBinaryScale(secondDecimalNumber).toString();


        OperationClass operationClass = new OperationClass(firstBinaryNumber,secondBinaryNumber);

        if(operation.equals("+") ) {
            System.out.print("A + B = ");
            System.out.println(operationClass.add());
        }
        else if(operation.equals("-")) {
            System.out.print("A - B = ");
            System.out.println(operationClass.deduction());
        }
        else if(operation.equals("*")){
            System.out.print("A * B = ");
            System.out.println(operationClass.multiplication());
        }
        else {
            System.out.print("A / B = ");
            System.out.println(operationClass.division());
        }
    }

}
