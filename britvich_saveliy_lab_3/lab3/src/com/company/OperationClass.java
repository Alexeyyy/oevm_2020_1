package com.company;

public class OperationClass {
    private char[] firstNumber;
    private char[] secondNumber;

    public OperationClass(String firstNumber, String secondNumber) {

        this.firstNumber = new StringBuffer(firstNumber).reverse().toString().toCharArray(); // принимаем и переворачиваем строку для удобного понимания и выполнения задачи
        this.secondNumber = new StringBuffer(secondNumber).reverse().toString().toCharArray();
    }

    private boolean compareNumbers(char[] firstNumber, char[] secondNumber) {
        if (firstNumber.length > secondNumber.length) {
            return true;
        } else if (firstNumber.length < secondNumber.length) {
            return false;
        } else {
            for (int i = firstNumber.length - 1; i >= 0; i--) {
                if (firstNumber[i] == '1' && secondNumber[i] == '0') {
                    return true;
                } else if (firstNumber[i] == '0' && secondNumber[i] == '1') {
                    return false;
                }
            }
        }
        return true;
    }

    public String add() {

        StringBuilder result = new StringBuilder();
        int i = 0;
        int transferNumber = 0;
        int firstDigit = 0;
        int secondDigit = 0;
        char resultStepChar = '0';

        while (i < firstNumber.length || i < secondNumber.length || transferNumber == 1) {

            if (i >= firstNumber.length) {
                firstDigit = 0;
            } else if (firstNumber[i] == '1') {
                firstDigit = 1;
            } else {
                firstDigit = 0;
            }

            if (i >= secondNumber.length) {
                secondDigit = 0;
            } else if (secondNumber[i] == '1') {
                secondDigit = 1;
            } else {
                secondDigit = 0;
            }

            int sum = firstDigit + secondDigit + transferNumber;

            switch (sum) {
                case 0:
                    transferNumber = 0;
                    resultStepChar = '0';
                    break;
                case 1:
                    transferNumber = 0;
                    resultStepChar = '1';
                    break;
                case 2:
                    transferNumber = 1;
                    resultStepChar = '0';
                    break;
                case 3:
                    transferNumber = 1;
                    resultStepChar = '1';
                    break;
            }
            result.append(resultStepChar);
            i++;
        }
        return new String(result.reverse());
    }

    private char[] add(char[] firstNumber, char[] secondNumber) {

        StringBuilder result = new StringBuilder();
        int i = 0;
        int transferNumber = 0;
        int firstDigit = 0;
        int secondDigit = 0;
        char resultStepChar = '0';


        while (i < firstNumber.length || i < secondNumber.length || transferNumber == 1) {

            if (i >= firstNumber.length) {
                firstDigit = 0;
            } else if (firstNumber[i] == '1') {
                firstDigit = 1;
            } else {
                firstDigit = 0;
            }

            if (i >= secondNumber.length) {
                secondDigit = 0;
            } else if (secondNumber[i] == '1') {
                secondDigit = 1;
            } else {
                secondDigit = 0;
            }
            int sum = firstDigit + secondDigit + transferNumber;

            switch (sum) {
                case 0:
                    transferNumber = 0;
                    resultStepChar = '0';
                    break;
                case 1:
                    transferNumber = 0;
                    resultStepChar = '1';
                    break;
                case 2:
                    transferNumber = 1;
                    resultStepChar = '0';
                    break;
                case 3:
                    transferNumber = 1;
                    resultStepChar = '1';
                    break;
            }
            result.append(resultStepChar);
            i++;
        }
        return result.toString().toCharArray();
    }

    public String deduction() {
        int length = firstNumber.length - secondNumber.length;
        if (length < 0) {
            length = 0;
        }
        char[] verbalCode;
        boolean compare = true;
        if (compareNumbers(firstNumber, secondNumber)) {
            verbalCode = new char[firstNumber.length];
        } else {
            verbalCode = new char[secondNumber.length];
            compare = false;
        }
        for (int i = 0; i < length; i++) {
            verbalCode[i + verbalCode.length - length] = '1';
        }

        for (int i = 0; i < verbalCode.length - length; i++) {
            if (secondNumber[i] == '1') {
                verbalCode[i] = '0';
            } else {
                verbalCode[i] = '1';
            }
        }
        char[] additionalCode = add(verbalCode, new char[]{'1'});
        char[] resultAdd = add(additionalCode, firstNumber);
        StringBuilder result = new StringBuilder("");

        if (!compare) {
            result.append("-");
            for (int i = resultAdd.length - 1; i >= 0; i--) {
                if (resultAdd[i] == '1') {
                    resultAdd[i] = '0';
                } else {
                    resultAdd[i] = '1';
                }
            }
            char[] resultMinusAdd = add(resultAdd, new char[]{'1'});

            boolean zero = true;
            for (int i = resultMinusAdd.length - 1; i >= 0; i--) {
                if (resultMinusAdd[i] == '1') {
                    zero = false;
                } else {
                    if (zero) {
                        continue;
                    }
                }
                result.append(resultMinusAdd[i]);
            }

            return result.toString();
        } else {
            if (resultAdd.length > 1) {
                boolean zero = true;
                for (int i = resultAdd.length - 2; i >= 0; i--) {
                    if (resultAdd[i] == '1') {
                        zero = false;
                    } else {
                        if (zero) {
                            continue;
                        }
                    }
                    result.append(resultAdd[i]);
                }
            } else {
                result.append(resultAdd[0]);
            }
        }

        if(result.length() == 0) {
            result.append('0');
        }

        return result.toString();
    }

    public String deduction(char[] firstNumber, char[] secondNumber) {
        int length = firstNumber.length - secondNumber.length;
        if (length < 0) {
            length = 0;
        }
        char[] verbalCode;
        boolean compare = true;
        if (compareNumbers(firstNumber, secondNumber)) {
            verbalCode = new char[firstNumber.length];
        } else {
            verbalCode = new char[secondNumber.length];
            compare = false;
        }
        for (int i = 0; i < length; i++) {
            verbalCode[i + verbalCode.length - length] = '1';
        }

        for (int i = 0; i < verbalCode.length - length; i++) {
            if (secondNumber[i] == '1') {
                verbalCode[i] = '0';
            } else {
                verbalCode[i] = '1';
            }
        }
        char[] additionalCode = add(verbalCode, new char[]{'1'});
        char[] resultAdd = add(additionalCode, firstNumber);
        StringBuilder result = new StringBuilder("");

        if (!compare) {
            result.append("-");
            for (int i = resultAdd.length - 1; i >= 0; i--) {
                if (resultAdd[i] == '1') {
                    resultAdd[i] = '0';
                } else {
                    resultAdd[i] = '1';
                }
            }
            char[] resultMinusAdd = add(resultAdd, new char[]{'1'});

            boolean zero = true;
            for (int i = resultMinusAdd.length - 1; i >= 0; i--) {
                if (resultMinusAdd[i] == '1') {
                    zero = false;
                } else {
                    if (zero) {
                        continue;
                    }
                }
                result.append(resultMinusAdd[i]);
            }
            return (new StringBuffer(result.toString()).reverse()).toString();
        } else {
            if (resultAdd.length > 1) {
                boolean zero = true;
                for (int i = resultAdd.length - 2; i >= 0; i--) {
                    if (resultAdd[i] == '1') {
                        zero = false;
                    } else {
                        if (zero) {
                            continue;
                        }
                    }
                    result.append(resultAdd[i]);
                }
            } else {
                result.append(resultAdd[0]);
            }
        }

        if(result.length() == 0) {
            result.append('0');
        }
        return (new StringBuffer(result.toString()).reverse()).toString();
    }

    public String multiplication() {

        String firstNumberMultiply = new String(firstNumber);
        StringBuilder rowOfZero = new StringBuilder();

        char[] result = {'0'};
        int offsetIndex = 0;
        for (int i = 0; i < secondNumber.length; i++) {
            if (secondNumber[i] == '1') {
                offsetIndex = i;
                for (int j = 0; j < offsetIndex; j++) {
                    rowOfZero.append('0');
                }
                firstNumberMultiply = rowOfZero + firstNumberMultiply;
                result = add(firstNumberMultiply.toCharArray(), result);
                firstNumberMultiply = new String(firstNumber);
            }
        }

        return new StringBuffer(new String(result)).reverse().toString();
    }
    public String division() {
        String result = "";
        int cnt = 0;
        while(compareNumbers(firstNumber,secondNumber)) {
            result = deduction(firstNumber,secondNumber);
            firstNumber = result.toCharArray();
            cnt++;
        }

        return convertToBinary(cnt);
    }

    private String convertToBinary(int decimalNumber) {
        StringBuilder binaryResultNumber = new StringBuilder();
        while(decimalNumber > 0) {
            binaryResultNumber.append(decimalNumber % 2);
            decimalNumber /= 2;
        }
        return  binaryResultNumber.reverse().toString();
    }

}
