package com.company;

import javax.swing.*;
import java.awt.*;

public class PanelTable extends JPanel {

    private TableOfTrue table;
    private int changeX;
    private int changeY;
    private int[][] truthTable;
    private int[] vector;
    private boolean dnf = false;
    private boolean knf = false;
    private boolean hasTable = false;

    /**
     * @param table таблица истинности
     */
    public void setTable(TableOfTrue table) {
        this.table = table;
        truthTable = table.getTable();
        vector = table.getVector();
    }

    /**
     * Метод разрешающие или запрещающий
     * отрисовку ДНФ
     *
     * @param dnf true or false
     */
    public void setDnf(boolean dnf) {
        this.dnf = dnf;
    }

    /**
     * Метод разрешающие или запрещающий
     * отрисовку КНФ
     *
     * @param knf true or false
     */
    public void setKnf(boolean knf) {
        this.knf = knf;
    }

    /**
     * Метод разрешающие или запрещающий
     * отрисовку таблицы истинности
     *
     * @param hasTable true or false
     */
    public void setTruthTable(boolean hasTable) {
        this.hasTable = hasTable;
    }

    public void paint(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;

        Font stringFont = new Font("Vertigo", Font.PLAIN, 30);
        g2.setFont(stringFont);

        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);

        if (dnf)
            paintDNF(g2);
        if (hasTable)
            paintTable(g2);
        if (knf)
            paintKNF(g2);
    }

    /**
     * Отрисовка ДНФ
     */
    public void paintDNF(Graphics g) {
        changeX = 0;
        changeY = 600;

        String sOut = "DNF is : ";
        boolean flag = true;
        int countS = 0;
        for (int i = 0; i < table.getN(); i++) {
            if (vector[i] == 1) {
                if (flag) {
                    sOut += "(";
                    countS++;
                    flag = false;
                } else {
                    sOut += "+(";
                    countS += 2;
                }
                for (int j = 0; j < table.getM(); j++) {
                    if (truthTable[i][j] == 0) {
                        sOut += "-X";
                        countS += 2;
                    } else {
                        sOut += "X";
                        countS++;
                    }
                    sOut += (j + 1);
                    if (j < table.getM() - 1) {
                        sOut += " * ";
                        countS += 3;
                    }
                }
                sOut += ")";
                countS++;
            }
            if (countS >= 40) {
                g.drawString(sOut, changeX, changeY);
                changeY += 30;
                sOut = "";
                countS = 0;
            }
        }
        g.drawString(sOut, changeX, changeY);
    }

    /**
     * Отрисовка КНФ
     */
    public void paintKNF(Graphics g) {

        changeX = 0;
        changeY = 600;
        String sOut = "KNF is : ";
        boolean flag = true;
        int countS = 0;
        for (int i = 0; i < table.getN(); i++) {
            if (vector[i] == 0) {
                if (flag) {
                    sOut += "(";
                    countS += 1;
                    flag = false;
                } else {
                    sOut += "*(";
                    countS += 2;
                }
                for (int j = 0; j < table.getM(); j++) {
                    if (truthTable[i][j] == 1) {
                        sOut += "-X";
                        countS += 2;
                    } else {
                        sOut += "X";
                        countS += 1;
                    }
                    sOut += j + 1;
                    if (j < table.getM() - 1) {
                        sOut += " + ";
                        countS += 3;
                    }
                }
                sOut += ")";
                countS += 1;
            }
            if (countS >= 40) {
                g.drawString(sOut, changeX, changeY);
                changeY += 30;
                sOut = "";
                countS = 0;
            }
        }
        g.drawString(sOut, changeX, changeY);
    }

    /**
     * Отрисовка Таблицы истинности
     */
    public void paintTable(Graphics g) {
        changeY = 100;
        changeX = 50;
        String[] header = new String[]{"X1", "X2", "X3", "X4", "F"};

        truthTable = table.getTable();
        vector = table.getVector();

        for (int i = 0; i <= table.getM(); i++) {
            g.drawString(header[i], changeX, changeY - 30);
            changeX += 50;
        }

        changeX = 50;
        changeY = 100;
        for (int i = 0; i < table.getN(); i++) {
            changeX = 50;
            for (int j = 0; j < table.getM(); j++) {
                g.drawString(toString(truthTable[i][j]), changeX, changeY);
                changeX += 50;
            }
            g.drawString(toString(vector[i]), changeX, changeY);
            changeY += 30;
        }
        System.out.println("3");
    }

    /**
     * Перегрущка метода toString
     * для int
     *
     * @param i численная перменная
     * @return строковый эквивалент i
     */
    private String toString(int i) {
        return String.valueOf(i);
    }

}
