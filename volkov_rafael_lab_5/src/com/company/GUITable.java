package com.company;

import javax.swing.*;

public class GUITable {

    private final JFrame frame;
    TableOfTrue table;
    boolean dnf = false;
    boolean knf = false;
    PanelTable panel;

    /**
     * Конструктор, задаёт настройки формы
     */
    public GUITable() {
        frame = new JFrame("Table");
        frame.setSize(1200, 800);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);

        table = new TableOfTrue();
        panel = new PanelTable();
        panel.setTable(table);
        frame.getContentPane().add(panel);
        panel.setBounds(0, 0, 1200, 900);

        JButton buttonCreateTable = new JButton("Create Table");

        frame.getContentPane().add(buttonCreateTable);
        frame.getContentPane().add(panel);

        JButton buttonDNF = new JButton("DNF");
        JButton buttonKNF = new JButton("KNF");

        buttonDNF.setBounds(110, 10, 90, 30);
        buttonKNF.setBounds(210, 10, 90, 30);

        buttonDNF.addActionListener(e -> DNF());
        buttonKNF.addActionListener(e -> KNF());

        frame.getContentPane().add(buttonDNF);
        frame.getContentPane().add(buttonKNF);

        buttonCreateTable.setBounds(10, 10, 90, 30);

        buttonCreateTable.addActionListener(e -> Table());
        panel.repaint();
        frame.repaint();
    }

    /**
     * Добавления таблицы на форму
     */
    public void Table() {
        panel.setTruthTable(true);

        panel.repaint();
        frame.repaint();
    }

    /**
     * Добавление ДНФ на форму
     */
    public void DNF() {
        if (!knf) {
            dnf = true;
            panel.setDnf(true);
        }
        panel.repaint();
        frame.repaint();
    }

    /**
     * Добавление КНФ на фомру
     */
    public void KNF() {
        if (!dnf) {
            knf = true;
            panel.setKnf(true);
        }
        panel.repaint();
        frame.repaint();
    }
}
