package com.company;

public class NormalForm {
    public static String[] variables= {"X1", "X2", "X3", "X4" };

    public static void toDisNormalForm(int n, int m, int[][] truthTable) {
        boolean printPlus = false;

        for (int i = 0; i < n; ++i) {
            if (truthTable[i][m - 1] == 1) {
                // если не первое слагаемое, то ставим плюс
                if (printPlus) {
                    System.out.print(" + ");
                }
                System.out.print("(");
                printPlus = true;

                for (int j = 0; j < m - 1; ++j) {
                    if (truthTable[i][j] == 0) {
                        System.out.print("!");
                    }
                    System.out.print(variables[j]);
                    if (j < m - 2) {
                        System.out.print(" * ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }

    public static void toKonNormalForm(int n, int m, int[][] truthTable) {
        boolean printStar = false;

        for (int i = 0; i < n; ++i) {
            if (truthTable[i][m - 1] == 0) {
                // если не первый множитель, то ставим *
                if (printStar) {
                    System.out.print(" * ");
                }
                System.out.print("(");
                printStar = true;

                for (int j = 0; j < m - 1; ++j) {
                    if (truthTable[i][j] == 1) {
                        System.out.print("!");
                    }
                    System.out.print(variables[j]);
                    if (j < m - 2) {
                        System.out.print(" + ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }
}
