package com;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите исходную систему счисления:");
        int ISS = scanner.nextInt();

        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Введите первое число:");
        String firstNum = scanner1.nextLine();

        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Введите второе число:");
        String secondNum = scanner2.nextLine();

        Scanner scanner3 = new Scanner(System.in);
        System.out.println("Введите арифметическую операцию:");
        char operation = scanner3.next().charAt(0);

        ArithmeticOperations ao = new ArithmeticOperations(firstNum, secondNum, operation, ISS);

        System.out.println("Результат в двоичной системе счисления:");
        System.out.print(ao.result());
    }
}
