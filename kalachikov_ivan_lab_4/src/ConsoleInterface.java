import java.util.Random;
import java.util.Scanner;

public class ConsoleInterface {

    int[][] truthTable;

    private int[][] arrayInit() {
        Random random = new Random();
        int[][] array = new int[][]{
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)}
        };
        printTable(array);
        return array;
    }

    private void printTable(int[][] truthTable) {
        System.out.println("Таблица истинности: ");
        System.out.println("X1|| X2|| X3|| X4|| F");
        for (int[] ints : truthTable) {
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j]);
                if (j != ints.length - 1) {
                    System.out.print(" || ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public void dialog() {
        Scanner scanner = new Scanner(System.in);
        int digit;

        System.out.println("Здравствуйте! Сейчас сгенерируется таблица истинности)");
        truthTable = arrayInit();
        printInfo();

        while (true) {
            System.out.print("Ожидаю ввод команды : ");
            digit = scanner.nextInt();

            switch (digit) {
                case 0 -> {
                    System.out.println("Сейчас будет сгенерирована новая таблица истинности");
                    truthTable = arrayInit();
                    System.out.println("Готово! Таблица в вашем распоряжении)");
                }
                case 1 -> {
                    System.out.println("Приводим таблицу к ДНФ...");
                    System.out.println(Converter.convertToDNF(truthTable));
                    System.out.println("Таблица успешно приведена в ДНФ!");
                }
                case 2 -> {
                    System.out.println("Приводим таблицу к КНФ...");
                    System.out.println(Converter.convertToKNF(truthTable));
                    System.out.println("Таблица успешно приведена в КНФ!");
                }
                case 9 -> {
                    System.out.println("До новых встреч!");
                    System.exit(0);
                }
                default -> {
                    System.out.println("К сожалению, я не понимаю чего вы хотите...");
                    System.out.println("Вы можете использовать команды которые я знаю)");
                    printInfo();
                }
            }
        }
    }

    private void printInfo() {
        System.out.println("Для того чтобы привести таблицу в ДНФ введите 1");
        System.out.println("Для того чтобы привести таблицу в КНФ введите 2");
        System.out.println("Если желаете сгенерировать новую таблицу, введите 0");
        System.out.println("Для выхода из программы введите 9)");
    }
}
