import java.util.ArrayList;

public class Converter {
    private int _origin_ss;
    private char[] _num_ch1;
    private char[] _num_ch2;
    private final int helpSymbol = 55;

    public Converter(int origin_ss, char[] num_ch1, char[] num_ch2) {
        _origin_ss = origin_ss;
        _num_ch1 = num_ch1;
        _num_ch2 = num_ch2;
    }

    private ArrayList <Integer> convertNumber(char[] num_ch) {
        ArrayList <Integer> num_int = new ArrayList(num_ch.length + 1);
        for (int k = 0; k < num_ch.length; k++)
            num_int.add(0);
        int a = 1;
        for (int i = 0; i < num_ch.length; i++) {
            if (i == 0) {
                    num_int.set(0,(num_ch[0] == '-')? 1: 0);
                    if (num_int.get(0) == 0) {
                        num_int.add(0);
                    }
                    else
                    {
                        i++;
                    }
            }

            int buf = (int) num_ch[i] - (int) '0';
            num_int.set(a,(buf >= 0 && buf <= 9)? buf: (buf + (int) '0' - helpSymbol));

            if (num_int.get(a) >= _origin_ss) {
                System.out.println("Неверное число");
                System.exit(0);
            }
            a++;
        }
        return num_int;
    }

    private int [] convertFromOriginToDecimal(ArrayList <Integer> num_int) {
        int [] result = new int[2];
        int res = 0;
        for (int i = 1; i < num_int.size(); i++)
            res += num_int.get(i) * Math.pow(_origin_ss, num_int.size()-i-1);
        result [0] = num_int.get(0);
        result [1] = res;
        return result;
    }

    public String convertToBinary(char[] num_ch) {
        int [] numZnAndMantissa = convertFromOriginToDecimal(convertNumber(num_ch));
        int decimal = numZnAndMantissa[1];
        String mod = "";
        do {
            int cur_mod = 0;
            cur_mod = decimal % 2;
            decimal /= 2;
            mod+=cur_mod;
        } while (decimal >= 2);
        mod+=decimal;
        mod+=numZnAndMantissa[0];
        mod = new StringBuffer(mod).reverse().toString();
        return mod;
    }

}
