
public class Operations {
    private StringBuffer num1;
    private StringBuffer num2;
    private String _operation;

    public Operations(StringBuffer num1, StringBuffer num2, String operation) {
        this.num1 = num1;
        this.num2 = num2;
        _operation = operation;
    }

    private StringBuffer insertNulls (StringBuffer numIsmen, StringBuffer numForCmp) {
        StringBuffer buff = numIsmen;
        int length = Math.max(numIsmen.length(), numForCmp.length());
        while (numIsmen.length()<length)
        {
            buff.insert(1, '0');
        }
        return buff;
    }

    private StringBuffer deleteNulls (StringBuffer num) {
        StringBuffer buff = num;
        while (buff.length()>2 && buff.charAt(1) == '0') {
            buff.deleteCharAt(1);
        }
        return buff;
    }

    private StringBuffer getObrCode (StringBuffer num){
        StringBuffer buff = num;
        if (buff.charAt(0) != '0'){
            for (int i = 1; i < num.length(); i++){
                if (buff.charAt(i)== '0'){
                    buff.setCharAt(i, '1');
                }
                else if (buff.charAt(i)== '1'){
                    buff.setCharAt(i, '0');
                }
            }
        }
        return buff;
    }

    private StringBuffer sumNumForMulDiv (StringBuffer num1, StringBuffer num2){
        StringBuffer result = new StringBuffer();
        result.append(num1);
        int buf = 0;
        int cursum = 0;

        for (int i = num2.length()-1; i >= 0; i--) {
            cursum = ((int) num1.charAt(i) + (int) num2.charAt(i) - 2 * (int)('0')) - 2 + buf;
            buf = 0;
            if (cursum >= 0) {
                buf = 1;
                result.setCharAt(i, (char)(cursum + (int)('0')));
            }
            else if (cursum < 0) {
                buf = 0;
                result.setCharAt(i, (char)((cursum + 2) + (int)('0')));
            }
        }
        if (buf == 1) {
            result.insert(0, buf);
        }
        return result;
    }


    private StringBuffer sumNums(StringBuffer num1, StringBuffer num2) {
        num1 = getObrCode(insertNulls(num1, num2));
        num2 = getObrCode(insertNulls(num2, num1));

        StringBuffer result = new StringBuffer();
        result.setLength(num1.length());
        int buf = 0;
        int cursum = 0;

        for (int i = num1.length()-1; i >= 0; i--) {
            cursum = ((int) num1.charAt(i) + (int) num2.charAt(i) - 2 * (int)('0')) - 2 + buf;
            buf = 0;
            if (cursum >= 0) {
                buf = 1;
                result.setCharAt(i, (char)(cursum + (int)('0')));
            }
            else if (cursum < 0) {
                buf = 0;
                result.setCharAt(i, (char)((cursum + 2) + (int)('0')));
            }
        }
        if (buf == 1){
            num2 = new StringBuffer("01");
            result = sumNums(result, num2);
        }
        result = getObrCode(result);

        if (_operation.contains("+") || (_operation.contains("-"))) {
            deleteNulls(result);
        }
        return result;
    }

    private StringBuffer multipleNums (StringBuffer num1, StringBuffer num2) {
        char znak = '0';
        if (((int)num1.charAt(0) + (int)num2.charAt(0) - 2*(int)'0') == 1) {
            znak = '1';
        }

        num1 = num1.deleteCharAt(0);
        num2 = num2.deleteCharAt(0);

        StringBuffer result = new StringBuffer();
        result.setLength(num1.length() + num2.length());
        for (int i = 0; i < result.length(); i++){
            result.setCharAt(i, '0');
        }

        for (int i = num2.length() - 1; i >= 0; i--){
            if (num2.charAt(i) == '1'){
                result = sumNumForMulDiv(result, num1);
            }

            if (result.length() > (num1.length() + num2.length())){
                result.deleteCharAt(result.length() - 1);
            }
            else {
                result.deleteCharAt(result.length() - 1);
                result.insert(0, '0');
            }
        }

        result.insert(0, znak);
        deleteNulls(result);

        return result;
    }

    private StringBuffer divideNums (StringBuffer num1, StringBuffer num2){

        StringBuffer result = new StringBuffer();
        StringBuffer chastnoe = new StringBuffer();
        StringBuffer vosst = new StringBuffer();

        if (num2.charAt(1) == '0' && num2.charAt(2) == '0'){
            chastnoe.append(" Number1 cannot be divided by 0");
            return chastnoe;
        }

        if (num2.length() > num1.length()) {
            result = new StringBuffer("00");
            return result;
        }

        char znak = '0';
        if (((int)num1.charAt(0) + (int)num2.charAt(0) - 2*(int)'0') == 1) {
            znak = '1';
        }

        vosst.append(num2);
        vosst.setCharAt(0, '0');

        num1.setCharAt(0,'0');
        num2.setCharAt(0,'1');

        int length;
        int diffBtwLengths = num1.length() - num2.length();
        StringBuffer forDopCode = new StringBuffer("01");
        num2 = getObrCode(sumNums(num2, forDopCode));   //получили доп код для делителя
        result.append(num1);

        for (int i = 0; i <= diffBtwLengths ; i++) {
            length = result.length();


            result = sumNumForMulDiv(result, num2);

            if (result.length() > length) {
                result.deleteCharAt(0);
            }
            if (result.charAt(0) == '1') {
                chastnoe.append('0');
                result = sumNumForMulDiv(result, vosst);
                if (result.length() > length) {
                    result.deleteCharAt(0);
                }
            }
            else {
                chastnoe.append('1');
            }
            result.deleteCharAt(0);
        }
        chastnoe.insert(0, znak);
        while (chastnoe.length()>2 && chastnoe.charAt(1) == '0')
        {
            chastnoe.deleteCharAt(1);
        }
        return chastnoe;
    }

    public StringBuffer formResultString() {

        StringBuffer result = new StringBuffer();
        switch (_operation) {
            case "+":
                result = sumNums(num1, num2);
                if (result.charAt(1) == '0')
                    result.setCharAt(0, ' ');
                result.insert(1, '|');
                break;
            case "-":
                num2.setCharAt(0, (char)(Math.abs((int)num2.charAt(0) - (int)'0' + 1 - 2) + (int)('0')));
                result = sumNums(num1, num2);
                if (result.charAt(1) == '0')
                    result.setCharAt(0, ' ');
                result.insert(1, '|');
                break;
            case "*":
                result = multipleNums(num1, num2);
                if (result.charAt(1) == '0')
                    result.setCharAt(0, ' ');
                result.insert(1, '|');
                break;
            case "/":
                result = divideNums(num1, num2);

                if (result.charAt(1) == '0')
                    result.setCharAt(0, ' ');
                result.insert(1, '|');
                break;
        }
        return result;
    }
}
