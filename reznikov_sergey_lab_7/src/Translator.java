import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Translator {

    String[] sourceCode;
    StringBuffer purposeCode;
    ArrayList variableStrings = new ArrayList();
    ArrayList variableNumbers = new ArrayList();
    String emptystring = "empstr";
    String article = "artstr";
    int resultIndex=0;

    public void translate(String source, String purpose) throws IOException {
        FileWriter writer = new FileWriter(purpose, false);
        System.out.print("format PE console\n" +
                "\n" +
                "entry Start\n" +
                "\n" +
                "include 'win32a.inc'\n" +
                "\n" +
                "section '.data' data readable writable\n\n");
        writer.write("format PE console\n" +
                "\n" +
                "entry Start\n" +
                "\n" +
                "include 'win32a.inc'\n" +
                "\n" +
                "section '.data' data readable writable\n\n");
        read(source);
        getVar(writer);
        System.out.print("\nsection '.code' code readable executable \n\nStart:\n");
        writer.write("\nsection '.code' code readable executable \n\nStart:\n");
        readAndWriteCode(writer);
        System.out.print("finish:\n" +
                "\n" +
                "                call[getch]\n" +
                "\n" +
                "                push NULL\n" +
                "                call[ExitProcess]\n" +
                "\n" +
                "\n" +
                "section '.idata' import data readable\n" +
                "\n" +
                "        library kernel, 'kernel32.dll',\\\n" +
                "                msvcrt, 'msvcrt.dll'\n" +
                "\n" +
                "        import  kernel,\\\n" +
                "                ExitProcess, 'ExitProcess'\n" +
                "\n" +
                "        import msvcrt,\\\n" +
                "               printf, 'printf',\\\n" +
                "               scanf, 'scanf',\\\n" +
                "               getch,'_getch'");
        writer.write("finish:\n" +
                "\n" +
                "                call[getch]\n" +
                "\n" +
                "                push NULL\n" +
                "                call[ExitProcess]\n" +
                "\n" +
                "\n" +
                "section '.idata' import data readable\n" +
                "\n" +
                "        library kernel, 'kernel32.dll',\\\n" +
                "                msvcrt, 'msvcrt.dll'\n" +
                "\n" +
                "        import  kernel,\\\n" +
                "                ExitProcess, 'ExitProcess'\n" +
                "\n" +
                "        import msvcrt,\\\n" +
                "               printf, 'printf',\\\n" +
                "               scanf, 'scanf',\\\n" +
                "               getch,'_getch'");

        writer.close();
    }

    private void readAndWriteCode(FileWriter writer) throws IOException {
        int index = 0;
        while (!sourceCode[index].contains("begin")) {
            index++;
        }
        while (!sourceCode[index].contains("end")) {
            if (sourceCode[index].contains("write")) {
                if (sourceCode[index].contains("" + (char) 39) || sourceCode[index].contains("" + (char) 34)) {
                    StringBuffer tmp = new StringBuffer("");
                    boolean flag = false;
                    for (int i = 0; i < sourceCode[index].length(); i++) {
                        if (((sourceCode[index].charAt(i) == (char) 39) || (sourceCode[index].charAt(i) == (char) 34)) && !flag) {
                            flag = true;
                            i++;
                        } else if (((sourceCode[index].charAt(i) == (char) 39) || (sourceCode[index].charAt(i) == (char) 34)) && flag) {
                            break;
                        }
                        if (flag) {
                            tmp.append(sourceCode[index].charAt(i));
                        }
                    }
                    for (int i = 0; i < variableStrings.size(); i++) {
                        if (((String) variableStrings.get(i)).contains(tmp.toString())) {
                            System.out.print("push str" + i + "\ncall [printf]\n\n");
                            writer.write("push str" + i + "\ncall [printf]\n\n");
                        }
                    }
                    if (sourceCode[index].contains("writeln")) {
                        System.out.print("push " + article + "\ncall [printf]\n\n");
                        writer.write("push " + article + "\ncall [printf]\n\n");
                    }
                } else {
                    StringBuffer tmp = new StringBuffer("");
                    boolean flag = false;
                    for (int i = 0; i < sourceCode[index].length(); i++) {
                        if (sourceCode[index].charAt(i) == (char) 40 && !flag) {
                            flag = true;
                            i++;
                        } else if (sourceCode[index].charAt(i) == (char) 41 && flag) {
                            break;
                        }
                        if (flag) {
                            tmp.append(sourceCode[index].charAt(i));
                        }
                    }
                    System.out.print("push [" + variableNumbers.get(resultIndex) + "]\n" + "push " + emptystring + "\ncall [printf]\n\n");
                    writer.write("push [" + variableNumbers.get(resultIndex) + "]\n" + "push " + emptystring + "\ncall [printf]\n\n");

                    if (sourceCode[index].contains("writeln")) {
                        System.out.print("push " + article + "\ncall [printf]\n\n");
                        writer.write("push " + article + "\ncall [printf]\n\n");
                    }
                }
            }
            if (sourceCode[index].contains("read")) {
                StringBuffer tmp = new StringBuffer("");
                boolean flag = false;
                for (int i = 0; i < sourceCode[index].length(); i++) {
                    if (sourceCode[index].charAt(i) == (char) 40 && !flag) {
                        flag = true;
                        i++;
                    } else if (sourceCode[index].charAt(i) == (char) 41 && flag) {
                        break;
                    }
                    if (flag) {
                        tmp.append(sourceCode[index].charAt(i));
                    }
                }
                System.out.print("push " + tmp.toString() + "\npush " + emptystring + "\ncall [scanf]\n\n");
                writer.write("push " + tmp.toString() + "\npush " + emptystring + "\ncall [scanf]\n\n");
            }
            if (sourceCode[index].contains(":=")) {
                StringBuffer tmp = new StringBuffer(sourceCode[index]);
                {
                    StringBuffer number = new StringBuffer("");
                    int i =0;
                    boolean wrote=false;
                    while(isLetterOrNumber(sourceCode[index].charAt(i))||!wrote){
                        if(isLetterOrNumber(sourceCode[index].charAt(i))){
                            wrote=true;
                        }
                        if(wrote){
                            number.append(sourceCode[index].charAt(i));
                        }
                        i++;
                    }
                    for(int k=0;k<variableNumbers.size();k++){
                        if(((String) variableNumbers.get(k)).contains(number.toString())){
                            resultIndex=k;
                        }
                    }
                }
                tmp.delete(0,tmp.indexOf("=")+1);
                if(tmp.toString().contains("+")){
                    String[] numbers = tmp.toString().split("\\"+(char) 43);
                    if(numbers.length==2){
                        System.out.print("mov ecx, [" + numbers[0] + "]\n" + "add ecx, [" + numbers[1] + "]\n"+"mov [" + variableNumbers.get(resultIndex)+"], ecx\n\n");
                        writer.write("mov ecx, [" + numbers[0] + "]\n" + "add ecx, [" + numbers[1] + "]\n"+"mov [" + variableNumbers.get(resultIndex)+"], ecx\n\n");
                    }
                }
                if(tmp.toString().contains("-")){
                    String[] numbers = tmp.toString().split("\\"+(char) 45);
                    if(numbers.length==2){
                        System.out.print("mov ecx, [" + numbers[0] + "]\n" + "sub ecx, [" + numbers[1] + "]\n"+"mov [" + variableNumbers.get(resultIndex)+"], ecx\n\n");
                        writer.write("mov ecx, [" + numbers[0] + "]\n" + "sub ecx, [" + numbers[1] + "]\n"+"mov [" + variableNumbers.get(resultIndex)+"], ecx\n\n");
                    }
                }
                if(tmp.toString().contains("*")){
                    String[] numbers = tmp.toString().split("\\"+(char) 42);
                    if(numbers.length==2){
                        System.out.print("mov ecx, [" + numbers[0] + "]\n" + "imul ecx, [" + numbers[1] + "]\n"+"mov [" + variableNumbers.get(resultIndex)+"], ecx\n\n");
                        writer.write("mov ecx, [" + numbers[0] + "]\n" + "imul ecx, [" + numbers[1] + "]\n"+"mov [" + variableNumbers.get(resultIndex)+"], ecx\n\n");
                    }
                }
                if(tmp.toString().contains("/")||tmp.toString().contains("div")){
                    String[] numbers = tmp.toString().split("div");
                    if(numbers.length<2){
                        numbers=tmp.toString().split("/");
                    }
                    if(numbers.length==2){
                        System.out.print("mov eax, [" + numbers[0] + "]\n" + "mov ecx, [" + numbers[1] + "]\n"
                                + "idiv ecx\n\n" +"mov [" + variableNumbers.get(resultIndex)+"], eax\n\n");
                        writer.write("mov eax, [" + numbers[0] + "]\n" + "mov ecx, [" + numbers[1] + "]\n"
                                + "idiv ecx\n\n" + "mov [" + variableNumbers.get(resultIndex)+"], eax\n\n");
                    }
                }
            }
            index++;
        }
    }

    private boolean isLetterOrNumber(char letter) {
        if((letter<=57&&letter>=48)||(letter<=90&&letter>=65)||(letter<=122&&letter>=97)){
            return true;
        }
        return false;
    }

    private void getVar(FileWriter writer) throws IOException {
        for (String string :
                sourceCode) {
            //ascii code of ' and "
            if (string.contains("" + (char) 39) || string.contains("" + (char) 34)) {
                StringBuffer tmp = new StringBuffer("");
                boolean flag = false;
                for (int i = 0; i < string.length(); i++) {
                    if (((string.charAt(i) == (char) 39) || (string.charAt(i) == (char) 34)) && !flag) {
                        flag = true;
                        i++;
                    } else if (((string.charAt(i) == (char) 39) || (string.charAt(i) == (char) 34)) && flag) {
                        flag = false;
                        variableStrings.add(tmp.toString());
                        tmp.delete(0, tmp.length());
                    }
                    if (flag) {
                        tmp.append(string.charAt(i));
                    }
                }
            }
        }
        int index=0;
        while (!sourceCode[index].contains("var")){
            index++;
        }
        while (!sourceCode[index].contains("begin")) {
            int i = 0;
            if(sourceCode[index].contains("var")){
                i=sourceCode[index].indexOf("var")+3;
            }
            StringBuffer tmp = new StringBuffer("");
            boolean type=false;
            boolean flag = false;
            while(i<sourceCode[index].length()) {
                if (isLetterOrNumber(sourceCode[index].charAt(i)) && !flag) {
                    flag = true;
                } else if (!isLetterOrNumber(sourceCode[index].charAt(i))){
                    flag = false;
                    if(!tmp.toString().isEmpty()){
                        variableNumbers.add(tmp.toString());
                        System.out.print(tmp.toString() + " dd ?\n");
                        writer.write(tmp.toString() + " dd ?\n");
                    }
                    tmp.delete(0, tmp.length());
                    if(sourceCode[index].charAt(i)==':'){
                        type=true;
                    }
                }
                if (flag && !type) {
                    tmp.append(sourceCode[index].charAt(i));
                }
                i++;
            }
            index++;
        }

        for (int i = 0; i < variableStrings.size(); i++) {
            System.out.print("str" + i + " db " + ((char) 39) + variableStrings.get(i) + ((char) 39) + ", 0\n");
            writer.write("str" + i + " db " + ((char) 39) + variableStrings.get(i) + ((char) 39) + ", 0\n");
        }
        System.out.print(emptystring + " db " + ((char) 39) + "%d" + ((char) 39) + ", 0\n");
        writer.write(emptystring + " db " + ((char) 39) + "%d" + ((char) 39) + ", 0\n");
        System.out.print(article + " db " + ((char) 39) + " " + ((char) 39) + ",0xA, 0\nNULL=0\n");
        writer.write(article + " db " + ((char) 39) + " " + ((char) 39) + ",0xA, 0\nNULL=0\n");
    }

    private void read(String source) {
        StringBuffer string = new StringBuffer("");
        try (FileReader reader = new FileReader(source)) {
            int c;
            while ((c = reader.read()) != -1) {
                string.append((char) c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sourceCode = string.toString().split(";");
    }
}
