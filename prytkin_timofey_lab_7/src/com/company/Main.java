package com.company;

public class Main {

    public static void main(String[] args) {

        //Создаем класс для чтения
        PasFileReader pasFileReader = new PasFileReader();

        //Создаем класс для записи
        ASMFileWriter asmFileWriter = new ASMFileWriter();

        //Парсим текст
        ParseString parseString = new ParseString(pasFileReader.read(), asmFileWriter);
        parseString.parseVarAndBody();

        //Записываем текст в .ASM
        asmFileWriter.write();

    }
}
