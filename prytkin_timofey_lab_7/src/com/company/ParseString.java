package com.company;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseString {

    ASMFileWriter asmFileWriter;

    private String string;

    private String blockOfVars;
    private String blockOfCods;

    private ArrayList<String> arrayVarsString = new ArrayList<>();
    private ArrayList<String> arrayCodsString = new ArrayList<>();
    private ArrayList<String> arrayVars = new ArrayList<>();


    public ParseString(String string, ASMFileWriter asmFileWriter) {

        this.string = string;
        this.asmFileWriter = asmFileWriter;

        String temp = "\n-----------------------------------------------\n";
        System.out.println(temp + this.string + temp);
    }

    /**
     * Этот метод отделит блок с переменными от блока с кодом
     */
    public void parseVarAndBody() {
        Pattern patternForBlocOfVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");//Паттерн от слова var не включая его через абсолютно все символы [\\s\\S]+ до слова begin не включая его
        Matcher matcherForBlocOfVariables = patternForBlocOfVariables.matcher(string);

        System.out.println("Вывод блока переменных:");
        while (matcherForBlocOfVariables.find()) {
            blockOfVars = string.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end());
            System.out.println(blockOfVars);
        }

        Pattern patternForBlocOfCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");//Паттерн от слова begin не включая его через абсолютно все символы [\\s\\S]+ до слова end. не включая его
        Matcher matcherForBlocOfCode = patternForBlocOfCode
                .matcher(string);

        System.out.println("Вывод блока кода:");
        while (matcherForBlocOfCode.find()) {
            blockOfCods = string.substring(matcherForBlocOfCode.start(), matcherForBlocOfCode.end());
            System.out.println(blockOfCods);
        }

        parseVarsStrings();
        parseCodsStrings();
    }

    /**
     * Этот метод отделит все строки с переменными в список
     */
    public void parseVarsStrings(){

        Pattern patternForBlocOfVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherForBlocOfVariables = patternForBlocOfVariables.matcher(blockOfVars);

        while (matcherForBlocOfVariables.find()) {

            arrayVarsString.add(blockOfVars.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end()));

        }
        System.out.println("Вывод списка строк переменных:\n" + arrayVarsString.toString());

        parseVarsInteger();
    }

    /**
     * Этот метод отделит все переменные из блока с переменными в список
     */
    public void parseVarsInteger(){
        Pattern patternForBlocOfVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherForBlocOfVariables;
        String var;

        for (String string : arrayVarsString) {

            matcherForBlocOfVariables = patternForBlocOfVariables.matcher(string);

            while (matcherForBlocOfVariables.find()) {
                var = string.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end());
                if (!var.equals("integer")) { arrayVars.add(var); }
            }
        }
        System.out.println("Вывод переменных:\n" + arrayVars.toString());

        asmFileWriter.addToVariables(arrayVars);
    }

    /**
     * Этот метод отделит все функции и операции из блока с кодом в список
     */
    public void parseCodsStrings(){

        Pattern patternForStringsOfCods = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherForStringsOfCods = patternForStringsOfCods.matcher(blockOfCods);

        while (matcherForStringsOfCods.find()) {

            arrayCodsString.add(blockOfCods.substring(matcherForStringsOfCods.start(), matcherForStringsOfCods.end()));

        }
        System.out.println("Вывод списка строк кода:\n" + arrayCodsString.toString());

        createCode();
    }

    /**
     * Этот метод отделит все функции и операции из блока с кодом в список
     */
    public void createCode(){

        Pattern patternForWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternForWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern patternForReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternForOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        for (String string : arrayCodsString) {


            if(string.matches(patternForWrite.toString())){
                Matcher matcherForCode = patternForWrite.matcher(string);

                if (matcherForCode.find()) {
                    System.out.println("Подходит по шаблону write('...'); ||| " + string + " ||| " + matcherForCode.group(1));
                    asmFileWriter.addToCodeWrite(matcherForCode.group(1));
                }
            }
            else if(string.matches(patternForReadLn.toString())){
                Matcher matcherForCode = patternForReadLn.matcher(string);

                if (matcherForCode.find()) {
                    System.out.println("Подходит по шаблону readln('...'); ||| " + string + " ||| " + matcherForCode.group(1));
                    asmFileWriter.addToCodeReadLn(matcherForCode.group(1));
                }
            }
            else if(string.matches(patternForOperation.toString())){
                Matcher matcherForCode = patternForOperation.matcher(string);

                if (matcherForCode.find()) {
                    System.out.println("Подходит по шаблону str := str +-*/ str; ||| " + string);
                    asmFileWriter.addToCodeOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            }
            else if(string.matches(patternForWriteLn.toString())){
                Matcher matcherForCode = patternForWriteLn.matcher(string);

                if (matcherForCode.find()) {
                    System.out.println("Подходит по шаблону writeln(...); ||| " + string + " ||| " + matcherForCode.group(1));
                    asmFileWriter.addToCodeWriteLn(matcherForCode.group(1));
                }
            }
        }

    }
}
