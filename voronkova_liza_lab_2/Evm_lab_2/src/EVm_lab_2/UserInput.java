package EVm_lab_2;

import java.util.Scanner;

public class UserInput {

    private static Scanner scan = new Scanner(System.in);

    public static int getNotation(String dopInfo) {
        try {
            System.out.print("Введите " + dopInfo + " систему: ");
            int not = Integer.parseInt(scan.nextLine());
            if (not >= Converter.Bin && not <= Converter.Hex) {
                return not;
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static String getNum(int not) throws Exception {
        System.out.print("Введите число в исходной системе: ");
        String result = scan.nextLine();
        result = result.toLowerCase();
        char[] resultArr = result.toCharArray();
        for (int i = 0; i < resultArr.length; i++) {
            if (Character.isDigit(resultArr[i])) {
                if (Character.getNumericValue(resultArr[i]) >= not) {
                    throw new Exception();
                }
            } else {
                if (resultArr[i] < Converter.downAsciiBorder || resultArr[i] > Converter.upperAsciiBorder) {
                    throw new Exception();
                } else {
                    if ((resultArr[i] - 87) >= not) {
                        throw new Exception();
                    }
                }
            }
        }
        return result;
    }
}
