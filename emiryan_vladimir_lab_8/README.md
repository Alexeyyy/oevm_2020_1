**Лабораторная работа: №7**  
**Выполнил: Эмиря́н Владимир**

**Задача:**  
Разборка и сборка компьютера

Накопилось у меня за  несколько лет добро, вот пригодилось. Вообще в планах было сделать что-то типо приставки, чтобы играть в ретро игры, ибо
железо старое на большее претендовать не получится. Это старый блок питания, на 300w, настоллько старый, что нет SATA-2 кабелей, только молексы
![title](https://sun9-33.userapi.com/impg/9oApuNHKiWqROFmGDLBcCfaqW9L1RIvYXqVTyQ/-bUTQkoWZj0.jpg?size=1600x1200&quality=96&proxy=1&sign=88e7d855cf821fb7c976fc22bfc233aa&type=album)

Материнская плата SATA поддерживает, но тоже довольно старая, на 478 сокете с Pentium 4. Самое новое в этой сборке это куллер, притянутый за
шнурки, потому что крепление старое не стандартное
![title](https://sun9-31.userapi.com/impg/qBc34sREcW-UUbiBwFWAtBLb75O2-KufKiZqmg/CvB2-F0I_cQ.jpg?size=1200x1600&quality=96&proxy=1&sign=4a96687ff5bd0f14d6a44e66d092aed9&type=album)

Видеокарата, пока дом не спалила, известное поверие пока не сбывается, к счастью. Модель Nvidia GT210, на ddr2 памяти, объемом 512mb
![title](https://sun9-4.userapi.com/impg/NPJYsTfxgYxPz_qIh3QoMbxa3DCBlI5Zo2TgkQ/qsGEAiJDXY0.jpg?size=1600x1200&quality=96&proxy=1&sign=c81c71d86a406463bb0f714100877e0f&type=album)

Две плашки оперативки, к сожалению одна ddr2 другая ddr1, суммарный объем 1,5gb, на телефонах сейчас больше
![title](https://sun9-16.userapi.com/impg/xAsmmbqvGwttCtuqCzzNNXIJwonreVnTq7DNcQ/JLHjG2KyFG8.jpg?size=1600x1200&quality=96&proxy=1&sign=19f61dd000ed6d0abb03e2b3e501e10e&type=album)

Жесткий диск на 80gb, опять же на телефонах уже больше бывает, время неумолимо идет вперед, на компьютере стоит линукс чисто для экспериментов.
Сам диск старый, разъем IDE, не актуален уже долгое время
![title](https://sun9-67.userapi.com/impg/qypxYX7T51JczrBMcc-bIZ85faatlhO5toPFuQ/oO8ce_jARBU.jpg?size=1600x1200&quality=96&proxy=1&sign=ffdd13e13ef54371c8920fc7924a1176&type=album)

В планах сделать из него свой сервер, банальная тема, однако интересно, хотя возможно что и серевер получится бестолковый, однако планы есть планы. 
