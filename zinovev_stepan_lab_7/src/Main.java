public class Main {

    public static void main(String[] args) {
        CommandStorage commandStorage = new CommandStorage();
        PascalReader pascalReader = new PascalReader("C:\\Users\\User\\IdeaProjects\\7thLaboratoryWork\\files\\code.pas");
        pascalReader.readPasFile(commandStorage);

        FASMWriter fasmWriter = new FASMWriter("C:\\Users\\User\\IdeaProjects\\7thLaboratoryWork\\files\\codeFromPascalToFASM.ASM");
        fasmWriter.writeFile(commandStorage);
    }
}
