package com.company;

import javax.swing.*;
import java.awt.*;

public class Frame {

    private JButton buttonDNF;
    private JButton buttonKNF;
    private JButton buttonNext;
    private JTextArea resultArea;
    private JTextArea statusArea;
    private JFrame frame;
    private Panel panel;
    private Minimalization minimalization;
    private TruthTable truthTable;
    private Font resultFont;

    public void initialization() {
        minimalization = new Minimalization();
        truthTable = new TruthTable();
        buttonNext = new JButton("Next Step");
        buttonNext.setBounds(650, 50, 90, 30);
        buttonNext.setEnabled(false);
        buttonNext.addActionListener(e -> {
            if (panel.getSelectedForm().equals("DNF")) {
                minimalization.setIteration(minimalization.getIteration() + 1);
                if (minimalization.getIteration() == truthTable.getCountOfStrings() - 1) {
                    buttonNext.setEnabled(false);
                }
                minimalization.disjunctiveNormalForm(TruthTable.truthTable);
                resultArea.setText(minimalization.getStringResult());
                if (minimalization.getIteration() == 15 && TruthTable.truthTable[minimalization.getIteration()][truthTable.getCountOfColumns() - 1] != 1) {
                    resultArea.append("()");
                }
                if (TruthTable.truthTable[minimalization.getIteration()][truthTable.getCountOfColumns() - 1] == 1) {
                    statusArea.setText("Строка " + (minimalization.getIteration() + 1) + " подходит");
                } else {
                    statusArea.setText("Строка " + (minimalization.getIteration() + 1) + " не подходит");
                }
            } else if (panel.getSelectedForm().equals("KNF")) {
                minimalization.setIteration(minimalization.getIteration() + 1);
                if (minimalization.getIteration() == truthTable.getCountOfStrings() - 1) {
                    buttonNext.setEnabled(false);
                }
                minimalization.сonjunctiveNormalForm(TruthTable.truthTable);
                resultArea.setText(minimalization.getStringResult());
                if (minimalization.getIteration() == 15 && TruthTable.truthTable[minimalization.getIteration()][truthTable.getCountOfColumns() - 1] != 0) {
                    resultArea.append("()");
                }
                if (TruthTable.truthTable[minimalization.getIteration()][truthTable.getCountOfColumns() - 1] == 0) {
                    statusArea.setText("Строка " + (minimalization.getIteration() + 1) + " подходит");
                } else {
                    statusArea.setText("Строка " + (minimalization.getIteration() + 1) + " не подходит");
                }
            }
            frame.repaint();
        });
        buttonDNF = new JButton("DNF");
        buttonDNF.setBounds(510, 50, 60, 30);
        buttonDNF.addActionListener(e -> {
            buttonNext.setEnabled(true);
            buttonDNF.setEnabled(false);
            buttonKNF.setEnabled(false);
            panel.setSelectedForm("DNF");
            frame.repaint();

        });
        buttonKNF = new JButton("KNF");
        buttonKNF.setBounds(580, 50, 60, 30);
        buttonKNF.addActionListener(e -> {
            buttonNext.setEnabled(true);
            buttonDNF.setEnabled(false);
            buttonKNF.setEnabled(false);
            panel.setSelectedForm("KNF");
            frame.repaint();
        });

        resultFont = new Font("Times New Roman", Font.PLAIN, 15);
        resultArea = new JTextArea();
        resultArea.setBounds(500, 100, 300, 400);
        resultArea.setEnabled(false);
        resultArea.setFont(resultFont);
        resultArea.setBackground(new Color(238, 238, 238));
        resultArea.setDisabledTextColor(Color.BLACK);
        resultArea.setLineWrap(true);
        resultArea.setWrapStyleWord(true);

        statusArea = new JTextArea();
        statusArea.setBounds(510, 650, 150, 50);
        statusArea.setEnabled(false);
        statusArea.setFont(resultFont);
        statusArea.setBackground(new Color(238, 238, 238));
        statusArea.setDisabledTextColor(Color.BLACK);
        statusArea.setLineWrap(true);
        statusArea.setWrapStyleWord(true);

    }

    public Frame() {
        panel = new Panel();
        frame = new JFrame("Визуализация");
        frame.setSize(900, 900);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
        initialization();
        frame.getContentPane().add(buttonDNF);
        frame.getContentPane().add(buttonKNF);
        frame.getContentPane().add(resultArea);
        frame.getContentPane().add(statusArea);
        frame.getContentPane().add(buttonNext);
        frame.getContentPane().add(panel);
        panel.setBounds(0, 0, 900, 900);
        frame.repaint();
    }
}
