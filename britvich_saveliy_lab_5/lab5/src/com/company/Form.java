package com.company;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Form {
    private final JTextArea textArea = new JTextArea();
    private final JButton btnDNF = new JButton("DNF");
    private final JButton btnCNF = new JButton("CNF");
    public final int[][] truthTable;
    public final JPanel tableVisualization;
    public static boolean isDNF;
    public static boolean isCNF;

    public Form() {
        TruthTable table = new TruthTable();
        truthTable = table.truthTable;
        tableVisualization = new Visualization(truthTable);
        initialize();
    }

    private void initialize() {
        TruthTable table = new TruthTable();
        JFrame form = new JFrame();
        form.setTitle("Визуализация DNF и CNF");
        form.setBounds(100, 100, 400, 600);
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.getContentPane().setLayout(null);
        form.setVisible(true);
        tableVisualization.setBounds(0, 0, 180, 525); // tableVisualization
        form.getContentPane().add(tableVisualization);
        textArea.setBounds(220, 10, 200, 600); // textArea
        textArea.setEditable(false);
        form.getContentPane().add(textArea);
        btnDNF.addActionListener(new ActionListener() { // btnDNF
            public void actionPerformed(ActionEvent e) {
                isCNF = false;
                isDNF = true;
                textArea.setText((table.toDisjunctiveNormalForm(truthTable)));
                tableVisualization.repaint();
            }
        });
        btnDNF.setBounds(10, 525, 65, 30);
        form.getContentPane().add(btnDNF);
        btnCNF.addActionListener(new ActionListener() { // btnCNF
            public void actionPerformed(ActionEvent e) {
                isDNF = false;
                isCNF = true;
                textArea.setText((table.toConjunctiveNormalForm(truthTable)));
                tableVisualization.repaint();
            }
        });
        btnCNF.setBounds(95, 525, 65, 30);
        form.getContentPane().add(btnCNF);
    }
}
