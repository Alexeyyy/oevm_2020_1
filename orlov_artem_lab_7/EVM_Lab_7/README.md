# Лабораторная работа №7

## Орлов Артем ПИБд-21

##### Техническое задание: 

Изучение методов трансляции.

Разработать транслятор программ Pascal-FASM. Предусмотреть
проверку синтаксиса и семантики исходной программы. Результатом
работы транслятора является программа для ассемблера FASM,
идентичная по функционалу исходной программе. Полученная
программа должна компилироваться и выполняться без ошибок.

_Исходная программа на языке программирования Pascal имеет вид:_

```
var
x, y: integer;
res1, res2, res3, res4: integer;
begin
write(‘input x: ’); readln(x);
write(‘input y: ’); readln(y);
res1 := x + y; write(‘x + y = ’); writeln(res1);
res2 := x - y; write(‘x - y = ’); writeln(res2);
res3 := x * y; write(‘x * y = ’); writeln(res3);
res4 := x / y; write(‘x / y = ’); writeln(res4);
end.
```

Видео https://drive.google.com/file/d/1rP1fcOYP2ovecb7akiYWZr2HWt8jcHWq/view?usp=sharing
