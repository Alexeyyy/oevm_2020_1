package EVM_Lab_7;

import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {

    private static int newVariablesCount = 0;

    public static void readPascalFile(String name, Translator translator) {
        try (FileReader reader = new FileReader(name)) {
            Scanner scanner = new Scanner(reader);
            StringBuilder text = new StringBuilder();
            while (scanner.hasNextLine()) {
                text.append(scanner.nextLine()).append("\n");
            }
            String code = text.toString();
            codeHandling(code, translator);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void codeHandling(String code, Translator translator) {
        variableHandling(code, translator);
        commandHandling(code, translator);
    }

    private static void variableHandling(String code, Translator translator) {
        Pattern patternVariablesString = Pattern.compile("(?<=var\\n)[\\w\\W]*(?=\\nbegin)");
        Matcher matcherVariablesString = patternVariablesString.matcher(code);

        if (matcherVariablesString.find()) {
            String variables = matcherVariablesString.group();

            Pattern patternVariables = Pattern.compile("\\b[a-zA-Z][\\w]*\\b");
            Matcher matcherVariables = patternVariables.matcher(variables);

            while (matcherVariables.find()) {
                if (!matcherVariables.group().equals("integer")) {
                    translator.addVariable(matcherVariables.group() + " dd ?\n");
                }
            }
        }
    }

    private static void commandHandling(String code, Translator translator) {
        Pattern patternCommandsString = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherCommandString = patternCommandsString.matcher(code);
        if (matcherCommandString.find()) {
            String commands = matcherCommandString.group();
            Pattern patternCommands = Pattern.compile("[\\w][\\w /,=*+:()'-]*;");
            Matcher matcherCommands = patternCommands.matcher(commands);

            while (matcherCommands.find()) {
                String command = matcherCommands.group();
                String valueWithoutQuotes = null;
                if (command.contains("(")) {
                    valueWithoutQuotes = command.substring(command.indexOf('(') + 1, command.indexOf(')'));
                }

                if (command.contains("readln")) {
                    translator.addReadOperation(valueWithoutQuotes);
                } else if (command.contains("write")) {
                    writeHandling(command, translator, valueWithoutQuotes);
                } else if (command.contains(":=")) {
                    operationHandling(command, translator);
                }
            }
        }
    }

    private static void writeHandling(String command, Translator translator, String valueWithoutQuotes) {
        String endVariable = "', 0\n";
        String endVariableNewLine = ", 0dh, 0ah, 0\n";
        if (command.contains("'")) {
            String value = command.substring(command.indexOf('(') + 2, command.indexOf(')') - 1);
            String newVariable = "string" + newVariablesCount + " db '" + value;

            if (command.contains("writeln")) {
                newVariable += endVariableNewLine;
            } else {
                newVariable += endVariable;
            }

            translator.addVariable(newVariable);
            translator.addWriteOperation("string" + newVariablesCount);
            newVariablesCount++;
        } else {
            if (command.contains("writeln")) {
                translator.addWritelnOperation(valueWithoutQuotes);
            } else {
                translator.addWriteOperation(valueWithoutQuotes);
            }
        }
    }

    private static void operationHandling(String command, Translator translator) {
        String result = command.substring(0, command.indexOf(":") - 1);
        if (command.contains("+")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("+") - 1);
            String secondElement = command.substring(command.indexOf("+") + 2, command.length() - 1);
            translator.addMathsOperation(result + " " + firstElement + " " + secondElement + " add");
        } else if (command.contains("-")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("-") - 1);
            String secondElement = command.substring(command.indexOf("-") + 2, command.length() - 1);
            translator.addMathsOperation(result + " " + firstElement + " " + secondElement + " sub");
        } else if (command.contains("*")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("*") - 1);
            String secondElement = command.substring(command.indexOf("*") + 2, command.length() - 1);
            translator.addMathsOperation(result + " " + firstElement + " " + secondElement + " imul");
        } else if (command.contains("div")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("div") - 1);
            String secondElement = command.substring(command.indexOf("div") + 4, command.length() - 1);
            translator.addMathsOperation(result + " " + firstElement + " " + secondElement + " div");
        }
    }

}
