package com.company;

import java.util.Random;

public class Helper {
	 public static int[][] generateArray() {
	        Random random = new Random();

	        return new int[][]{
	                {0, 0, 0, 0, random.nextInt(2)},
	                {0, 0, 0, 1, random.nextInt(2)},
	                {0, 0, 1, 0, random.nextInt(2)},
	                {0, 0, 1, 1, random.nextInt(2)},
	                {0, 1, 0, 0, random.nextInt(2)},
	                {0, 1, 0, 1, random.nextInt(2)},
	                {0, 1, 1, 0, random.nextInt(2)},
	                {0, 1, 1, 1, random.nextInt(2)},
	                {1, 0, 0, 0, random.nextInt(2)},
	                {1, 0, 0, 1, random.nextInt(2)},
	                {1, 0, 1, 0, random.nextInt(2)},
	                {1, 0, 1, 1, random.nextInt(2)},
	                {1, 1, 0, 0, random.nextInt(2)},
	                {1, 1, 0, 1, random.nextInt(2)},
	                {1, 1, 1, 0, random.nextInt(2)},
	                {1, 1, 1, 1, random.nextInt(2)},
	        };
	    }

	    public static String dnf(int[][] generatedArray) {
	        String result = new String();

	        for (int[] T : generatedArray) {
	            if (T[4] == 1) {
	                if (result.isEmpty()) {
	                    result += ((T[0] == 0) ? "( !x1 * " : "( x1 * ");
	                } else {
	                    result += ((T[0] == 0) ? " + \n( !x1 * " : " + \n( x1 * ");
	                }
	                result += ((T[1] == 0) ? "!x2 * " : "x2 * ");
	                result += ((T[2] == 0) ? "!x3 * " : "x3 * ");
	                result += ((T[3] == 0) ? "!x4 )" : "x4 )");
	            }
	        }
	        return result;
	    }

	    public static String knf(int[][] generatedArray) {
	        String result = new String();

	        for (int[] T : generatedArray) {
	            if (T[4] == 0) {
	                if (result.isEmpty()) {
	                    result += ((T[0] == 0) ? "( x1 + " : "( !x1 + ");
	                } else {
	                    result += ((T[0] == 0) ? " * \n( !x1 + " : " * \n( !x1 + ");
	                }
	                result += ((T[1] == 0) ? "x2 + " : "!x2 + ");
	                result += ((T[2] == 0) ? "x3 + " : "!x3 + ");
	                result += ((T[3] == 0) ? "x4 )" : "!x4 )");
	            }
	        }
	        return result;
	    }
	}
