package com.company;

import java.awt.Graphics;

import javax.swing.JPanel;

public class plane extends JPanel {
	  private static String iteration;
	    private final int[][] arrayForVisualization;

	    public plane(int[][] array) {
	        arrayForVisualization = array;
	    }

	    @Override
	    public void paint(Graphics g) {
	        super.paint(g);
	        HelperDraw.drawTable(arrayForVisualization, iteration, g);
	    }

	    public static void setIteration(String setIteration) {
	        iteration = setIteration;
	    }

}
