package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static final int OFFSET = 10;

    public static void main(String[] args) {
        int base;

        Scanner in = new Scanner(System.in);
        System.out.print("Input the base: ");
        base = in.nextInt();
        Correctness.correctBase(base);

        Scanner in1 = new Scanner(System.in);
        System.out.print("Input the first number: ");
        String num = in1.nextLine();
        char[] number = num.toCharArray();
        Correctness.correctNumber(base,number);
        String firstNumber = Converting.convertToDecimal(base, number);
        firstNumber = firstNumber.replaceAll("[^0-9]", "");

        System.out.print("Input the second number: ");
        num = in1.nextLine();
        number = num.toCharArray();
        String secondNumber = Converting.convertToDecimal(base, number);
        secondNumber=secondNumber.replaceAll("[^0-9]", "");

        Correctness.correctNumber(base,number);

        calculate(firstNumber, secondNumber);
    }
    public static void calculate(String firstNumber, String secondNumber){
        Scanner in1 = new Scanner(System.in);

        Long firstNum = Long.parseLong(firstNumber);
        Long secondNum = Long.parseLong(secondNumber);

        System.out.print("\n Input the action : 1.+  2.-  3.*  4./ \n");
        char action = in1.next ().charAt (0);

        switch (action) {
            case '+':
                ArrayList<Long> arrayListForAdd;
                arrayListForAdd = BinaryCalculator.add(firstNum, secondNum);

                System.out.print("Result: ");
                BinaryCalculator.print(arrayListForAdd);

                break;

            case '-':
                ArrayList<Long> arrayListForSub;
                arrayListForSub = BinaryCalculator.subtract(firstNum, secondNum);

                System.out.print("Result: ");
                BinaryCalculator.print(arrayListForSub);

                break;

            case '*':
                long power, storage, result= 0;
                long digit, factor = 1;

                ArrayList<Long> arrayListForMultiply = new ArrayList<>();

                if (firstNum < secondNum) {
                    long n = firstNum;
                    firstNum = secondNum;
                    secondNum = n;
                }

                while (secondNum != 0) {
                    digit = (secondNum % 10);

                    if (digit == 1) {
                        storage = firstNum * factor;

                        for (int j = arrayListForMultiply.size()-1; j >= 0; j--) {
                            power = (long)Math.pow(10,j);
                            result = result + (power * arrayListForMultiply.get(j));
                        }

                        arrayListForMultiply = BinaryCalculator.add(result, storage);
                    } else {
                        firstNum = firstNum * factor;
                    }

                    secondNum = secondNum / 10;
                    factor = factor * 10;
                }

                BinaryCalculator.print(arrayListForMultiply);
                break;

            case '/':
                long amount = 0;
                long pow, repository = 0;

                ArrayList<Long> arrayListForDiv;

                while (firstNum >= secondNum) {
                    arrayListForDiv = BinaryCalculator.subtract(firstNum, secondNum);

                    for (int j = arrayListForDiv.size() - 1; j >= 0; j--) {
                        pow = (long) Math.pow(10, j);
                        repository = repository + (pow * arrayListForDiv.get(j));
                    }

                    firstNum = repository;
                    amount++;

                    repository = 0;
                    arrayListForDiv.clear();
                }
                String resultDiv = Converting.convertToBinary(amount);

                resultDiv=resultDiv.replaceAll("[^0-2]", "");
                System.out.print(resultDiv);

                break;
        }
    }
}

