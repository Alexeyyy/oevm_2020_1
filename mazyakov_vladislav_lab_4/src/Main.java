import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        TruthTable table = new TruthTable();
        table.fillTable();
        table.printTable();

        System.out.println();
        System.out.println("выберите 1 для  ДНФ таблицы   | выберите  2 для КНФ таблицы");
        System.out.println();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String normalForm = reader.readLine();
        NormalForm form = new NormalForm();
        switch (normalForm) {
            case "1":
                form.convertToDisjunctiveNormalForm(table.getTruthTable());
                break;
            case "2":
                form.convertToConjunctiveNormalForm(table.getTruthTable());
                break;
        }
    }
}
