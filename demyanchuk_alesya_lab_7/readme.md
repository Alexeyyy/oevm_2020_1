###### ПИбд-21 Алеся Демянчук
# Лабораторная работа №7

###### Как запустить лабораторную работу
1. Открыть папку "demyanchuk_alesya_lab_7" в IntelliJ IDEA как проект
2. Запустить программу
3. После выполнения перейти в папку src и открыть файл demyanchuk_alesya_lab7.EXE
###### Использованные технологии
- Java 11
- IntelliJ IDEA

###### Что делает программа?
Транслятор программ Pascal-FASM. 
Результатом работы транслятора является программа для ассемблера FASM, 
идентичная по функционалу исходной программе.

Исходная программа на языке программирования Pascal имеет вид:
```
var
x, y: integer;
res1, res2, res3, res4: integer;
begin
write('input x: '); readln(x);
write('input y: '); readln(y);
res1 := x + y; write('x + y = '); writeln(res1);
res2 := x - y; write('x - y = '); writeln(res2);
res3 := x * y; write('x * y = '); writeln(res3);
res4 := x / y; write('x / y = '); writeln(res4);
end.
```
***Видео с примером работы программы: https://yadi.sk/i/KLjWRrT7gySp-A***
