package com;

import java.util.ArrayList;
import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {
    private static ArrayList<String> peremenList = new ArrayList<>();
    private static ArrayList<String> commandList = new ArrayList<>();
    private static ArrayList<String> readCommandList = new ArrayList<>();
    private static ArrayList<String> writeCommandList = new ArrayList<>();
    private static ArrayList<String> matematicCommandList = new ArrayList<>();

    public static void readPascalFile(String name) {
        try (FileReader reader = new FileReader(name)) {
            Scanner scanner = new Scanner(reader);
            StringBuilder textStringBuilder = new StringBuilder();
            while (scanner.hasNextLine()) {
                textStringBuilder.append(scanner.nextLine()).append("\n");
            }
            String code = textStringBuilder.toString();
            codeProcessing(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Writer wrt = new Writer(peremenList,commandList,readCommandList,writeCommandList,matematicCommandList);
    }

    private static void codeProcessing(String code) {
        peremenProcessing(code);
        commandProcessing(code);
    }

    private static void peremenProcessing(String code) {
        Pattern patternVariablesString = Pattern.compile("(?<=var\\n)[\\w\\W]*(?=\\nbegin)");
        Matcher matcherVariablesString = patternVariablesString.matcher(code);

        if (matcherVariablesString.find()) {
            String variables = matcherVariablesString.group();

            Pattern patternVariables = Pattern.compile("\\b[a-zA-Z][\\w]*\\b");
            Matcher matcherVariables = patternVariables.matcher(variables);

            while (matcherVariables.find()) {
                if (!matcherVariables.group().equals("integer")) {
                    peremenList.add(matcherVariables.group() + " dd ?\n");
                }
            }
        }
    }

    private static void commandProcessing(String code) {
        Pattern patternCommandsString = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherCommandString = patternCommandsString.matcher(code);
        if (matcherCommandString.find()) {
            String commands = matcherCommandString.group();

            Pattern patternCommands = Pattern.compile("[\\w][\\w /,=*+:()'-]*;");
            Matcher matcherCommands = patternCommands.matcher(commands);

            while (matcherCommands.find()) {

                String command = matcherCommands.group();
                String valueWithoutQuotes = null;
                if (command.contains("(")) {
                    valueWithoutQuotes = command.substring(command.indexOf('(') + 1, command.indexOf(')'));
                }

                if (command.contains("readln")) {
                    readCommandList.add(valueWithoutQuotes);
                    commandList.add("READ");
                } else if (command.contains("write")) {
                    write(command);
                } else if (command.contains(":=")) {
                    operation(command);
                }
            }
        }
    }

    private static int newVariablesCount = 0;

    private static void write(String command) {
        String endVariable = "', 0\n";
        String endVariableNewLine = ", 0dh, 0ah, 0\n";
        if (command.contains("'")) {
            String value = command.substring(command.indexOf('(') + 2, command.indexOf(')') - 1);
            String newVariable = "string" + newVariablesCount + " db '" + value;

            if (command.contains("writeln")) {
                newVariable += endVariableNewLine;
            } else {
                newVariable += endVariable;
            }

            peremenList.add(newVariable);
            writeCommandList.add("string" + newVariablesCount);
            commandList.add("WRITE");
            newVariablesCount++;

        }
    }

    private static void operation(String command) {
        String result = command.substring(0, command.indexOf(":") - 1);
        if (command.contains("+")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("+") - 1);
            String secondElement = command.substring(command.indexOf("+") + 2, command.length() - 1);
            matematicCommandList.add(result + " " + firstElement + " " + secondElement + " add");
            commandList.add("MATHS");
        } else if (command.contains("-")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("-") - 1);
            String secondElement = command.substring(command.indexOf("-") + 2, command.length() - 1);
            matematicCommandList.add(result + " " + firstElement + " " + secondElement + " sub");
            commandList.add("MATHS");
        } else if (command.contains("*")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("*") - 1);
            String secondElement = command.substring(command.indexOf("*") + 2, command.length() - 1);
            matematicCommandList.add(result + " " + firstElement + " " + secondElement + " imul");
            commandList.add("MATHS");
        } else if (command.contains("div")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("div") - 1);
            String secondElement = command.substring(command.indexOf("div") + 4, command.length() - 1);
            matematicCommandList.add(result + " " + firstElement + " " + secondElement + " div");
            commandList.add("MATHS");
        }
    }
}
