package com;

public class MinBoolFunc {
    int numCol = 5;
    int numRow = 16;

    public String DNF(int[][]tablIst) {
        int count = 0;
        String rez = "";
        for (int i = 0; i < numRow; i++) {
            if (tablIst[i][numCol - 1] == 1) {
                if (count > 0) {
                    rez += " + ";
                }
                if (tablIst[i][0] == 0) {
                    rez += "( - X1 ";
                } else {
                    rez += "( X1 ";
                }
                if (tablIst[i][1] == 0) {
                    rez += " * -X2 ";
                } else {
                    rez += " * X2 ";
                }
                if (tablIst[i][2] == 0) {
                    rez += " * -X3 ";
                } else {
                    rez += " * X3 ";
                }
                if (tablIst[i][3] == 0) {
                    rez = rez + "* -X4 )\n";
                } else {
                    rez = rez + "* X4 )\n";
                }
                count++;
            }
        }
        return rez;
    }

    public String KNF(int[][]tablIst) {
        int count = 0;
        String rez ="";
        for (int i = 0; i < numRow; i++) {
            if (tablIst[i][numCol - 1] == 0) {
                if (count > 0) {
                    rez += " * ";
                }
                if (tablIst[i][0] == 1) {
                    rez += "( - X1 ";
                } else {
                    rez += " ( X1 ";
                }
                if (tablIst[i][1] == 1) {
                    rez += " - X2 ";
                } else {
                    rez += " + X2 ";
                }
                if (tablIst[i][2] == 1) {
                    rez += " - X3 ";
                } else {
                    rez += " + X3 ";
                }
                if (tablIst[i][3] == 1) {
                    rez += " - X4 )\n";
                } else {
                    rez += " + X4 )\n";
                }
                count++;
            }
        }
        return rez;
    }
}
