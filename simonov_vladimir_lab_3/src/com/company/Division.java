package com.company;

import java.util.ArrayList;

public class Division implements IArithmetic {

    private ArrayList<Integer> result;
    private Sum sum;
    private Sub sub;
    private Compare compare;
    private ArrayList<Integer> unit;
    /**
     * Конструктор, инициализирующий вспомогательные классы;
     */
    public Division() {
        this.sum = new Sum();
        this.result = new ArrayList();
        this.compare = new Compare();
        this.sub = new Sub();
        this.unit = new ArrayList<>(1);
        this.unit.add(1);
    }
    /**
     * Метод деления 2х чисел в 2-й с.с
     */
    @Override
    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> num1, ArrayList<Integer> num2) {
        this.result = new ArrayList<>();
        if (num2.get(num2.size() - 1) == 0) {
            System.out.printf("Error! Делить на ноль нельзя.\n");
            return this.result;
        } else {
            ArrayList<Integer> tmp = num1;
            if (compare.equals(tmp, num2, 0) >= 0) {
                while (compare.equals(tmp, num2, 0) >= 0) {
                    tmp = sub.startArithmeticOperation(tmp, num2);
                    result = sum.startArithmeticOperation(unit, result);
                }
            } else {
                this.result.add(0);
            }
        }
        return result;
    }
}
