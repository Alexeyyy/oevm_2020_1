package com.company;

import java.util.ArrayList;

public class Multiplication implements IArithmetic {

    private ArrayList<Integer> result;
    private Sum sum;
    private Compare compare;
    private ArrayList<Integer> Unit;
    /**
     * Конструктор, инициализирующий вспомогательные классы;
     */
    public Multiplication() {
        this.sum = new Sum();
        this.compare = new Compare();
        this.Unit = new ArrayList<>(1);
        this.Unit.add(1);
    }
    /**
     * Метод умножения 2х чисел в 2-й с.с
     */
    @Override
    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> num1, ArrayList<Integer> num2) {
        this.result = new ArrayList<>();
        ArrayList<Integer> tmp = new ArrayList<>();
        tmp.add(0);
        if (compare.equals(tmp, num2, 0) < 0) {
            while (compare.equals(tmp, num2, 0) == -1) {
                result = sum.startArithmeticOperation(num1, result);
                tmp = sum.startArithmeticOperation(tmp, Unit);
            }
        } else {
            this.result.add(0);
        }
        return result;
    }
}
