package com.company;

import java.util.ArrayList;

public interface IArithmetic {
    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> a, ArrayList<Integer> b);
}

