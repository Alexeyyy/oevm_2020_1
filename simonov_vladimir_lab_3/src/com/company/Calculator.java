package com.company;

import java.util.ArrayList;
import java.util.HashMap;

public class Calculator {

    private Sum sum;
    private Sub sub;
    private Multiplication mult;
    private Division division;
    private Compare compare;
    private Per tranlator = new Per();
    private Operations operations;
    private HashMap<Integer, String> Methods;
    private ArrayList<Integer> num1Binary = new ArrayList();
    private ArrayList<Integer> num2Binary = new ArrayList<>();
    private ArrayList<Integer> result = new ArrayList();
    private StringBuilder num1 = new StringBuilder("");
    private StringBuilder num2 = new StringBuilder("");
    private boolean orientResult = true;
    private String choice;
    private int scaleOfNotation = -1;

    /**
     * Метод выбора арифметических операций
     */
    public void setChoice(String choice) {
        this.choice = choice;
    }
    /**
     * Метод проверки переменных на неверные значения
     */
    private boolean checkInvalidInput() {
        return (num1Binary == null || num2Binary == null || scaleOfNotation == -1);
    }
    /**
     * Метод вывода в консоль всех функций калькулятора
     */
    public void list() {
        for (int i = 1; i <= Methods.size(); i++) {
            System.out.println(Methods.get(i));
        }
        System.out.print("\n");
    }
    /**
     * Конструктор для функций калькулятора
     */
    public Calculator() {
        sum = new Sum();
        sub = new Sub();
        mult = new Multiplication();
        division = new Division();
        compare = new Compare();
        Methods = new HashMap<>();
        initDesc();
    }
    /**
     * Метод, принимающий первое и второе число ввиде строк, и с.с
     */
    public void setArguments(String first, String second, int ScaleOfNotation) {
        this.num1.append(first);
        this.num2.append(second);
        if (ScaleOfNotation <= 0) {
            System.out.printf("Ошибка введения с.с");
            return;
        }
        this.scaleOfNotation = ScaleOfNotation;
        convertToArrayList();
    }
    /**
     * Приватный метод инициализации хэш-таблицы, хранящей описание функций калькулятора
     */
    private void initDesc() {
        Methods.put(1, "Сложение первого и второго числа: +");
        Methods.put(2, "Разность первого и второго числа: -");
        Methods.put(3, "Произведение первого и второго числа: *");
        Methods.put(4, "Частное первого и второго числа: /");
    }
    /**
     * Метод запуска арифметических действий
     */
    public void toCalculate() {
        this.result = new ArrayList<>();
        if (!checkInvalidInput()) {
            getBinaryAandB();
            switch (operations.getOperations(choice)) {
                case Sum: {
                    this.result = sum.startArithmeticOperation(num1Binary, num2Binary);
                    break;
                }
                case Sub: {
                    if (compare.equals(num1Binary, num2Binary, 0) == -1) {
                        this.orientResult = false;
                        ArrayList<Integer> tmp = num1Binary;
                        num1Binary = num2Binary;
                        num2Binary = tmp;
                    } else {
                        this.orientResult = true;
                    }
                    this.result = sub.startArithmeticOperation(num1Binary, num2Binary);
                    break;
                }
                case Multiplication: {
                    this.result = mult.startArithmeticOperation(num1Binary, num2Binary);
                    break;
                }
                case Division: {
                    this.result = division.startArithmeticOperation(num1Binary, num2Binary);
                    break;
                }
                default:
                    break;
            }
            getResult();
        }
    }
    /**
     * Метод конвертации строкового ввода в ArrayList.
     */
    private void convertToArrayList() {
        String A_BinaryStr = tranlator.translate(scaleOfNotation, 2, num1.toString());
        String B_BinaryStr = tranlator.translate(scaleOfNotation, 2, num2.toString());
        for (int i = A_BinaryStr.length() - 1; i >= 0; i--) {
            if (A_BinaryStr.charAt(i) == '0' || A_BinaryStr.charAt(i) == '1') {
                this.num1Binary.add(Character.getNumericValue(A_BinaryStr.charAt(i)));
            } else {
                num1Binary = null;
                return;
            }
        }
        for (int i = B_BinaryStr.length() - 1; i >= 0; i--) {
            if (B_BinaryStr.charAt(i) == '0' || B_BinaryStr.charAt(i) == '1') {
                this.num2Binary.add(Character.getNumericValue(B_BinaryStr.charAt(i)));
            } else {
                num2Binary = null;
                return;
            }
        }
    }
    /**
     * Метод вывода в консоль первого и второго числа в 2-й с.с
     */
    private void getBinaryAandB() {
        System.out.println("Первое число в 2-й с.с");
        for (int i = num1Binary.size() - 1; i >= 0; i--) {
            System.out.print(num1Binary.get(i));
        }
        System.out.printf("\n");
        System.out.println("Второе число в 2-й с.с");
        for (int i = num2Binary.size() - 1; i >= 0; i--) {
            System.out.print(num2Binary.get(i));
        }
        System.out.printf("\n");
    }
    /**
     * Метод получения результата арифметической операции
     */
    public void getResult() {
        if (this.result.size() != 0) {
            System.out.println("Ответ:");
            for (int i = this.result.size() - 1; ((i >= 0) && (this.result.get(i) == 0)); i--) {
                this.result.remove(i);
            }
            if (this.result.size() == 0) {
                result.add(0);
            }
            if (!orientResult) {
                System.out.printf("-");
                this.orientResult = true;
            }
            for (int i = this.result.size() - 1; i >= 0; i--) {
                System.out.print(this.result.get(i));
            }
            System.out.printf("\n");
        }
    }
}
