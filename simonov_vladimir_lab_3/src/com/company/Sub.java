package com.company;

import java.util.ArrayList;

public class Sub implements IArithmetic {
    private ArrayList<Integer> result;
    private Sum sum;
    private ArrayList<Integer> unit;
    /**
     * Конструктор, инициализирующий вспомогательные классы;
     */
    public Sub() {
        this.sum = new Sum();
        this.unit = new ArrayList<>(1);
        this.unit.add(1);
    }
    /**
     * Метод разности 2х чисел в 2-й с.с
     */
    @Override
    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> num1, ArrayList<Integer> num2) {
        this.result = new ArrayList<>();
        ArrayList<Integer> backnum2 = new ArrayList<>();
        //Обратный код
        for (int i = 0; i < num2.size(); i++) {
            if (num2.get(i) == 1) {
                backnum2.add(0);
            } else {
                backnum2.add(1);
            }
        }
        //Дополняем нулями
        for (int i = backnum2.size(); i < num1.size(); i++) {
            backnum2.add(1);
        }
        //Дополнительный код
        backnum2 = this.sum.startArithmeticOperation(backnum2, this.unit);
        //Сложение первого числа и дополнительного кода второго числа
        this.result = this.sum.startArithmeticOperation(num1, backnum2);
        this.result.remove(this.result.size() - 1);
        return this.result;
    }
}
