package com.company;

import java.util.ArrayList;
import static java.lang.Math.min;

public class Compare {
    /**
     * Метод для возвращения длины без незначущих нулей впереди
     */
    private int lengthWithoutZeros(ArrayList<Integer> arrayList) {
        int result = arrayList.size();
        for (int i = result - 1; ((result > 0) && (arrayList.get(i) == 0)); i--) {
            result--;
        }
        return result;
    }
    /**
     * Метод сравнения 2х ArrayList. Вернет 1 => num1>num2; 0 => num1=num2; -1 => num1<num2;
     */
    public int equals(ArrayList<Integer> num1, ArrayList<Integer> num2, int step) {
        int num1Size = lengthWithoutZeros(num1);
        int num2Size = lengthWithoutZeros(num2);
        if (num1Size > num2Size) {
            return 1;
        }
        if (num2Size > num1Size) {
            return -1;
        }
        if (step >= min(num1Size, num2Size)) {
            if (num1.get(num1Size - step) > num2.get(num2Size - step)) {
                return 1;
            } else {
                if (num2.get(num2Size - step) > num1.get(num1Size - step)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
        if (num1.get(num1Size - step - 1) > num2.get(num2Size - step - 1)) {
            return 1;
        } else {
            if (num2.get(num2Size - step - 1) > num1.get(num1Size - step - 1)) {
                return -1;
            } else {
                return equals(num1, num2, step + 1);
            }
        }
    }
}
