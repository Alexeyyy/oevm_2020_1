public class BinaryCalculator {
    private static char _signFirst;
    private static char _signSecond;

    private static String _binaryFirst;
    private static String _binarySecond;

    private static String _result = "";
    private static boolean _reversed = false;

    public static String calculate(int originalSON, String firstNum, String secondNum, String operation) throws Exception {
        _signFirst = firstNum.toCharArray()[0];
        _signSecond = secondNum.toCharArray()[0];

        _binaryFirst = Convertor.convertToUnsignedBinary(originalSON, firstNum);
        _binarySecond = Convertor.convertToUnsignedBinary(originalSON, secondNum);
        swapNumbers();

        switch (operation) {
            case "-":
               getBinarySubtraction();
            case "+":
                getBinarySum();
                return _result;
            case "*":
                getBinaryMultiplication();
                return _result;
            case "/":
                getBinaryDivide();
                return _result;
            default:
                throw new Exception();
        }
    }

    //////////////////////////////////////////////////////
    ///////Функции для осуществления арифм операций///////
    //////////////////////////////////////////////////////

    private static void getBinarySum() throws Exception {
        if (_signFirst != '-' && _signSecond != '-') {
            _result += countSumBinaryNumbers(_binaryFirst, _binarySecond);
        }
        else if (_signFirst == '-' && _signSecond != '-') {
            reverseCode(_binaryFirst, _binarySecond);
            _result += countSumBinaryNumbers(_binaryFirst, _binarySecond);
            _result = removeZero(_result);
            _result = new StringBuffer(_result).reverse().toString();
            _result += "-";
            _result = new StringBuffer(_result).reverse().toString();
        }
        else if (_signFirst != '-') {
            reverseCode(_binaryFirst, _binarySecond);
            _result += countSumBinaryNumbers(_binaryFirst, _binarySecond);
            _result = removeZero(_result);
        }
        else {
            _result += "-" + countSumBinaryNumbers(_binaryFirst, _binarySecond);
        }
    }

    private static void getBinarySubtraction() {
        if (_reversed) {
            if (_signFirst == '-') {
                _signFirst = '+';
            }
            else {
                _signFirst = '-';
            }
        }
        else {
            if (_signSecond == '-') {
                _signSecond = '+';
            }
            else {
                _signSecond = '-';
            }
        }
    }

    private static void getBinaryMultiplication() {
        if ((_signFirst == '-' && _signSecond != '-') || (_signFirst != '-' && _signSecond == '-')) {
            _result += "-";
        }
        _result += countMultiplicationBinaryNumbers(_binaryFirst, _binarySecond);
    }

    private static void getBinaryDivide() throws Exception {
        if ((_signFirst == '-' && _signSecond != '-') || (_signFirst != '-' && _signSecond == '-'))
        {
            _result += "-";
            _result += countDivideBinaryNumbers(_binaryFirst, _binarySecond);
            return;
        }
        _result += countDivideBinaryNumbers(_binaryFirst, _binarySecond);
    }

    /////////////////////////////////////////////////////////////
    //////////////////////служебные функции//////////////////////
    /////////////////////////////////////////////////////////////

    private static String countSumBinaryNumbers(String firstNum, String secondNum) {
        String result = "";
        char[] firstNumArray = new StringBuffer(firstNum).reverse().toString().toCharArray();//реверс строки и перевод в массив символов
        char[] secondNumArray = new StringBuffer(secondNum).reverse().toString().toCharArray();

        int count = 0;
        char transfer = '0';

        for (int i = 0; i < firstNumArray.length; i++) {
            if (count < secondNumArray.length) {
                if (firstNumArray[i] == '1' && secondNumArray[i] == '1') {
                    result += transfer;
                    transfer = '1';
                    count++;
                }
                else if ((firstNumArray[i] == '1' && secondNumArray[i] == '0') ||
                        (firstNumArray[i] == '0' && secondNumArray[i] == '1')) {
                    if (transfer != '1') {
                        result += "1";
                    }
                    else {
                        result += "0";
                    }
                    count++;
                }
                else if (firstNumArray[i] == '0' && secondNumArray[i] == '0') {
                    result += transfer;
                    transfer = '0';
                    count++;
                }
            }

            else {
                if (firstNumArray[i] != '0' || transfer != '0') {
                    if ((firstNumArray[i] == '1' && transfer == '0') ||
                            (firstNumArray[i] == '0' && transfer == '1')) {
                        result += "1";
                        transfer = '0';
                    }
                    else if (firstNumArray[i] == '1' && transfer == '1') {
                        result += "0";
                    }
                }
                else {
                    result += "0";
                }
            }
        }
        if (transfer == '1') {
            result += "1";
        }
        return new StringBuffer(result).reverse().toString();
    }

    /**
     Функция для обратного кода, применяется перед операцией вычитания.
     Данная операция трансформирует операцию сложения в операцию вычитания
     */
    private static void reverseCode (String firstNum, String secondNum) {
        char[] firstNumArray = firstNum.toCharArray();
        char[] secondNumArray = new char[firstNumArray.length];
        char[] bufArray = secondNum.toCharArray();
        _binarySecond = "";

        for (int i = 0; i < firstNumArray.length - bufArray.length; i++) {
            secondNumArray[i] = '0';
        }
        System.arraycopy(bufArray,0, secondNumArray,firstNumArray.length - bufArray.length, bufArray.length);
        for (char c : secondNumArray) {
            if (c == '1') {
                _binarySecond += "0";
            }
            else {
                _binarySecond += "1";
            }
        }
        _binarySecond = countSumBinaryNumbers(_binarySecond, "1");
    }

    /**
     Удаление первой единицы и лишних нулей, применяется после операции вычитания
     */
    private static String removeZero(String value) {
        char[] valueArray = value.toCharArray();
        boolean firstElem = false;
        String result = "";

         for (int i = 1; i < valueArray.length; i++)
        {
            if (valueArray[i] == '1') {
                firstElem = true;
            }
            if (firstElem) {
                result += valueArray[i];
            }
        }
        if (result == "") {
            result = "0";
        }
        return result;
    }

    private static String countMultiplicationBinaryNumbers(String firstNum, String secondNum) {
        char[] secondNumArray = new StringBuffer(secondNum).reverse().toString().toCharArray();
        String result = "0";
        int jIndex = 0;

        for (int i = 0; i < secondNumArray.length; i++) {
            if (secondNumArray[i] == '1') {
                for (int j = 0; j < i - jIndex; j++) {
                    firstNum += "0";
                }
                result = countSumBinaryNumbers(firstNum, result);
                jIndex = i;
            }
        }
        return result;
    }

    private static String countDivideBinaryNumbers(String firstNum, String secondNum) throws Exception {
        String subtrahend = _binaryFirst;
        String result = "0";
        reverseCode(firstNum, secondNum);

        if (secondNum.toCharArray().length == 1 && secondNum.toCharArray()[0] != '1') {
            throw new Exception();
        }
        else if (_reversed) {
            return "0";
        }
        else
        {
            while (compareNumbers(subtrahend, secondNum)) {
                result = countSumBinaryNumbers(result, "1");
                subtrahend = countSumBinaryNumbers(subtrahend, _binarySecond);
                subtrahend = removeZero(subtrahend);
            }
            return result;
        }
    }

    /**
     *Функция для свапа значений.
     *Значения меняются местами в случае, когда второе число >= первого
     */
    private static void swapNumbers() {
        char[] firstArray = _binaryFirst.toCharArray();
        char[] secondArray = _binarySecond.toCharArray();

        if (firstArray.length < secondArray.length)
        {
            String tempStr = _binaryFirst;
            char sign = _signFirst;
            _binaryFirst = _binarySecond;
            _signFirst = _signSecond;
            _binarySecond = tempStr;
            _signSecond = sign;
            _reversed = true;
            return;
        }
        if (firstArray.length == secondArray.length) {
            for (int i = 0; i < firstArray.length; i++) {
                if (firstArray[i] == '0' && secondArray[i] == '1') {
                    String tempStr = _binaryFirst;
                    char sign = _signFirst;
                    _binaryFirst = _binarySecond;
                    _signFirst = _signSecond;
                    _binarySecond = tempStr;
                    _signSecond = sign;
                    _reversed = true;
                    return;
                }
            }
        }
    }

    private static boolean compareNumbers(String firstNum, String secondNum) {
        char[] firstNumArray = firstNum.toCharArray();
        char[] secondNumArray = secondNum.toCharArray();

        if (firstNumArray.length > secondNumArray.length) {
            return true;
        }
        else if (firstNumArray.length < secondNumArray.length) {
            return false;
        }
        else {
            for(int i = 0; i < firstNumArray.length; i++) {
                if (firstNumArray[i] == '1' && secondNumArray[i] == '0') {
                    return true;
                }
                else if (firstNumArray[i] == '0' && secondNumArray[i] == '1'){
                    return false;
                }
            }
        }
        return true;
    }
}
