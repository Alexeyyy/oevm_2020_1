package com.company;

public class BinaryNS {
    public static String add(char[] firstTerm, char[] secondTerm) {
        String result = "";
        String currentChar = "0";
        int memorytm = 0; // разряд при сложении

        for (int i = firstTerm.length - 1, j = secondTerm.length - 1; (i >=0 || j >=0) || memorytm == 1; --i, --j) {
            int firstDigit, secondDigit;
            if (i < 0) {
                firstDigit = 0;
            }
            else if(firstTerm[i] == '1') {
                firstDigit = 1;
            } else {
                firstDigit = 0;
            }
            if (j < 0) {
                secondDigit = 0;
            }                                       // незнач. нули
            else if(secondTerm[j] == '1') {
                secondDigit = 1;} else{secondDigit = 0;
            }

            int currentSum = firstDigit + secondDigit + memorytm;

            if (currentSum == 3) {
                currentChar = "1";
                memorytm = 1;
            } else if (currentSum == 2) {
                currentChar = "0";
                memorytm = 1;
            } else if(currentSum == 1) {
                currentChar = "1";
                memorytm = 0;
            }
            else {
                currentChar = "0";
                memorytm = 0;
            }

            result = currentChar + result;
        }
        return result;
    }
    public static String subtract(char[] subtrahend, char[] minuend) {
        String result = "";

        int cnt = subtrahend.length - minuend.length;
        char[] minuendAdditional = new char[subtrahend.length];
        // обратный код
        for (int i = 0; i < cnt; ++i) {
            minuendAdditional[i] = '1';
        }
        for (int i = cnt; i < minuendAdditional.length; ++i) {
            if (minuend[i - cnt] == '1') {
                minuendAdditional[i] = '0';
            }
            else {
                minuendAdditional[i] = '1';
            }
        }

        char[] one = {'1'};
        minuendAdditional = add(minuendAdditional, one).toCharArray();
        char[] additionalSum = add(minuendAdditional, subtrahend).toCharArray();

        if (additionalSum.length > 1) {
            boolean insignificantZero = true; // незнач. нули
            for (int i = 1; i < additionalSum.length; ++i) { // пропускаем первый разряд и незначащие нули
                if (additionalSum[i] == '1') {
                    insignificantZero = false; }
                else {
                    if (insignificantZero) {
                        continue;
                    }
                }
                result += additionalSum[i];
            }
        }
        return result;
    }



    // :
    public static String division(char[] dividend, char[] divider) {
        long cnt = 0;
        while (dividend.length > 0) { // вычитаем пока не дойдём до нуля
            dividend = (subtract(dividend, divider)).toCharArray();
            cnt++;
        }
        String result = HelpClass.convertToBinary(cnt);
        return result;
    }


    // *
    public static String multiplication(char[] firstFactor, char[] secondFactor) {
        long cnt = HelpClass.convertToTen(secondFactor, 2);

        char[] dopFactor = firstFactor;
        for (int i = 0; i < cnt - 1; ++i){
            firstFactor = (add(firstFactor, dopFactor)).toCharArray();
        }
        String result = new String(firstFactor);
        return result;
    }
}


