package com.company;

public class HelpClass {
    public static long convertToTen(char[] number, int startSystem) {
        long numberTen = 0;



        for(int i = 0; i < number.length; ++i) {
            int сurDigit = 1;
            if (number[i] >= '0' && number[i] <= '9') {
                сurDigit = number[i] - '0';
            }
            else if (Character.isLetter(number[i])) {
                сurDigit = 10 + number[i] - 'A';
            }

            numberTen = (long)((double)numberTen + (double)сurDigit * Math.pow((double)startSystem, (double)(number.length - i - 1)));
        }

        return numberTen;
    }

    public static String convertToBinary(long numberTen) {
        String result = "";
        long remainder = 1;
        int binaryNotation = 2;


        for(String сurChar = ""; numberTen > 0; numberTen /= (long)binaryNotation) {
            remainder = numberTen % (long)binaryNotation;
            сurChar = "";
            if (numberTen % (long)binaryNotation < 10) {
                сurChar = Long.toString(remainder);
            }
            else {
                сurChar = сurChar + (char)((int)('A' + remainder - 10));
            }
            result = сurChar + result;
        }
        return result;
    }
}


