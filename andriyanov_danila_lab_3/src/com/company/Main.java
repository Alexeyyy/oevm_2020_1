package com.company;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException{
        System.out.print("Введите систему счисления: ");
        Scanner scanner = new Scanner(System.in);
        int notation = scanner.nextInt();

        System.out.print("Введите первое число: ");
        Scanner s = new Scanner(System.in);
        char[] firstNumber = s.nextLine().toCharArray();

        System.out.print("Введите второе число: ");
        Scanner sс = new Scanner(System.in);
        char[] secondNumber = sс.nextLine().toCharArray();

        System.out.print("Операция: ");
        char operation = (char) System.in.read();

        long firstDecimal = HelpClass.convertToTen(firstNumber, notation);
        long secondDecimal = HelpClass.convertToTen(secondNumber, notation);

        char[] firstBinary = (HelpClass.convertToBinary(firstDecimal)).toCharArray();
        char[] secondBinary = (HelpClass.convertToBinary(secondDecimal)).toCharArray();

        if (operation == '+') {
            System.out.println(BinaryNS.add(firstBinary, secondBinary));
        }
        if (operation == '-') {
            if (firstDecimal > secondDecimal) {
                System.out.println(BinaryNS.subtract(firstBinary, secondBinary));
            }
            else {
                System.out.println("Уменьшаемое < Вычетаемого");
                System.out.println(BinaryNS.subtract(secondBinary, firstBinary));
            }
        }
        if (operation == '*') {
            System.out.println(BinaryNS.multiplication(firstBinary, secondBinary));
        }
        if (operation == '/' || operation == ':') {

            if (firstDecimal > secondDecimal) {
                System.out.println(BinaryNS.division(firstBinary, secondBinary));
            }
            else {
                System.out.println("Делимое < Делителя");
                System.out.println(BinaryNS.division(secondBinary, firstBinary));
            };
        }
    }
}

