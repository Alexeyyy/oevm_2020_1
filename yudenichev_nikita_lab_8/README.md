**Лабораторная работа №8**

**Выполнил студент группы ИСЭбд-21:** Юденичев Никита

**Задание:** Разобрать и собрать системный блок ЭВМ.

**Этапы работы:** \
**1) Вид перед разборкой [Фото №1] https://www.dropbox.com/s/nb7vadl2tzs7pf8/gLuWm_7wvNs.jpg?dl=0**

**2) Комплектующие [Фото №2] https://www.dropbox.com/s/1l79mvmqckchs35/K2FPceh_y1Q.jpg?dl=0**

**3) Вид после сборки  [Фото №3] https://www.dropbox.com/s/e84l5as0mmg22vx/FgFfN8CRI7Q.jpg?dl=0**

