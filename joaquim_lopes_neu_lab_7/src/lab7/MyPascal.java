package lab7;

import java.io.FileReader;
import java.io.IOException;

public class MyPascal {
	
	public String read(){

        StringBuilder stringbuilder = new StringBuilder();

        try(FileReader filereader = new FileReader("PascalFile.pas"))
        {
            int ch;
            while((ch = filereader.read()) != -1){
                stringbuilder.append((char) ch);
            }
        }

        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        return stringbuilder.toString().replaceAll(";", ";\n");
    }


}
