package com.nodj;

import javax.swing.*;
import java.awt.*;

public class BoolPanel extends JPanel {
    private final Converter converter;

    public BoolPanel(Converter converter) {
        this.converter = converter;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        converter.toPictureMatrix(g);
    }
}
