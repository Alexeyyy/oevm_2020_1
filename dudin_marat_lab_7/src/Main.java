public class Main {
    public static void main(String[] args) {
            Reader Pascal = new Reader();
            Assembler assembler = new Assembler();
            Parser parser = new Parser(Pascal.read(), assembler);

            parser.parseBody();
            assembler.createResult();
    }
}

