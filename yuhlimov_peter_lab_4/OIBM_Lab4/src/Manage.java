public class Manage {
	final static int size = 4;
	public static void fillMatrix(int[][] matrix) {
		int maxRang = 1;
		int nowRang = 1;
		int[] safe = new int[size];
		int[] term = { 1, 0, 0, 0 };
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (j == 0) {
					double a = Math.random();
					matrix[i][j] = (int) (Math.random() * 2);
					continue;
				}
				matrix[i][j] = safe[j - 1];
			}
			safe = summ(safe, term);
		}
	}

	public static int[] summ(int[] num1, int[] num2) {
		int[] result = new int[size];
		int safe = 0;
		for (int i = 0; i < num1.length; i++) {
			int newNumb;
			if (num1[i] + num2[i] + safe >= 2) {
				newNumb = num1[i] + num2[i] + safe - 2;
				safe = 1;
			} else {
				newNumb = num1[i] + num2[i] + safe;
				safe = 0;
			}
			result[i] = newNumb;
		}
		return result;
	}

	public static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = matrix[i].length - 1; j >= 0; j--) {
				System.out.print(matrix[i][j] + " ");
				if (j == 1) {
					System.out.print("= ");
				}
			}
			System.out.print("\n");
		}
	}

	public static void DNF(int[][] matrix) {
		boolean have = false;
		for (int i = 0; i < matrix.length; i++) {
			boolean zero = false;
			if (matrix[i][0] == 0) {
				zero = true;
			}
			if (!zero) {
				if (have) {
					System.out.print(" + ");
				}
				have = true;
				System.out.print("(");
				for (int j = matrix[i].length - 1; j > 0; j--) {
					if (!zero && matrix[i][j] == 0) {
						System.out.print("-");
					}
					if (j == 1 && i == matrix.length - 1 && zero) {
						System.out.print("-");
					}
					System.out.print("X" + (matrix[i].length - j));
					if (j == 1) {
						continue;
					}
					System.out.print(" * ");
				}
				System.out.print(")");
			}
		}
	}

	public static void KNF(int[][] matrix) {
		boolean have = false;
		for (int i = 0; i < matrix.length; i++) {
			boolean zero = false;
			if (matrix[i][0] == 0) {
				zero = true;
			}
			if (zero) {
				if (have) {
					System.out.print(" * ");
				}
				have = true;
				System.out.print("(");
				for (int j = matrix[i].length - 1; j > 0; j--) {
					if (zero && matrix[i][j] == 1) {
						System.out.print("-");
					}
					if (j == 1 && i == matrix.length - 1 && zero) {
						System.out.print("-");
					}
					System.out.print("X" + (matrix[i].length - j));
					if (j == 1) {
						continue;
					}
					System.out.print(" + ");
				}
				System.out.print(")");
			}
		}
	}
}
