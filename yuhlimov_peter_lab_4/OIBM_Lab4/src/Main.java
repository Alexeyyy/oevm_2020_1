import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int[][] matrix = new int[16][5];
		Manage.fillMatrix(matrix);
		Manage.printMatrix(matrix);
		System.out.print("Enter 1 or 2 (DNF or KNF) : ");
		int property = scan.nextInt();

		switch (property) {
		case 1:
			Manage.DNF(matrix);
			break;
		case 2:
			Manage.KNF(matrix);
			break;
		}
	}
}
