import java.util.Scanner;

class Main {

    private static int numTo10ss(int ss1, char[] number) {
        int mainNumber = 0;
        for (int i = 0; i < number.length; i++) {
            int temp;
            if (number[i] >= '0' && number[i] <= '9') {
                temp = number[i] - '0';
            } else {
                temp = 10 + number[i] - 'A';
            }
            mainNumber += temp * Math.pow(ss1, number.length - i - 1);
        }
        return mainNumber;
    }

    private static String numToss2(int ss1, int ss2, char[] number) {
        StringBuilder strBuilder = new StringBuilder();
        int assistNumber1 = 48; //для перевода числа в аналогичный char
        int assistNumber2 = 55; //для перевода числа в аналогичный char(если буква)

        int num = numTo10ss(ss1, number);

        if (num == 0) {
            strBuilder.append("0");
            return strBuilder.toString();
        }

        for (int i = 0; num > 0; i++) {
            if (num % ss2 <= 9) {
                strBuilder.append((char) (num % ss2 + assistNumber1)) ;
            } else {
                strBuilder.append((char) (num % ss2 + assistNumber2)) ;
            }
            num = num / ss2;
        }

        return strBuilder.reverse().toString();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Начальная система счисления: ");
        int ss1 = in.nextInt();
        System.out.print("Конечная система счисления: ");
        int ss2 = in.nextInt();
        System.out.print("Число: ");
        char[] number = in.next().toCharArray();

        System.out.print(numToss2(ss1, ss2, number));
    }
} 