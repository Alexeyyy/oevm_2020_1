package com.company;
import static java.lang.Math.pow;

public class Per {
        private StringBuilder output = new StringBuilder();
        private long SystemPer = 0;
        private int isxSistem;
        private int konSistem;
        private String num;

    // Переменные для удобства переовда и проверки
    int n = 55;
    int m = 48;
    int d = 10;

    //Ввод данных и вызов метода перевода в 10
        public Per(int isx, int kon, String enter) {
            this.isxSistem = isx;
            this.konSistem = kon;
            this.num = enter;
            Per10();
        }
       //Метод перевода в 10 с.с.
        private Object Per10() {
            for (int i = 0; i < num.length(); i++) {
                if (Character.isLetter(num.charAt(i))) {
                    if (num.charAt(i) - n < isxSistem) {
                        SystemPer += (num.charAt(i) - n) * pow(isxSistem, num.length() - 1 - i);
                    } else {
                        return System.out.printf("Некорректное значение ");
                    }
                } else {
                    if (Character.getNumericValue(num.charAt(i)) < isxSistem) {
                        SystemPer += (num.charAt(i) - m) * pow(isxSistem, num.length() - 1 - i);
                    } else {
                        return System.out.printf("Некорректное значение ");
                    }
                }
            }
            return null;
        }
        //Метод перевода в разные с.с.
        public void translateToEnd() {
            if (SystemPer < konSistem)
            {
                if (SystemPer < d) {
                    this.output = this.output.append(SystemPer);
                } else {
                    this.output = this.output.append((char) (SystemPer + n));
                }
            } else {
                while (SystemPer / konSistem != 0) {
                    if (SystemPer % konSistem < d) {
                        this.output.append(SystemPer % konSistem);
                    } else {
                        this.output.append((char) (SystemPer % konSistem + n));
                    }
                    SystemPer /= konSistem;
                }
                if (SystemPer < d) {
                    this.output.append(SystemPer);
                } else {
                    this.output.append((char) (SystemPer + n));
                }
            }
        }
        //Конечный вывод
        public String getResult() {
            if (this.output.length() == 0) {
                this.output.replace(0, output.length(), "Введите другое значение ");
            } else {
                this.output.reverse();
            }
            return this.output.toString();
        }
    }


