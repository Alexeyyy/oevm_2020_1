## Лабораторная работа №1
**Выполнил Симонов Владимир**
**ПИбд-22**

Ссылка на видео: https://drive.google.com/file/d/165nj5waWkyJbbUQY_9yfa88A6tuO_DU7/view?usp=sharing <br>
<H3> Как запустить?</H3>
Запустить лабу через Main.java
<H3>Какие технологии использовались?</H3>
Использовалась IntelliJ IDEA и стандартные библиотеки Java. Использовал StringBuilder
<H3>Функционал программы</H3>
Переводит целое положительное число из введенной с.с в другую введенную с.с

<table bgcolor="#DCDCDC" border="2">
    <caption >
        Пример
    </caption>
    <tr>
        <th>№</th>
        <th>Входные данные</th>
        <th>Выходные данные</th>
    </tr>
    <tr>
        <td>1.</td>
        <td>16 10 ABCD</td>
        <td>43981</td>
    </tr>
    <tr>
        <td>2.</td>
        <td>10 2 9876</td>
        <td>10011010010100</td>
    </tr>
    <tr>
        <td>3.</td>
        <td>2 3 101011</td>
        <td>1121</td>
    </tr>
    <tr>
        <td>4.</td>
        <td>4 5 532</td>
        <td>Некоректное значение</td>
    </tr>    
</table>
</body>
</html>