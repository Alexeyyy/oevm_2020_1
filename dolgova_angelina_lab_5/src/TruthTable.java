public class TruthTable {
    private final int NUMBER_OF_ROWS;
    private final int NUMBER_OF_COLUMNS;
    public int[][] truthTable;

    public TruthTable(int countOfRows, int countOfColumns){
        this.NUMBER_OF_ROWS = countOfRows;
        this.NUMBER_OF_COLUMNS = countOfColumns;
        truthTable = new int[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];
        fillTruthTable();
    }

    /*
    Метод для заполнения таблицы истинности
     */
    public void fillTruthTable() {
        for (int i = 0; i < NUMBER_OF_ROWS; i++) {
            char[] fillChar = Converter.convertToBinary(i).toCharArray();
            for (int j = 0; j < NUMBER_OF_COLUMNS - 1; j++) {
                truthTable[i][j] = fillChar[j] - '0';
                truthTable[i][NUMBER_OF_COLUMNS - 1] = (int) (Math.random() * 2);
            }
        }
    }

    public int[][] getTruthTable() {
            return truthTable;
    }
}