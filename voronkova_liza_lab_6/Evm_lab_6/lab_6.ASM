format PE console

entry start

include 'win32a.inc'

section '.code' code readable executable
    start:
        push str1
        call [printf]

        push X
        push enterNumber
        call [scanf]

        push str2
        call [printf]

        push Y
        push enterNumber
        call [scanf]

        mov ecx, [X]
        add ecx, [Y]

        push ecx
        push sumStr
        call [printf]

        mov ecx, [X]
        sub ecx, [Y]
        push ecx
        push subStr
        call [printf]

        mov ecx, [X]
        imul ecx, [Y]

        push ecx
        push mulStr
        call [printf]

        mov eax, [X]
        cdq
        mov ecx, [Y]
        idiv ecx

        push eax
        push divStr
        call [printf]

        call [getch]

        push NULL
        call [ExitProcess]

section '.data' data readable writable

        str1 db 'Enter X ', 0
        str2 db 'Enter Y ', 0
        enterNumber db '%d', 0

        X dd ?
        Y dd ?

        sumStr db 'X + Y = %d', 0dh, 0ah, 0
        subStr  db 'X - Y = %d', 0dh, 0ah, 0
        mulStr db 'X * Y = %d', 0dh, 0ah, 0
        divStr db 'X / Y = %d', 0dh, 0ah, 0

        NULL = 0

section '.idata' import data readable
        library kernel, 'kernel32.dll',\
                msvctr, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvctr,\
               printf, 'printf',\
               getch, '_getch',\
               scanf, 'scanf'
