import java.util.Scanner;

public class Program
{
    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args)
    {
        Helper helper = new Helper();
        int[][] truthTable = new int[16][5];
        String resultString = "";

        helper.data(truthTable);
        helper.matrixFilling();

        System.out.printf("\n1 - DNF\n2 - KNF\nEnter: ");
        int option = in.nextInt();

        while(option != 1 && option != 2)
        {
            System.out.printf("1 - DNF\n2 - KNF\nEnter: ");
            option = in.nextInt();
        }

        if(option == 1)
            System.out.printf("Result: %s", helper.disjunctiveNormalForm());
        else
            System.out.printf("Result: %s", helper.conjunctiveNormalForm());
    }
}
