package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Укажите исходную систему счисления (2-16): ");

        int startSystem = scanner.nextInt();

        System.out.print("Укажите первое число: ");
        char[] firstNumber = scanner.next().toCharArray();

        System.out.print("Укажите второе число: ");
        char[] secondNumber = scanner.next().toCharArray();

        System.out.print("Укажите арифметическую операцию (+, -, *, /): ");
        char arithmeticOperation = scanner.next().charAt(0);

        long firstNumberDecimal = Converter.convertToDecimal(startSystem, firstNumber);
        long secondNumberDecimal = Converter.convertToDecimal(startSystem, secondNumber);

        char[] firstNumberBinary = Converter.convertToBinary(firstNumberDecimal).toCharArray();
        char[] secondNumberBinary = Converter.convertToBinary(secondNumberDecimal).toCharArray();

        switch (arithmeticOperation) {
            case '+':
                System.out.print("Сумма равна: " + BinaryArithmeticOperations.sum(firstNumberBinary, secondNumberBinary));
                break;
            case '-':
                System.out.print("Разность равна: " + BinaryArithmeticOperations.subtract(firstNumberBinary, secondNumberBinary));
                break;
            case '*':
                System.out.print("Произведение равно: " + BinaryArithmeticOperations.multiply(firstNumberBinary, secondNumberBinary));
                break;
            case '/':
                System.out.print("Частное равно: " + BinaryArithmeticOperations.divide(firstNumberBinary, secondNumberBinary));
                break;
            default:
                System.out.print("Нет такой аримфетической операции =(");
                break;
        }
    }
}
