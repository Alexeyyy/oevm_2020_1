import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class MyPanel extends JPanel {

    private int array[][];
    Methods methods = new Methods();

    public MyPanel(int array[][]) {
        this.array = array;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        methods.draw(array, g);
    }

}
