package My_Func;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the original number system (2-16): ");
        int Value = scanner.nextInt();
        System.out.print("Enter arithmetic operations (+, -, *, /): ");
        char Operations = scanner.next().charAt(0);
        System.out.print("Enter the first number: ");
        char[] First_Num = scanner.next().toCharArray();
        System.out.print("Enter the second number: ");
        char[] Second_Num = scanner.next().toCharArray();

        int First_Num_Dec = Calculator.To_Decimal(Value, First_Num);
        int Second_Num_Dec = Calculator.To_Decimal(Value, Second_Num);
        char[] First_Num_Bin = Calculator.To_Binary(First_Num_Dec).toCharArray();
        char[] Second_Num_Bin = Calculator.To_Binary(Second_Num_Dec).toCharArray();

        switch (Operations) {
            case '+':
                System.out.print(Calculator.Sum(First_Num_Bin, Second_Num_Bin));
                break;
            case '-':
                System.out.print(Calculator.Subtract(First_Num_Bin, Second_Num_Bin));
                break;
            case '*':
                System.out.print(Calculator.Multiply(First_Num_Bin, Second_Num_Bin));
                break;
            case '/':
                System.out.print(Calculator.Divide(First_Num_Bin, Second_Num_Bin));
                break;
            default:
                System.out.print("You have chosen the wrong arithmetic operation!");
                break;
        }
    }
}