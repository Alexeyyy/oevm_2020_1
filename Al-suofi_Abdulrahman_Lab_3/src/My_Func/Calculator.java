package My_Func;
import java.util.Arrays;

public class Calculator {
    /**
     * Method to transform from all numerals system to decimal
     */
    public static int To_Decimal(int Value, char[] Number) {
        int counter = 0;
        for (int i = 0; i < Number.length; i++) {
            int T=0;
            if (Number[i] >= '0' && Number[i] <= '9') {
                T = Number[i] - 48;
            }
            else if(Number[i] >= 'A' && Number[i] <= 'F') {
                T = Number[i] - 55;
            }
            counter += T * Math.pow(Value, Number.length - i - 1);
        }
        return counter;
    }
    /**
     * Method to transform from decimal system to binary.
     */
    public static String To_Binary(int Decimal) {
        StringBuilder res = new StringBuilder();
        int tmp;
        for (String coun; Decimal > 0; Decimal /= 2) {
            tmp = Decimal %  2;
            coun = Long.toString(tmp);
            res.insert(0, coun);
        }
        return res.toString();
    }
    /**
     * Method to find the sum of inputted numbers.
     */
    public static String Sum(char[] sum_1, char[] sum_2) {
        StringBuilder result = new StringBuilder();
        char remainder;
        int tmp = 0;
        int First_Num, Second_Num;
        int i = sum_1.length - 1, j = sum_2.length - 1;

        for (; i >= 0 || j >= 0 || tmp == 1; --i, --j) {

            //First binary number
            if (i < 0) { First_Num = 0; }
            else if (sum_1[i] == '0') { First_Num = 0; }
            else { First_Num = 1; }

            //Second binary number
            if (j < 0) { Second_Num = 0; }
            else if (sum_2[j] == '0') { Second_Num = 0; }
            else { Second_Num = 1; }

            int Sum = First_Num + Second_Num + tmp;
            if (Sum == 0) { remainder = '0';tmp = 0; }
            else if (Sum == 1) { remainder = '1';tmp = 0; }
            else if (Sum == 2) { remainder = '0';tmp = 1; }
            else if (Sum == 3) { remainder = '1';tmp = 1; }
            else { remainder = '1'; tmp = 1; }

            result.insert(0, remainder);
        }
        return result.toString();
    }
    /**
     * Method to find the subtract of inputted numbers.
     */
    public static String Subtract(char[] First_Num, char[] Second_Num) {
         if (To_Decimal(2, First_Num) == To_Decimal(2, Second_Num)) {
            return "0";
         }
         boolean minus = false;
         if (To_Decimal(2, First_Num) < To_Decimal(2, Second_Num)) {
            minus = true;
            char[] tmp = Second_Num;
            Second_Num = First_Num;         //меняем местами с помощью доп переменной tmp
            First_Num = tmp;
         }
         StringBuilder result = new StringBuilder();
         int dif = First_Num.length - Second_Num.length;
         char[] reverse = new char[First_Num.length];

        for (int i = 0; i < reverse.length; ++i) {      //reverse code
            if (i < dif) { reverse[i] = '1'; }
            else { if (Second_Num[i - dif] == '0') { reverse[i] = '1'; }
            else { reverse[i] = '0'; }
            }
        }
        char[] add = Sum(reverse, new char[]{'1'}).toCharArray();       //additional code
        char[] addSum = Sum(add, First_Num).toCharArray();      //additional code + minuend
        boolean zero = true;
        for (int i = 1; i < addSum.length; ++i) {
            if (addSum[i] == '1') { zero = false; }
            else { if (zero) { continue; }
            }
            result.append(addSum[i]);
        }
        if (minus) { result.insert(0, '-'); }
        return result.toString();
    }

    /**
     * Method to find the multiples of inputted numbers.
     */
    public static String Multiply(char[] First_Num, char[] Second_Num) {
        char[] result = {};

        for (int i = 0; i < To_Decimal(2, Second_Num); ++i) {
            result = (Sum(result, First_Num)).toCharArray();
        }
        return new String(result);
    }
    /**
     * Method to find the Divide of inputted numbers.
     */
    public static String Divide(char[] First_Num, char[] Second_Num) {
        int result = 0;
        while (To_Decimal(2, First_Num) >= To_Decimal(2, Second_Num)
                && !Arrays.equals(First_Num, new char[]{'0'}))
        {
            First_Num = (Subtract(First_Num, Second_Num)).toCharArray();
            result++;
        }
        return To_Binary(result);
    }
}