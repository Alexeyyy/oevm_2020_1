package com.company;

public class Main {

    public static void main(String[] args) {
        ProgramsReader programsReader = new ProgramsReader();

        String textProgramPascal = programsReader.read();

        Translator translator = new Translator(textProgramPascal);
        translator.splitCode();
        translator.writeProgram();

    }
}
