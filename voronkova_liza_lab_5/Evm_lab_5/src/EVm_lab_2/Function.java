package EVm_lab_2;

public class Function {

    static int LastTableColumn = 3;
    static int FColumn = 4;

    public static String DNF(int[][] table, int lengthI) {
        int count = 0;
        StringBuilder strBuilder = new StringBuilder();
        for (int ind = 0; ind < lengthI; ind++) {
            if (table[ind][FColumn] == 1) {
                count++;
                if(count > 1){
                    strBuilder.append(" + ");
                }
                strBuilder.append("(");
                for (int j = 0; j <= LastTableColumn; j++) {
                    if(j != LastTableColumn){
                        if (table[ind][j] == 1) {
                            strBuilder.append("X").append(j + 1).append(" * ");
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")").append(" * ");
                        }
                    }else{
                        if (table[ind][j] == 1) {
                            strBuilder.append("X").append(j + 1);
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")");
                        }
                    }
                }
                strBuilder.append(")");
                strBuilder.append("\n");
            }
        }
        return strBuilder.toString();
    }

    public static String KNF(int[][] table, int lengthI) {
        int count = 0;
        StringBuilder strBuilder = new StringBuilder();
        for (int ind = 0; ind < lengthI; ind++)
            if (table[ind][FColumn] == 0) {
                count++;
                if(count > 1){
                    strBuilder.append(" * ");
                }
                strBuilder.append("(");
                for (int j = 0; j <= LastTableColumn; j++) {
                    if(j != LastTableColumn){
                        if (table[ind][j] == 0) {
                            strBuilder.append("X").append(j + 1).append(" + ");
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")").append(" + ");
                        }
                    }else{
                        if (table[ind][j] == 0) {
                            strBuilder.append("X").append(j + 1);
                        } else {
                            strBuilder.append("(-X").append(j + 1 + ")");
                        }
                    }
                }
                strBuilder.append(")");
                strBuilder.append("\n");
            }
        return strBuilder.toString();
    }
}
