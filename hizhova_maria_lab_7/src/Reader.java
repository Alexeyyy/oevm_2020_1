import java.io.FileReader;
import java.io.IOException;

public class Reader {

    public String read(){
        StringBuilder string = new StringBuilder();

        try(FileReader reader = new FileReader("pascal.txt"))
        {
            int c;
            while((c=reader.read())!=-1){
                string.append((char) c);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        return string.toString().replaceAll(";", ";\n");
    }
}

