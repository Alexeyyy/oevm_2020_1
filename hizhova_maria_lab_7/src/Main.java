public class Main {

    public static void main(String[] args) {
        Reader reader = new Reader();
        InputAsm asm = new InputAsm();
        Convertion Convertion = new Convertion(reader.read(), asm);
        Convertion.parseBody();
        asm.create();
    }
}
