# Лабораторная работа №2

##### студента ПИбд-21 Резникова Сергея

Задание: Разработать программный продукт для минимизации булевой функции из четырех переменных.

Видео-демонстрацию работоспособности программы вы можете увидеть по ссылке https://drive.google.com/file/d/1I_N1y1jINRBnJSQpsTPnIegz8-4eVotv/view?usp=sharing (короткое видео, имеющее своей целью лишь доказать, что "результат работы программы" я написал не руками).

Результат работы программы. 


<html>
<body>
<pre>
"C:\Program Files\Java\jdk-13.0.2\bin\java.exe" "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.2.2\lib\idea_rt.jar=56571:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.2.2\bin" -Dfile.encoding=UTF-8 -classpath C:\Users\Mvideo\IdeaProjects\Laba4_oevm\out\production\Laba4_oevm Main
  1  1  1  0 = 0
  1  1  1  1 = 1
  0  0  1  1 = 0
  0  1  0  0 = 1
  1  0  0  0 = 0
  0  0  0  0 = 1
  1  0  1  0 = 1
  0  0  0  1 = 1
  1  0  1  0 = 1
  0  1  0  1 = 0
  0  0  1  0 = 0
  0  0  0  1 = 0
  0  0  1  1 = 1
  0  1  1  0 = 0
  1  0  0  1 = 0
  1  0  0  1 = 1
1 - СКНФ, 2 - СДНФ, 3 - СКНФ + СДНФ
3
f(a, b, c, d) = (-a + -b + -c + d) * (a + b + -c + -d) * (-a + b + c + d)
 * (a + -b + c + -d) * (a + b + -c + d) * (a + b + c + -d) * (a + -b + -c + d)
  * (-a + b + c + -d) 
f(a, b, c, d) = (a * b * c * d) + (-a * b * -c * -d) + (-a * -b * -c * -d)
 + (a * -b * c * -d) + (-a * -b * -c * d) + (a * -b * c * -d) + (-a * -b * c * d)
  + (a * -b * -c * d) 

Process finished with exit code 0

</pre>
</body>
</html>

