
public class Main {

	public static void main(String[] args) {
		pascalklass pascal = new pascalklass();

		Assembler assembler = new Assembler();

		Parser parser = new Parser(pascal.read(), assembler);
		parser.parseBody();
		assembler.create();
	}

}
