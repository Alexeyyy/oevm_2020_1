﻿var
    x, y: integer;
    res1, res2, res3, res4: integer;
begin
    write('Enter x: '); readln(x);
    write('Enter y: '); readln(y);
    res1 := x + y; write('x + y = '); writeln(res1);
    res2 := x - y; write('x - y = '); writeln(res2);
    res3 := x * y; write('x * y = '); writeln(res3);
    res4 := x / y; write('x / y = '); writeln(res4);
end.
