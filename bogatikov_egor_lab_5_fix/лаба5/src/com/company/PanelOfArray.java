package com.company;
import javax.swing.*;
import java.awt.*;


public class PanelOfArray extends JPanel {

    private int numbersArray[][];
    Calculations methods = new Calculations();

    public PanelOfArray(int numbersArray[][]) {
        this.numbersArray = numbersArray;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Calculations.draw(numbersArray, g);
    }
}
