package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;
import java.util.Arrays;

public class DrawWindow {
    private BufferedImage buffer;
    private int[] bufferData;
    private Graphics bufferGraphics;
    private int clearColor;

    private JFrame window;
    private Canvas content;


    private JButton bDNF;
    private JButton bKNF;


    public DrawWindow(int width, int height, String title) {

        window = new JFrame(title);
        content = new Canvas();

        window.setPreferredSize(new Dimension(width, height));
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);

        window.setLayout(new BorderLayout());
        window.add(content, BorderLayout.CENTER);

        // window.add(bDNF, BorderLayout.SOUTH);
        //6 window.add(bKNF, BorderLayout.SOUTH);
        window.pack();
        window.setVisible(true);
        window.setLocationRelativeTo(null);

        buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        bufferData = ((DataBufferInt) buffer.getRaster().getDataBuffer()).getData();
        bufferGraphics = buffer.getGraphics();
        ((Graphics2D) bufferGraphics).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        clearColor = 0Xffffffff;
        ((Graphics2D) bufferGraphics).setPaint(Color.BLACK);
        bufferGraphics.setFont(new Font("TimesRoman", Font.PLAIN, 22));
    }

    private void draw() {

        Graphics g = content.getGraphics();
        g.drawImage(buffer, 0, 0, null);
    }

    public void drawDNF(String normalForm) {
        Arrays.fill(bufferData, clearColor);
        bufferGraphics.drawString("X1 X2 X3 X4 ", 20, 20);
        draw(normalForm, "ДНФ");
        draw();
    }

    public void drawKNF(String normalForm) {
        Arrays.fill(bufferData, clearColor);
        bufferGraphics.drawString("X1 X2 X3 X4 ", 20, 20);
        draw(normalForm, "КНФ");


        draw();
    }

    private void draw(String normalForm, String type) {
        bufferGraphics.fillRect(20, 20, 125, 2);

        ArrayList<Integer> data = encoding(normalForm);
        int shiftY = 0;
        int numOfConnection = 0;
        int numberOfTerms = 0;

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i) == 10) {
                if (type.equals("ДНФ")) {
                    drawANDElement(210, 40 + shiftY + (i - numOfConnection) * 20, numOfConnection);
                } else if (type.equals("КНФ")) {
                    drawORElement(210, 40 + shiftY + (i - numOfConnection) * 20, numOfConnection);
                }
                drawConnection(240, 40 + shiftY + (i - numOfConnection/2) * 20, numberOfTerms);
                numOfConnection = 0;
                shiftY += 0;
                numberOfTerms++;
                continue;
            }
            numOfConnection++;
            bufferGraphics.fillRect(31 + (Math.abs(data.get(i)) - 1) * 34, 40 + shiftY + i * 20, 190 - (Math.abs(data.get(i)) - 1) * 34, 2);
            bufferGraphics.fillOval(28 + (Math.abs(data.get(i)) - 1) * 34, 38 + i * 20, 7, 7);
            if (data.get(i) < 0) {
                drawInvertElement(160, 40 + shiftY + i * 20);
            }
        }
        for (int i = 0; i < 4; i++) {
            bufferGraphics.fillRect(31 + i * 34, 20, 2, 100*numberOfTerms);
        }
        if (type.equals("ДНФ")) {
            drawORElement(530, 60 , numberOfTerms);
        } else if (type.equals("КНФ")) {
            drawANDElement(530, 60 , numberOfTerms);
        }
        bufferGraphics.fillRect(550, 60+(numberOfTerms/2)* 20, 100, 2 );
        bufferGraphics.drawString("F", 600, 50+(numberOfTerms/2)* 20);
    }

    private void drawInvertElement(int x, int y) {
        Polygon polygon = new Polygon();
        polygon.addPoint(x, y - 10);
        polygon.addPoint(x + 17, y);
        polygon.addPoint(x, y + 10);
        bufferGraphics.fillPolygon(polygon);
        bufferGraphics.drawOval(x + 13, y - 4, 8, 8);
    }

    private void drawANDElement(int x, int y, int numConnection) {

        bufferGraphics.fillRect(x, y, 20+numConnection, 4 + (numConnection - 1) * 20);
        bufferGraphics.fillOval(x + 5 +numConnection/2, y, 30, 4 + (numConnection - 1) * 20);
    }

    private void drawORElement(int x, int y, int numConnection){
        Area area = new Area(new Rectangle(x, y, 20+numConnection, 4 + (numConnection - 1) * 20));
        area.add(new Area(new Ellipse2D.Double(x + numConnection/2, y, 40, 4 + (numConnection - 1) * 20)));

        area.subtract(new Area( new Ellipse2D.Double(x - 20, y, 30, 4 + (numConnection - 1) * 20)));
        ((Graphics2D)bufferGraphics).fill(area);
    }

    private void drawConnection(int x, int y, int numberOfTerms) {

        bufferGraphics.drawLine(x,y,x+50+numberOfTerms * 15, y);
        bufferGraphics.drawLine(x+50+numberOfTerms * 15,y,x+50+numberOfTerms * 15, 60+numberOfTerms*20);
        bufferGraphics.drawLine(x+50+numberOfTerms * 15,60+numberOfTerms*20,550, 60+numberOfTerms*20);

      //  bufferGraphics.fillRect(x, y, 40 + numberOfTerms * 10, 2);
      //  bufferGraphics.fillRect(x + 40 + numberOfTerms * 10, y - numberOfTerms * 10, 2,numberOfTerms * 10);
       // bufferGraphics.fillRect(x + 40 + numberOfTerms * 10, y - numberOfTerms * 10, 60 - numberOfTerms * 5, 2);
    }

    private ArrayList<Integer> encoding(String strData) {
        ArrayList<Integer> data = new ArrayList<>();

        char[] charArrData = strData.toCharArray();
        for (int i = 0; i < charArrData.length; i++) {

            if (charArrData[i] == '!') {
                i += 2;
                data.add(-(charArrData[i] - '0'));
                continue;
            }
            if (charArrData[i] == 'X') {
                i++;
                data.add(charArrData[i] - '0');
                i++;
            }
            if (charArrData[i] == ')') {
                data.add(10);
            }
        }
        return data;
    }
}
