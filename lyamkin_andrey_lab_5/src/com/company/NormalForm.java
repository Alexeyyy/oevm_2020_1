package com.company;

public  class NormalForm {

    public static final String[] variables= {"X1", "X2", "X3", "X4" };

    public static String toDNF(int[][] table) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < table.length; i++) {
            if (table[i][table[i].length - 1] == 1) {
                sb.append("(");

                for (int j = 0; j < table[i].length - 1; j++) {
                    if (table[i][j] == 0) { sb.append("!"); }
                    sb.append(variables[j]);
                    if (j < table[i].length - 2) { sb.append(" * "); }
                    else { sb.append(")"); }
                }
                sb.append(" + ");
            }
        }
        sb.delete(sb.length()-3, sb.length()-1);
        return sb.toString();
    }

    public static String toKNF(int[][] table) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < table.length; i++) {
            if (table[i][table[i].length - 1] == 0) {
                sb.append("(");

                for (int j = 0; j < table[i].length - 1; j++) {
                    if (table[i][j] == 1) { sb.append("!"); }
                    sb.append(variables[j]);
                    if (j < table[i].length - 2) { sb.append(" + "); }
                    else { sb.append(")"); }
                }
                sb.append(" * ");
            }
        }
        sb.delete(sb.length()-3, sb.length()-1);
        return sb.toString();
    }
}
