package com.company;

public class TruthTable {

    private int[][] table;
    public TruthTable(){
        table = new int[16][5];
    }
    public void printTable(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("| X1| X2| X3| X4| F |\n");
        for (int i = 0; i < table.length; i++) {
            stringBuilder.append("|");
            for (int j = 0; j < table[i].length; j++) {
                stringBuilder.append(" ").append(table[i][j]).append(" |");
            }
            stringBuilder.append(" \n");
        }
        System.out.print(stringBuilder);
    }
    public void fillTable(){

        int setValues = 0;
        for (int i = 0; i < table.length; i++) {
            char[] binSetValues = toBinaryNotation(setValues);
            for (int j = 0; j < table[i].length - 1; j++) {
                table[i][j] = binSetValues[j] - '0';
            }
            table[i][table[i].length-1] = (int)(Math.random()*2);
            setValues++;
        }
    }
    private char[] toBinaryNotation(int num){

        StringBuilder sb = new StringBuilder();
        int remains = num;
        while (remains > 0){
            sb.append(remains%2);
            remains/=2;
        }
        while(sb.length()<4){
            sb.append("0");
        }
        return sb.reverse().toString().toCharArray();
    }

    public int[][] getTable() {
        return table;
    }
}
