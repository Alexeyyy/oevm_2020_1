package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

       TruthTable table = new TruthTable();
       DrawWindow window = new DrawWindow(800,800, "Схема");

        while (true){
            System.out.println("Введите 1 для ДНФ, 2 для КНФ: ");
            String in = scanner.next();

            System.out.println("\nСлучайно заполненная таблица истинности:\n");
            table.fillTable();
            table.printTable();

            if(in.equals("1")){
                System.out.println("ДНФ: ");
                String nf =NormalForm.toDNF(table.getTable());
                System.out.println(nf);
                window.drawDNF(nf);
            }else if(in.equals("2")){
                System.out.println("КНФ: ");
                String nf = NormalForm.toKNF(table.getTable());
                System.out.println(nf);
                window.drawKNF(nf);
            }else{
                System.out.println("Сорре, неккоректный ввод.");
            }
        }

    }

}
