import java.util.Random;

public class MinimalizationBooleanFunctions {

    private int[][] arrayForm = {
            {0, 0, 0, 0},
            {0, 0, 0, 1},
            {0, 0, 1, 0},
            {0, 0, 1, 1},
            {0, 1, 0, 0},
            {0, 1, 0, 1},
            {0, 1, 1, 0},
            {0, 1, 1, 1},
            {1, 0, 0, 0},
            {1, 0, 0, 1},
            {1, 0, 1, 0},
            {1, 0, 1, 1},
            {1, 1, 0, 0},
            {1, 1, 0, 1},
            {1, 1, 1, 0},
            {1, 1, 1, 1},
    };

    private int[]arrayRandom = new int[arrayForm.length];

    private void randomFilling(){
        for(int i = 0; i < arrayRandom.length; i++) {
            Random random = new Random();
            arrayRandom[i] = random.nextInt(2); // цифра 2 означает, что числа в рандоме от 0 до 1
        }
    }

    public void printForm(){
        StringBuilder strForm = new StringBuilder();
        randomFilling();

        strForm.append("X1  X2  X3  X4  F" + '\n');

        for(int i = 0; i < arrayForm.length; i++){
            for(int j = 0; j < arrayForm[i].length; j++){
                strForm.append(arrayForm[i][j] + "   ");
            }
            strForm.append(arrayRandom[i] + "   " + '\n');
        }
        System.out.println(strForm);
    }

    public void creatureDNF(){
        int countEntry = 0;
        StringBuilder strDNF = new StringBuilder();

        for(int i = 0; i < arrayForm.length; i++){

            if(arrayRandom[i] == 1){

                if(countEntry > 0){
                    strDNF.append(" + ");
                }

                strDNF.append("(");

                for(int j = 0; j < arrayForm[i].length; j++){

                    if(arrayForm[i][j] == 0) {
                        strDNF.append("!");
                    }
                    strDNF.append("X" + (j + 1)); // нумерация Х начинается с 1, поэтому j + 1
                    if(j < arrayForm[i].length - 1){ // ставить * перед последним X, поэтому - 1
                        strDNF.append(" * ");
                    }
                }
                strDNF.append(")");
                countEntry++;
            }
        }
        strDNF.append('\n');
        System.out.println(strDNF);
    }

    public void creatureKNF(){
        int countEntry = 0;
        StringBuilder strKNF = new StringBuilder();

        for(int i = 0; i < arrayForm.length; i++){
            if(arrayRandom[i] == 0){
                if(countEntry > 0){
                    strKNF.append(" * ");
                }
                strKNF.append("(");
                for(int j = 0; j < arrayForm[i].length; j++){
                    if(arrayForm[i][j] == 1){
                        strKNF.append("!");
                    }
                    strKNF.append("X" + (j + 1)); // нумерация Х начинается с 1, поэтому j + 1
                    if(j < arrayForm[i].length - 1){  // ставить + перед последним X, поэтому - 1
                        strKNF.append(" + ");
                    }
                }
                strKNF.append(")");
                countEntry++;
            }
        }
        strKNF.append('\n');
        System.out.println(strKNF);
    }
}
