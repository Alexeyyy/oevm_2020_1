## Лабораторная работа № 7 
### Шмелькова Марина, ИСЭбд-21


#### **Задание:** 
разработать транслятор программ Pascal-FASM. Результат работы программы является программа для ассемблера FASM, идентичная по функционалу исходной программе.

#### Исходный код:
```var
x, y: integer;
res1, res2, res3, res4: integer;
begin
    write('input x: '); readln(x);
    write('input y: '); readln(y);
    res1 := x + y; write('x + y = '); writeln(res1);
    res2 := x - y; write('x - y = '); writeln(res2);
    res3 := x * y; write('x * y = '); writeln(res3);
    res4 := x / y; write('x / y = '); writeln(res4);
end.
```

#### **Технологии**:

В качестве языка программирования был выбран язык **Java**

В качестве среды разработки **IntelliJ IDEA**

Для ассемблера среда разработки **FASMW**
#### Запуск

Чтобы запустить программу в рамках лабароторной работы №7, необходимо перейти по пути : *Lab_7\src\com\company* и открыть файл Main.java

В программе представлены классы :

* Assebler
* Pascal
* Parser
* Main

#### Пример работы программы:

https://www.dropbox.com/s/aianrk9i2w787ru/InShot_20201110_195958994.mp4?dl=0
