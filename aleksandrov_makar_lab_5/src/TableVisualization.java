import javax.swing.*;
import java.awt.*;

public class TableVisualization extends JPanel {
    private static String _operation;
    private final int[][] _array;

    public TableVisualization(int[][] array) {
        _array = array;
    }

    public static void setOperation(String operation){
        _operation = operation;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        DrawMethods.draw(_array, _operation, g);
    }
}
