import java.io.*;

public class From_Pascal {
    public String read() {

        StringBuilder string = new StringBuilder();

        try (FileReader fr = new FileReader("as.pas")) {
            int symbol;

            while((symbol = fr.read()) != -1) {
                string.append((char) symbol);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        return string.toString().replaceAll(";", ";\n");
    }
}
