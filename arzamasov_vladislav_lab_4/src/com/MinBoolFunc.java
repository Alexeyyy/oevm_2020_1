package com;

public class MinBoolFunc {
    final int numCol;
    final int numRow;
    private int[][] tablIst;
    String rez = "";

    public MinBoolFunc(int numCol, int numRow, int[][] table) {
        this.numCol = numCol;
        this.numRow = numRow;
        tablIst = table;
    }

    public String DNF() {
        int count = 0;
        for (int i = 0; i < numRow; i++) {
            if (tablIst[i][numCol - 1] == 1) {
                if (count > 0) {
                    rez += " + ";
                }
                if (tablIst[i][0] == 0) {
                    rez += "( - X1 ";
                } else {
                    rez += "( X1 ";
                }
                if (tablIst[i][1] == 0) {
                    rez += " * -X2 ";
                } else {
                    rez += " * X2 ";
                }
                if (tablIst[i][2] == 0) {
                    rez += " * -X3 ";
                } else {
                    rez += " * X3 ";
                }
                if (tablIst[i][3] == 0) {
                    rez += "* -X4 )";
                } else {
                    rez += "* X4 )";
                }
                count++;
            }
        }
        return rez;
    }

    public String KNF() {
        int count = 0;
        for (int i = 0; i < numRow; i++) {
            if (tablIst[i][numCol - 1] == 0) {
                if (count > 0) {
                    rez += " * ";
                }
                if (tablIst[i][0] == 1) {
                    rez += "( - X1 ";
                } else {
                    rez += " ( X1 ";
                }
                if (tablIst[i][1] == 1) {
                    rez += " - X2 ";
                } else {
                    rez += " + X2 ";
                }
                if (tablIst[i][2] == 1) {
                    rez += " - X3 ";
                } else {
                    rez += " + X3 ";
                }
                if (tablIst[i][3] == 1) {
                    rez += " - X4 )";
                } else {
                    rez += " + X4 )";
                }
                count++;
            }
        }
        return rez;
    }

    public void printTab() {
        System.out.println("x1 x2 x3 x4 F");
        for (int i = 0; i < numRow; i++) {
            for (int j = 0; j < numCol; j++) {
                System.out.print(tablIst[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
