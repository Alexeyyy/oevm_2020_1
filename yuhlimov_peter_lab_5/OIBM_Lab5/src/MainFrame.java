import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainFrame {

	private JFrame frame;
	private JTextField textField;
	private JTextField[][] Matrix = new JTextField[17][5];
	private JTextField txtDfs;
	private int[][] matrix;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame window = new MainFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(Color.BLACK);
		frame.setBounds(100, 100, 645, 475);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JTextArea ResultTextArea = new JTextArea();
		ResultTextArea.setEditable(false);
		ResultTextArea.setBounds(278, 14, 213, 600);
		frame.getContentPane().add(ResultTextArea);

		//Creating a truth table
		JButton ButtonCreate = new JButton("Start");
		ButtonCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ResultTextArea.setText("");
				matrix = new int[16][5];
				Manage.fillMatrix(matrix);
				for (int i = 0; i < Matrix.length; i++) {
					for (int j = 0; j < Matrix[i].length; j++) {
						if (i != 0) {
							Matrix[i][j] = new JTextField();
							Matrix[i][j].setForeground(Color.BLACK);
							Matrix[i][j].setText(" " + matrix[i - 1][matrix[i - 1].length - 1 - j]);
							Matrix[i][j].setBounds((j + 1) * 20, (i + 1) * 20, 20, 20);
							frame.getContentPane().add(Matrix[i][j]);
							Matrix[i][j].setColumns(10);
							Matrix[i][j].setBorder(new LineBorder(Color.BLACK));
							Matrix[i][j].setEditable(false);
						} else {
							Matrix[i][j] = new JTextField();
							Matrix[i][j].setForeground(Color.BLACK);
							if (j < matrix[i].length - 1) {
								Matrix[i][j].setText(" X" + (j + 1));
							} else {
								Matrix[i][j].setText(" F");
							}
							Matrix[i][j].setBounds((j + 1) * 20, (i + 1) * 20, 20, 20);
							frame.getContentPane().add(Matrix[i][j]);
							Matrix[i][j].setColumns(10);
							Matrix[i][j].setBorder(new LineBorder(Color.BLACK));
							Matrix[i][j].setEditable(false);
						}
					}
				}
			}
		});
		ButtonCreate.setBounds(179, 10, 89, 23);
		frame.getContentPane().add(ButtonCreate);

		JButton ButtonDNF = new JButton("DNF");
		ButtonDNF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultTextArea.setText(Manage.DNF(matrix));
				for (int i = 1; i < Matrix.length; i++) {
					for (int j = 0; j < Matrix[i].length; j++) {
						if (matrix[i - 1][0] == 1) {
							Matrix[i][j].setBorder(new LineBorder(Color.RED));
						} else {
							Matrix[i][j].setBorder(new LineBorder(Color.BLACK));
						}
					}
				}
			}
		});
		ButtonDNF.setBounds(179, 39, 89, 23);
		frame.getContentPane().add(ButtonDNF);

		JButton ButtonKNF = new JButton("KNF");
		ButtonKNF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultTextArea.setText(Manage.KNF(matrix));
				for (int i = 1; i < Matrix.length; i++) {
					for (int j = 0; j < Matrix[i].length; j++) {
						if (matrix[i - 1][0] == 0) {
							Matrix[i][j].setBorder(new LineBorder(Color.RED));
						} else {
							Matrix[i][j].setBorder(new LineBorder(Color.BLACK));
						}
					}
				}
			}
		});
		ButtonKNF.setBounds(179, 70, 89, 23);
		frame.getContentPane().add(ButtonKNF);
	}
}
