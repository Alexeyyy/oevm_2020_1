package com.company;

public class Main {

    public static void main(String[] args) {
        CodeTranslator translator = new CodeTranslator();
        Reader reader = new Reader();
        reader.readPascalFile("program.pas", translator);
        Writer writer = new Writer();
        writer.createAssemblerFile(translator);
    }
}
