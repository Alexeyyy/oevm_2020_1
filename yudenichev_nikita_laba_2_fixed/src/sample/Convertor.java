package sample;

public class Convertor {
    private int initial;
    private int desired;
    private char[] number;

    public Convertor(int initial, int desired, char[] number){
        this.initial = initial;
        this.desired = desired;
        this.number = number;
    }

    public static long convertToDec(char[] number, int initial) {
        long numeralTen = 0;
        final char zero = '0';
        final char nine = '9';
        final char theA = 'A';

        for (int i = 0; i < number.length; i++) {
            int currentNumeral = 1;
            if (number[i] >= zero && number[i] <= nine) {
                currentNumeral = number[i] - zero;
            } else if (Character.isLetter(number[i])) {
                currentNumeral = 10 + number[i] - theA;
            }
            numeralTen = (long) ((double) numeralTen + (double) currentNumeral * Math.pow(initial, number.length - i - 1));
        }
        return numeralTen;
    }

    public static String convertToDesiredNumericSystem(int desired, long numeralTen) {
        StringBuilder finish = new StringBuilder();
        long residue;

        for (String Char = ""; numeralTen > 0; numeralTen /= desired) {
            residue = numeralTen % (long) desired; //остаток
            Char = "";
            if (numeralTen % (long) desired < 10) {
                Char = Long.toString(residue);
            } else {
                Char = Char + (char) ((int)'A' + residue - 10);
            }
            finish.insert(0, Char);
        }
        return finish.toString();
    }

    public void checkResultOutput(){
        int minLimit = 2, maxLimit = 16;
        if ((initial >= minLimit && initial <= maxLimit) && (desired >= minLimit && desired <= maxLimit)) {
            System.out.println("Итог: " + convertToDesiredNumericSystem(desired, convertToDec(number, initial)));
        } else {
            System.out.println("Ошибка!");
        }
    }
}
