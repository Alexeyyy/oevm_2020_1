package com.company;

public class Helper {
    static int sizeI = 16;
    static int sizeJ = 5;
    static int X4 = 3;
    static int F = 4;

    public void printArray(int[][] array){
        for(int i = 0; i< sizeI; i++){
            for(int j = 0; j< sizeJ; j++){
                if(j == sizeJ - 1){
                    System.out.print("\t");
                }
                System.out.print("\t" + array[i][j]+" ");
            }
            System.out.print("\n");
        }
    }

    public String DNF(int[][] array) {
        int counter = 0;
        StringBuilder sb = new StringBuilder();
        for (int ind = 0; ind < sizeI; ind++) {
            if (array[ind][F] == 1) {
                counter++;
                if(counter > 1){
                    sb.append(" + ");
                }
                sb.append("(");
                for (int j = 0; j <= X4; j++) {
                    if(j != X4){
                        if (array[ind][j] == 1) {
                            sb.append("X").append(j + 1).append(" * ");
                        } else {
                            sb.append("(-X").append(j + 1 + ")").append(" * ");
                        }
                    }else{
                        if (array[ind][j] == 1) {
                            sb.append("X").append(j + 1);
                        } else {
                            sb.append("(-X").append(j + 1 + ")");
                        }
                    }
                }
                sb.append(")");
            }
        }
        return sb.toString();
    }

    public String KNF(int[][] array) {
        int counter = 0;
        StringBuilder sb = new StringBuilder();
        for (int ind = 0; ind < sizeI; ind++)
            if (array[ind][F] == 0) {
                counter++;
                if(counter > 1){
                    sb.append(" * ");
                }
                sb.append("(");
                for (int j = 0; j <= X4; j++) {
                    if(j != X4){
                        if (array[ind][j] == 1) {
                            sb.append("X").append(j + 1).append(" + ");
                        } else {
                            sb.append("(-X").append(j + 1 + ")").append(" + ");
                        }
                    }else{
                        if (array[ind][j] == 1) {
                            sb.append("X").append(j + 1);
                        } else {
                            sb.append("(-X").append(j + 1 + ")");
                        }
                    }
                }
                sb.append(")");
            }
        return sb.toString();
    }
}
