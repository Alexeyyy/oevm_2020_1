package com.company;

import java.io.FileReader;
import java.io.IOException;

public class Reader {
    public String read() {
        StringBuilder myString = new StringBuilder();

        try (FileReader reader = new FileReader("OEVM_lab7.pas"))
        {
            int myChar;
            while ((myChar = reader.read()) != -1) {
                myString.append((char) myChar);
            }
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return myString.toString().replaceAll(";", ";\n");
    }
}

