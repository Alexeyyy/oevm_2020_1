package com.company;

public class Main {

    public static void main(String[] args) {
        Reader myReader = new Reader();
        Writer myWriter = new Writer();
        Parser parser = new Parser(myReader.read(), myWriter);
        parser.parse();
        myWriter.write();
    }
}
