package com.company;
import java.util.*;

public class Main {
    static final int COLUMN = 16; // длина столбца
    static final int SYMBOL = 5; // длина строки

    public static void main(String[] args) {
        System.out.println("Для КНФ 1");
        System.out.println("Для ДНФ 2");

        Scanner number = new Scanner(System.in);
        int executionOption = number.nextInt(); // переменная которая будет определять по кнф решаем или по днф

        int[][] numbersArray = new int[COLUMN][SYMBOL]; // массив всех чисел
        Calculations.outputOnDisplay(numbersArray); // функция для вывода таблицы
        if (executionOption == 1) {
            Calculations.knfFormula(numbersArray); // функция для кнф формулы
        } else {
            Calculations.dnfFormula(numbersArray); // функция для днф формулы
        }
    }
}
