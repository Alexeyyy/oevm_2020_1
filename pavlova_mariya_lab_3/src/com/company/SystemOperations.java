package com.company;

public class SystemOperations {

    public static int translateToDecSystem(int inNumSystem, String number) {
        int res = 0;
        char[] numberArr = number.toCharArray();
        int countDecSystemDigit = 10;

        for (int i = 0; i < numberArr.length; i++) {
            if (Character.getNumericValue(numberArr[i]) >= inNumSystem) {
                return -1;
            }
            int power = (int)Math.pow(inNumSystem, number.length()-i-1);
            if (numberArr[i] >= '0' && numberArr[i] < '9') {
                res += Character.getNumericValue(numberArr[i])*power;
            }
            else if (numberArr[i] >= 'A' && numberArr[i] <= 'F') {
                res += (numberArr[i] - 'A' + countDecSystemDigit) * power;
            }
        }
        return res;
    }

    public static StringBuffer translateToBinSystem(int number) {
        int outNumSystem = 2;
        int maxDigitValue = 9;
        int maxLiteralValue = 15;
        int countDecSystemDigit = 10;
        if (number == -1) {
            return new StringBuffer("Введенная цифра отсутствует в исходной СС");
        }
        StringBuffer res = new StringBuffer();
        if (number/outNumSystem == 0) {
            return res.append(number);
        }
        while (number/outNumSystem != 0 ) {
            int remainder = number%outNumSystem;
            if (remainder <= maxDigitValue) {
                res.append(remainder);
            } else if (remainder > maxDigitValue &&  remainder <= maxLiteralValue) {
                char subRes = (char)(remainder + 'A' - countDecSystemDigit);
                res.append(subRes);
            }
            number = number/outNumSystem;
        }
        if (number > maxDigitValue) {
            res.append((char)(number + 'A' - countDecSystemDigit));
            return res.reverse();
        }
        res.append(number);
        return res.reverse();
    }

    public static String fillingZero(String number, int length) {
        String newString = new String();
        if (number.length() != length) {
            for (int i = 0; i < length-number.length(); i++) {
                newString += "0";
            }
        }
        newString += number;
        return newString;
    }

    public static StringBuilder add(String numberOne, String numberTwo) {
        int maxLength = numberOne.length() >= numberTwo.length() ? numberOne.length() : numberTwo.length();
        char[] numberOneArr = fillingZero(numberOne, maxLength).toCharArray();
        char[] numberTwoArr = fillingZero(numberTwo, maxLength).toCharArray();
        int digit;
        int rank = 0;
        int maxCountDigit = 2;
        int maxDigit = maxCountDigit - 1;
        StringBuilder result = new StringBuilder();

        for (int i = maxLength - 1; i >= 0; i--) {
            digit = Character.getNumericValue(numberOneArr[i])+Character.getNumericValue(numberTwoArr[i])+rank;
            if (digit > maxDigit) {
                result.append(digit-maxCountDigit);
                rank = 1;
            }
            if (digit <= maxDigit) {
                result.append(digit);
                rank = 0;
            }
        }
        if (rank != 0) {
            result.append(rank);
        }
        return result.reverse();
    }

    public static StringBuilder deleteExtraZeros(StringBuilder number) {
        StringBuilder newString = new StringBuilder();
        int pos = 0;
        char[] numberArr = number.toString().toCharArray();
        for (int i = 0; i < number.length(); i++) {
            int value = Character.getNumericValue(numberArr[i]);
            if (value != 0) {
                pos = i;
                break;
            }
        }
        for (int i = pos; i < numberArr.length; i++) {
            newString.append(numberArr[i]);
        }
        return newString;
    }

    public static StringBuilder subtract(String numberOne, String numberTwo) {
        int maxLength = numberOne.length() >= numberTwo.length() ? numberOne.length() : numberTwo.length();
        char[] numberOneArr = fillingZero(numberOne, maxLength).toCharArray();
        char[] numberTwoArr = fillingZero(numberTwo, maxLength).toCharArray();
        int digit;
        int rank = 0;
        StringBuilder result = new StringBuilder();
        for (int i = maxLength - 1; i >= 0; i--) {
            digit = numberOneArr[i]-numberTwoArr[i] - rank;
            if (digit >= 0) {
                result.append(digit);
                rank = 0;
            }
            if (digit == -1) {
                rank = 1;
                result.append(1);
            }
            if (digit == -2) {
                for (int j = i; j >= 0; j--) {
                    if (numberOneArr[j] == 0) {
                        numberOneArr[j] = 1;
                    }
                    if (numberOneArr[j] == 1) {
                        numberOneArr[j] = 0;
                        break;
                    }
                }
                result.append(0);
                rank = 1;
            }
        }
        return deleteExtraZeros(result.reverse());
    }

    public static String addExtraZeros(String number, int countZeros) {
        for (int i = 0; i < countZeros; i++) {
            number += "0";
        }
        return number;
    }

    public static StringBuilder multiply(String numberOne, String numberTwo) {
        if (numberTwo.length() > numberOne.length()) {
            String temp = numberOne;
            numberOne = numberTwo;
            numberTwo = temp;
        }

        int maxLength = numberOne.length() >= numberTwo.length() ? numberOne.length() : numberTwo.length();
        char[] numberOneArr = fillingZero(numberOne, maxLength).toCharArray();
        char[] numberTwoArr = fillingZero(numberTwo, maxLength).toCharArray();
        String result = "";
        String subResult = "";

        for (int i = maxLength - 1; i >= 0; i--) {
            if (Character.getNumericValue(numberTwoArr[i]) == 1) {
                subResult = addExtraZeros(numberOne, numberOne.length()-i-1);
                result = add(subResult, result).toString();
            }
        }
        return new StringBuilder(result);
    }

    public static StringBuffer divide(String numberOne, String numberTwo) {
        int result = 0;
        if (numberOne != "0" && numberTwo != "0") {
            while (translateToDecSystem(2, numberOne) >= translateToDecSystem(2, numberTwo)) {
                numberOne = (subtract(numberOne, numberTwo)).toString();
                result++;
            }
        }
        return translateToBinSystem(result);
    }
}
