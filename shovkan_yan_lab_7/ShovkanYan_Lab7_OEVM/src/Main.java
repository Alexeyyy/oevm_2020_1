import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        Reader reader = new Reader();
        Writer writer = new Writer();
        Analyzer parseString = new Analyzer(reader.read(), writer);
        parseString.parseParthBody();
        writer.write();

    }
}
