
public class Main {

    public static void main(String[] args) {

        ReaderPascal readPascal = new ReaderPascal();
        WriterAssembler writeAssembler = new WriterAssembler();
        CodeDivider divideCode = new CodeDivider(readPascal.read(), writeAssembler);

        divideCode.divideVarAndBody();
        writeAssembler.write();
    }
}