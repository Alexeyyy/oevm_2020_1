import java.util.ArrayList;
import java.util.regex.*;

public class CodeDivider {
    WriterAssembler writeAssembler;

    private String stringOfFile;
    private String strOfVariables;
    private String strOfBody;

    private ArrayList<String> arrayVarsString = new ArrayList<>();
    private ArrayList<String> arrayCodeString = new ArrayList<>();
    private ArrayList<String> arrayVars = new ArrayList<>();

    public CodeDivider(String str, WriterAssembler writeAssembler) {
        this.stringOfFile = str;
        this.writeAssembler = writeAssembler;
    }

    public void divideVarAndBody() {

        Pattern patternForVariable = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherVariables = patternForVariable.matcher(stringOfFile);

        while (matcherVariables.find()) {
            strOfVariables = stringOfFile.substring(matcherVariables.start(), matcherVariables.end());
        }

        Pattern patternBody = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherBody = patternBody.matcher(stringOfFile);

        while (matcherBody.find()) {
            strOfBody = stringOfFile.substring(matcherBody.start(), matcherBody.end());
        }

        divideVars();
        writeAssembler.addToVarList(arrayVars);

        divideBody();
    }

    public void divideVars() {

        Pattern patternDefineVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherDefineVariables = patternDefineVariables.matcher(strOfVariables);

        while (matcherDefineVariables.find()) {
            arrayVarsString.add(strOfVariables.substring(matcherDefineVariables.start(), matcherDefineVariables.end()));
        }

        Pattern patternVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherVariables;
        String var;

        for (String string : arrayVarsString) {

            matcherVariables = patternVariables.matcher(string);

            while (matcherVariables.find()) {
                var = string.substring(matcherVariables.start(), matcherVariables.end());
                if (!var.equals("integer")) { arrayVars.add(var); }
            }
        }
    }

    public void divideBody() {

        Pattern patternBody = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherBody = patternBody.matcher(strOfBody);

        while (matcherBody.find()) {
            arrayCodeString.add(strOfBody.substring(matcherBody.start(), matcherBody.end()));
        }

        createBody();
    }

    public void createBody() {

        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        for (String string : arrayCodeString) {

            if (string.matches(patternWrite.toString())) {
                Matcher matcherForCode = patternWrite.matcher(string);

                if (matcherForCode.find()) {
                    writeAssembler.addWrite(matcherForCode.group(1));
                }

            } else if (string.matches(patternReadLn.toString())) {
                Matcher matcherForCode = patternReadLn.matcher(string);

                if (matcherForCode.find()) {
                    writeAssembler.addReadLn(matcherForCode.group(1));
                }

            } else if (string.matches(patternWriteLn.toString())) {
                Matcher matcherForCode = patternWriteLn.matcher(string);

                if (matcherForCode.find()) {
                    writeAssembler.addWriteLn(matcherForCode.group(1));
                }

            } else if (string.matches(patternOperation.toString())) {
                Matcher matcherForCode = patternOperation.matcher(string);

                if (matcherForCode.find()) {
                    writeAssembler.addOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            }
        }
    }
}
