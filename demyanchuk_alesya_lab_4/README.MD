###### ПИбд-21 Алеся Демянчук
# Лабораторная работа №4

###### Как запустить лабораторную работу
1. Открыть папку "demyanchuk_alesya_lab_4" в IntelliJ IDEA как проект
2. Запустить программу

###### Использованные технологии
- Java 11
- IntelliJ IDEA

###### Что делает программа?
При запуске программы случайным образом заполняются 
значения функции в таблице истинности (таблица 1). 
Пользователь указывает конечную форму логической функции 
(ДНФ - 1 или КНФ - 2). После расчетов программа выводит функцию в выбранной форме.
* KNF - выводит конъюктивную нормальную форму вида (a + b) * (-a + b)
* DNF - выводит дизъюнктивную нормальную форму вида (a * b) + (-a * b)
| X1 | X2 | X3 | X4 | F |
| ------------- | ------------- | ------------- | ------------- | ------------- |
| 0  | 0  | 0  | 1  | random {0,1} |
| 0  | 0  | 1  | 0  | random {0,1} |
| ...  | ...  | ...  | ...  | random {0,1} |
| 1  | 1  | 1  | 0  | random {0,1} |
| 1  | 1  | 1  | 1  | random {0,1} |

***Видео с примером работы программы: https://yadi.sk/i/gPHt6M7SvHqaIg***
