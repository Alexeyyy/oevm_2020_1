<!DOCTYPE html>
<html>
<body>
Лабораторная работа №1<br>
выполнил: Филиппов Никита<br>
группа: ПИбд-22<br>
<table bgcolor="#DCDCDC" border="2">
    <caption >
        Моя сборка
    </caption>
    <tr>
        <th>№</th>
        <th>Комплектующие</th>
        <th>Название</th>
        <th>Комментарий</th>
    </tr>
    <tr>
        <td>1.</td>
        <td>Процессор</td>
        <td>Intel Core i5-4670K</td>
        <td>Процессор,который тянет 5-ти летние игры на высоких и новые на низких. Более не надо</td>
    </tr>
    <tr>
        <td>2.</td>
        <td>Материнская плата</td>
        <td>MSI H81M-P33</td>
        <td>Хорошая цена и хорошая производительность. Дешево и сердито. 7 лет - полет нормальный</td>
    </tr>
    <tr>
        <td>3.</td>
        <td>Видеокарта</td>
        <td>GIGABYTE nVidia GeForce GTX 1050TI</td>
        <td>Досталась бесплатно после майнинга</td>
    </tr>
    <tr>
        <td>4.</td>
        <td>Оперативная память</td>
        <td>Kingston 99U5471-038.A00LF x2</td>
        <td>2е dd3 по 8gb </td>
    </tr>
    <tr>
        <td>5.</td>
        <td>Блок питания</td>
        <td>Aerocool VP 750W</td>
        <td>Работает уже 7 лет, ни одной проблемы небыло.</td>
    </tr>
    <tr>
        <td>6.</td>
        <td>Корпус</td>
        <td>SuperPower Winard 5826 черный</td>
        <td>Самый обычный корпус</td>
    </tr>
    <tr>
        <td>7.</td>
        <td>Охлаждение системы</td>
        <td>GlacialStars IceHut 1150 PWM</td>
        <td>Не без труда, но справляется со своей задачей</td>
    </tr>
    <tr>
        <td>8.</td>
        <td>SSD-накопитель</td>
        <td>SiliconPower Velox V55 120gb</td>
        <td>Дешевый SSD для ОС</td>
    </tr>
    <tr>
        <td>9.</td>
        <td>HDD-накопитель</td>
        <td>SEAGATE Barracuda ST1000DM010</td>
        <td>Диск на 1тб,памяти хватает с запасом</td>
    </tr>
</table>
</body>
</html>