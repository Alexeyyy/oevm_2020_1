package com.company;

public class Main {

    public static void main(String[] args) {

        PascalRead pascalRead = new PascalRead();

        AsmWrite assemblerWrite = new AsmWrite();

        ParseStr parseString = new ParseStr(pascalRead.read(), assemblerWrite);
        parseString.parseVarAndBody();

        assemblerWrite.write();
    }

}
