package com.company;

import java.io.*;

public class PascalRead {

    public String read(){

        StringBuilder string = new StringBuilder();

        try(FileReader reader = new FileReader("PascalProgram.pas"))
        {
            int ch;
            while((ch = reader.read()) != -1){
                string.append((char) ch);
            }
        }

        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        return string.toString().replaceAll(";", ";\n");
    }
}

