package com.company;

public class Minimalization {

    private StringBuffer buffer = new StringBuffer();
    private String stringR = "";
    private TruthTable truthT = new TruthTable();
    private int iterations = -1;

    public String getStringR() {
        return stringR;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public int getIterations() {
        return iterations;
    }

    //Дизъюктивная нормальная форма
    public void dNF(int[][] array) {
        if (array[iterations][truthT.getCountOfColumns() - 1] == 1) {
            buffer.append("(");
            for (int j = 0; j < truthT.getCountOfColumns() - 1; j++) {
                if (array[iterations][j] == 0) {
                    if (j == truthT.getCountOfColumns() - 2) {
                        buffer.append("-x").append(j + 1);
                    } else {
                        buffer.append("-x").append(j + 1).append(" * ");
                    }
                }
                if (array[iterations][j] == 1) {
                    if (j == truthT.getCountOfColumns() - 2) {
                        buffer.append("x").append(j + 1);
                    } else {
                        buffer.append("x").append(j + 1).append(" * ");
                    }
                }
            }
            if (iterations == truthT.getCountOfStrings() - 1) {
                buffer.append(" )");
            } else {
                buffer.append(" ) + ");
            }
        }
        stringR = String.valueOf(buffer);
    }

    //Конъюкитивная нормальная форма
    public void сonjunctiveNormalForm(int[][] array) {
        if (array[iterations][truthT.getCountOfColumns() - 1] == 0) {
            buffer.append("(");
            for (int j = 0; j < truthT.getCountOfColumns() - 1; j++) {
                if (array[iterations][j] == 0) {
                    if (j == truthT.getCountOfColumns() - 2) {
                        buffer.append("X").append(j + 1);
                    } else {
                        buffer.append("X").append(j + 1).append(" + ");
                    }
                }
                if (array[iterations][j] == 1) {
                    if (j == truthT.getCountOfColumns() - 2) {
                        buffer.append("-X").append(j + 1);
                    } else {
                        buffer.append("-X").append(j + 1).append(" + ");
                    }
                }
            }
            if (iterations == truthT.getCountOfStrings() - 1) {
                buffer.append(" )");
            } else {
                buffer.append(" ) * ");
            }
        }
        stringR = String.valueOf(buffer);
    }
}
