package My_Func;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JOptionPane;
import java.util.Random;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Program_GUI {
	private JFrame frame;
	public static Random random =new Random();
    private static char arr[]=new char[16];
    private static char[][] table =new char[16][5];
    private static String [] True={"X4","X3","X2","X1"};
    public static StringBuilder num_1 = new StringBuilder();
    public static StringBuilder num_2 = new StringBuilder();
    
	/**
	 * Launch the application.
	 */
    
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Program_GUI window = new Program_GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	
	public Program_GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	private void initialize() {
		frame = new JFrame();
		frame.setBackground(Color.WHITE);
		frame.getContentPane().setBackground(Color.GRAY);
		frame.setBounds(10, 10, 1000, 730);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextPane textPane = new JTextPane();
		textPane.setForeground(Color.black);
        textPane.setFont(new Font("Tahoma", Font.PLAIN, 32));
		textPane.setBounds(10, 50, 300, 630);
		frame.getContentPane().add(textPane);
		
		JTextArea textArea = new JTextArea();
		textArea.setRows(16);
		textArea.setFont(new Font("Monospaced", Font.BOLD, 19));
		textArea.setBounds(550, 10, 420, 450);
		frame.getContentPane().add(textArea);
		
		JButton Table = new JButton("Create new Table");
		Table.setBounds(10, 10, 150, 30);
		frame.getContentPane().add(Table);
		
		JButton DNF = new JButton("Create_DNF");
		DNF.setBounds(200, 10, 150, 30);
		frame.getContentPane().add(DNF);
		
		JButton CNF = new JButton("Create_CNF");
		CNF.setBounds(390, 10, 150, 30);
		frame.getContentPane().add(CNF);
		
		Table.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int n = 4;
                int rows = (int) Math.pow(2,n);
                String OneZero = "01";
                for (int i=0; i<rows; i++) {
                    for (int j=n-1; j>=0; j--) {
                    	table[i][j]= (char) (i / (int) Math.pow(2, j) % 2 + 48);
                    }
                }
                String str ="";
                for(int i = 0;i < 16; i++){
                    str+="|";
                    for(int j = 4; j >= 0;j--){
                    	arr[i]=OneZero.charAt(random.nextInt(OneZero.length()));
                        str+=table[i][j]+"  " ;
                    }
                    str+=" | "+ arr[i]+" |\n";
                }
                textPane.setText(str);
			}
		});
		
		DNF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int counter=0;
                for(int i = 0; i < arr.length; i++){
                    if(arr[i] == '1'){
                        counter=i;
                    }
                }
                for(int i = 0;i < 16; i++){
                    if(arr[i]=='1') {
                    	num_1.append(" (");
                        for (int j = 4; j >= 0; j--) {
                            if (table[i][j] == '0') {
                            	num_1.append("!").append(String.valueOf(True[j]));
                            } else if (table[i][j] == '1') {
                            	num_1.append(String.valueOf(True[j]));
                            }
                            if(j >0 && j<4){ num_1.append("*"); }
                        }
                        num_1.append(")");
                        if(i >= 0&&i < counter)num_1.append("+\n");
                    }
                }
                textArea.setText(" DNF = Disjunctive Normal Form\n F(X1,X2,X3,X4)=\n\n"+num_1+"\n");
                num_1.delete(0,  num_1.length());
                while(num_1.length()==0) {
                    JOptionPane.showMessageDialog(frame, " if you want to reuse please click (Create new Table)");
                    break;
                }
			}
		});
		
		CNF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i = 0;i < 16; i++){
                    if(arr[i]=='0') { num_2.append(" (");
                    
                        for (int j = 4; j >= 0; j--) {
                            if (table[i][j] == '1') {
                            	num_2.append("!").append(String.valueOf(True[j]));
                            } else if (table[i][j] == '0') {
                            	num_2.append(String.valueOf(True[j]));
                            }
                            if(j >0 && j<4){ num_2.append("+"); }
                        }
                        num_2.append(")*\n");
                    }
                }
				textArea.setText(" CNF = Conjunctive Normal Form\n F(X1,X2,X3,X4)=\n\n"+num_2+"\n");
				num_2.delete(0,  num_2.length());
                while(num_2.length()==0) {
                    JOptionPane.showMessageDialog(frame, " if you want to reuse please click (Create new Table)");
                    break;
                }
			}
		});
	}
}
