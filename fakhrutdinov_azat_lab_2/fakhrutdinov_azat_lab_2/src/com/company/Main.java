package com.company;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите исходную с.с.: ");
        int startSystem = input.nextInt();

        System.out.print("Введите конечную с.с.: ");
        int lastSystem = input.nextInt();

        System.out.print("Введите число для перевода: ");
        char[] number = input.next().toCharArray();

        System.out.print("Результат: ");
        String answer = Transfer.convert10NSToEndNS(startSystem, lastSystem, number);
        System.out.print(answer);
    }
}