package com.company;

public class Transfer
{
    public static int convertNSTo10NS(int start, char[] number) {
        int counter = 0;
        for (int index = 0; index < number.length; index++) {
            int tmp;
            if (number[index] >= '0' && number[index] <= '9') {
                tmp = number[index] - '0';
            }
            else {
                tmp = 10 + number[index] - 'A';
            }
            counter += tmp * Math.pow(start, number.length - index - 1);
        }
        return counter;
    }
    public static String convert10NSToEndNS(int start, int end, char[] number) {
        StringBuilder result = new StringBuilder();
        int supForNumberWithoutLetters = 48; // 49 - code 1
        int supForNumberWithLetters = 55; // 65 - code A

        int num = convertNSTo10NS(start, number);

        if (num == 0) {
            result.append("0");
            return result.toString();
        }
        for (int index = 0; num > 0; index++) {
            if (num % end <= 9) {
                result.append((char) (num % end + supForNumberWithoutLetters)) ;
            }
            else {
                result.append((char) (num % end + supForNumberWithLetters)) ;
            }
            num = num / end;
        }
        return result.reverse().toString();
    }
}