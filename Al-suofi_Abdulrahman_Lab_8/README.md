# Лабораторная работа *№8*

##### *студентки гр*. **Пибд-22 Аль-суфи Абдулрахман А.Х.**

#Собираем компьютер:
>https://www.youtube.com/watch?v=wZwj-IP0L78

|**?**|        **Name**                         |    **Photo**    | 
|---- | --------------------------------------- | --------------- |
|**1**| CPU : i9-9900k.                         | ![image](photo/1.jpg)|
|**2**| GPU : GIGABYTE GTX 1080TI.              | ![image](photo/2.jpg)|
|**3**| MOTHERBOARD : ASUS ROG STRIX Z390-E.    | ![image](photo/3.jpg)|
|**4**| RAM : G.SKILL ROYAL TRIDENT Z GOLD 32GB.| ![image](photo/4.jpg)|
|**5**| PSU : CORSAIR RMX1000.                  | ![image](photo/5.jpg)|
|**6**| CASE : lian li o11 dynamic xl.          | ![image](photo/6.jpg)|
|**7**| COOLER : CUSTOM WATER LOOP.             | ![image](photo/7.jpg)|

#компьютер после Собираем.        
![image](photo/PC.jpg)