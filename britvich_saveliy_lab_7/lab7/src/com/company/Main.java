package com.company;

public class Main {

    public static void main(String[] args) {
        Pascal pascal = new Pascal();
        Asm asm = new Asm();

        ParserInAsm Parser = new ParserInAsm(pascal.read(), asm);
        Parser.parseBody();
        asm.create();

    }
}

