public class Main {
    public static void main(String[] args) {
        CodeTranslator translator = new CodeTranslator();
        Reader.readPascalFile("program.pas", translator);
        Writer.createAssemblerFile(translator);
        System.out.println("Код был успешно транслирован в программу для ассемблера FASM!");
    }
}