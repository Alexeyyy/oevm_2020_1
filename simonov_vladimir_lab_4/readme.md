## Лабораторная работа №4
**Выполнил Симонов Владимир**
**ПИбд-22**

Ссылка на видео: https://drive.google.com/file/d/1lZHbdhSQX2i9YZ9AsstxW-zoje7lWG98/view?usp=sharing<br>
<H3> Как запустить?</H3>
Запустить лабу через Main.java
<H3>Какие технологии использовались?</H3>
Использовалась IntelliJ IDEA и стандартные библиотеки Java. Использовал StringBuilder
<H3>Функционал программы</H3>
Выполняет минимизацию булевой функции из 4х переменных. Конечную форму логической функции указывает пользователь

<table bgcolor="#DCDCDC" border="2">
    <caption >
        Пример ДНФ
    </caption>
    <tr>
        <td width="25px">0</td>
		<td width="25px">0</td>
        <td width="25px">0</td>
        <td width="25px">0</td>
		<td width="25px">0</td>
		<td>( -X1 * -X2 * X3 * X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
		<td>+ (-X1 * X2 * -X3 * -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>1</td>
        <td>0</td>
		<td>0</td>
		<td>+ ( -X1 * X2 * X3 * -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
		<td>+ (-X1 * X2 * X3 * -X4 ))</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>0</td>
        <td>0</td>
		<td>0</td>
		<td>+ ( -X1 * X2 * X3 * X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
		<td>+ (X1 * -X2 * X3 * -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>		
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>1</td>
        <td>1</td>
		<td>0</td>		
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>0</td>
        <td>0</td>
		<td>0</td>		
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>0</td>
        <td>1</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>0</td>
        <td>0</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>		
	</tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>1</td>
        <td>1</td>
		<td>0</td>		
    </tr>
</table>

<table bgcolor="#DCDCDC" border="2">
    <caption >
        Пример КНФ
    </caption>
    <tr>
        <td width="25px">0</td>
		<td width="25px">0</td>
        <td width="25px">0</td>
        <td width="25px">0</td>
		<td width="25px">0</td>
		<td>(X1 + X2 + X3 + X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>0</td>
        <td>1</td>
		<td>1</td>
		<td>* (X1 + X2 + X3 + -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>
		<td>* ( X1 + X2 + -X3 + X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>0</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>
		<td>+ ( X1 + -X2 + -X3 + X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>0</td>
        <td>0</td>
		<td>0</td>
		<td>+ (-X1 + X2 + -X3 + X4 )</td>	
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>0</td>
        <td>1</td>
		<td>1</td>	
		<td>+ ( -X1 + -X2 + -X3 + -X4 )</td>
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>1</td>
        <td>0</td>
		<td>1</td>				
    </tr>
	<tr>
        <td>0</td>
		<td>1</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>		
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>0</td>
        <td>0</td>
		<td>1</td>		
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>1</td>
        <td>0</td>
		<td>0</td>
    </tr>
	<tr>
        <td>1</td>
		<td>0</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>		
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>0</td>
        <td>0</td>
		<td>0</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>0</td>
        <td>1</td>
		<td>0</td>
	</tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>1</td>
        <td>0</td>
		<td>0</td>
    </tr>
	<tr>
        <td>1</td>
		<td>1</td>
        <td>1</td>
        <td>1</td>
		<td>1</td>		
    </tr>
</table>



</body>
</html>