package com.company;

import java.util.Random;

public class TruthTable {
    private int countOfStrings = 16;
    private int countOfColumns = 5;
    private Random random = new Random();

    public int getCountOfStrings() {
        return countOfStrings;
    }

    public int getCountOfColumns() {
        return countOfColumns;
    }

    public void printTruthTable() {
        for (int i = 0; i < countOfStrings; i++) {
            for (int j = 0; j < countOfColumns; j++) {
                System.out.print(truthT[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public int[][] truthT = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)},
    };
}
