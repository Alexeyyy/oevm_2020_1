package src;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class Frame {

    private JTextFieldLimit textFieldCC1;
    private JTextFieldLimit textFieldCC2;
    private JTextFieldLimit textFieldDigit;
    private JLabel labelCC1;
    private JLabel labelCC2;
    private JLabel labelDigit;
    private JLabel labelAnswer;

    public void initialize() {
        initTextFields();
        initLabels();
        initFrame();
    }

    private void initFrame() {
        JFrame frame = new JFrame("Калькулятор");
        frame.setSize(400, 320);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

        frame.setLayout(null);
        frame.getContentPane().add(textFieldCC1);
        frame.getContentPane().add(textFieldCC2);
        frame.getContentPane().add(textFieldDigit);
        frame.getContentPane().add(labelCC1);
        frame.getContentPane().add(labelCC2);
        frame.getContentPane().add(labelDigit);
        frame.getContentPane().add(labelAnswer);
    }

    private void initTextFields() {
        textFieldCC1 = new JTextFieldLimit(2, 1);
        textFieldCC2 = new JTextFieldLimit(2, 1);
        textFieldDigit = new JTextFieldLimit(16, 2);

        textFieldCC1.setBounds(200, 30, 160, 25);
        textFieldCC2.setBounds(200, 80, 160, 25);
        textFieldDigit.setBounds(200, 130, 160, 25);

        DocumentListener docListener = new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                setAnswer(textFieldCC1.getText(), textFieldCC2.getText(), textFieldDigit.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                setAnswer(textFieldCC1.getText(), textFieldCC2.getText(), textFieldDigit.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                setAnswer(textFieldCC1.getText(), textFieldCC2.getText(), textFieldDigit.getText());
            }
        };

        textFieldCC1.getDocument().addDocumentListener(docListener);
        textFieldCC2.getDocument().addDocumentListener(docListener);
        textFieldDigit.getDocument().addDocumentListener(docListener);
    }

    private void initLabels() {
        labelCC1 = new JLabel("Введите исходную СС");
        labelCC2 = new JLabel("Введите СС для перевода");
        labelDigit = new JLabel("Введите значение");
        labelAnswer = new JLabel("...");


        labelCC1.setBounds(20, 30, 170, 25);
        labelCC2.setBounds(20, 80, 170, 25);
        labelDigit.setBounds(20, 130, 170, 25);
        labelAnswer.setFont(new Font("Verdana", Font.PLAIN, 24));

        labelAnswer.setBounds(0, 190, 380, 50);
        labelAnswer.setHorizontalAlignment(JLabel.CENTER);
    }

    private void setAnswer(String cc1s, String cc2s, String digit) {
        if (!cc1s.equals("") && !cc2s.equals("") && !digit.equals("")) {
            int cc1 = Integer.parseInt(cc1s);
            int cc2 = Integer.parseInt(cc2s);
            if (cc1 >= 2 && cc1 <= 16 && cc2 >= 2 && cc2 <= 16) {
                Converter converter = new Converter();
                labelAnswer.setText(converter.convert(cc1, cc2, digit));
            } else {
                labelAnswer.setText("Некорректная СС");
            }
        }
    }
}

