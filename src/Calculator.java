public class Calculator {
    Functions functions = new Functions();
    private int startSS;
    private String firstDigit;
    private String secondDigit;
    private Transfer transfer = new Transfer();

    public Calculator(String firstDigit, String secondDigit, int startSS) {
        this.startSS = startSS;
        this.firstDigit = transfer.transferSS(startSS, 2, firstDigit);
        this.secondDigit = transfer.transferSS(startSS, 2, secondDigit);
    }

    public String calculate(char action) {
        StringBuilder str = new StringBuilder();

        switch (action) {
            case '+':
                swap();
                str.append(functions.add(firstDigit, secondDigit));
                return str.reverse().toString();
            case '-':
                str.append(functions.subtract(firstDigit, secondDigit)).reverse();
                return functions.removeZeros(str.toString());
            case '*':
                swap();
                str.append(functions.multiply(firstDigit, secondDigit)).reverse();
                return str.toString();
            case '/':
                str.append(functions.divide(firstDigit, secondDigit)).reverse();
                return str.toString();
            default:
                throw new IllegalArgumentException("You enter wrong action");
        }
    }

    public void swap() {
        if (!functions.compare(firstDigit, secondDigit)) {
            String temp = firstDigit;
            firstDigit = secondDigit;
            secondDigit = temp;
        }
    }
}
