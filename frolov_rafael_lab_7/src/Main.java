public class Main {

    public static void main(String[] args) {
        PascalReader reader = new PascalReader();

        AssemblerWriter writer = new AssemblerWriter();

        StringParser parser = new StringParser(reader.read(), writer);
        parser.parse();

        writer.write();
    }
}

//"C:\\Users\\Рафаэль\\Desktop\\oevm\\oevm_lab_7\\frolov_rafael_lab_7\\Frolov_rafael_pascal_code_lab_7.pas"
//"C:\\Users\\Рафаэль\\Desktop\\oevm\\oevm_lab_7\\frolov_rafael_lab_7\\assemblercode.ASM"