import java.io.FileReader;
import java.io.IOException;

public class PascalReader {
    public String read() {
        StringBuilder string = new StringBuilder();

        try (FileReader reader = new FileReader("C:\\Users\\Рафаэль\\Desktop\\oevm\\oevm_lab_7\\frolov_rafael_lab_7\\Frolov_rafael_pascal_code_lab_7.pas"))
        {
            int ch;
            while ((ch = reader.read()) != -1) {
                string.append((char) ch);
            }
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return string.toString().replaceAll(";", ";\n");
    }
}
