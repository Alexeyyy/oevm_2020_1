import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class CustomPanel extends JPanel {

    private int array[][];
    Methods methods = new Methods();

    public CustomPanel(int array[][]) {
        this.array = array;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        methods.draw(array, g);
    }

}
