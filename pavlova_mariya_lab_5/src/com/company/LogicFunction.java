package com.company;

import java.awt.*;
import java.util.Random;

public class LogicFunction {

    private static int lines;
    private static int countVariables;
    private static int indexFunctionValue;
    private static int[][] truthTable;
    public LogicFunction(int countVariables) {
        this.countVariables = countVariables;
        lines = (int)Math.pow(2, countVariables);
        indexFunctionValue = countVariables;
        truthTable = new int[lines][countVariables+1];
        for (int i = 0; i < lines; i++) {
                truthTable[i] = new int[5];
        }
    }

    public static String getDisjunctiveNormalForm() {
        String result = "Disjunctive Normal Form \nF:";
        boolean isOther;
        for (int i = 0; i < lines; i++) {
            isOther = false;//переменная для проверки, последнее ли это было истинное выражение
            if (truthTable[i][indexFunctionValue] == 1) {
                result += "(";
                for (int j = 0; j < countVariables; j++) {
                    if (j != 0) {
                        result += "*";
                    }
                    if (truthTable[i][j] == 1) {
                        result += "(X" + j + ")";
                    }
                    if (truthTable[i][j] == 0) {
                        result += "(-X"+j+")";
                    }
                }
                result += ")";
                for (int k = i+1; k < lines; k++) {
                    if (truthTable[k][indexFunctionValue] == 1) {
                        isOther = true;
                        break;
                    }
                }
                if (isOther) {
                    result += "+";
                }
                result += "\n";//перевод на след строку для простоты восприятия
            }
        }
        return  result + "\n";
    }

    public static String getConjunctiveNormalForm() {
        String result = "Conjunctive Normal Form \nF:";
        for (int i = 0; i < lines; i++) {
            boolean isOther = false;//переменная для проверки, последнее ли это было ложное выражение
            if (truthTable[i][indexFunctionValue] == 0) {
                result += "(";
                for (int j = 0; j < countVariables; j++) {
                    if (j != 0) {
                        result += "+";
                    }
                    if (truthTable[i][j] == 1) {
                        result += "(-X"+j+")";
                    }
                    if (truthTable[i][j] == 0) {
                        result += "(X"+j+")";
                    }
                }
                result += ")";
                for (int k = i+1; k < lines; k++) {
                    if (truthTable[k][indexFunctionValue] == 0) {
                        isOther = true;
                        break;
                    }
                }
                if (isOther) {
                    result += "*";
                }
                result += "\n";//перевод на след. строку для простоты восприятия
            }
        }
        return result + "\n";
    }

    public static int[][] createTableTruth() {
        Random random = new Random();
        truthTable = new int[][]{
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };
        return truthTable;
    }

    public void draw(Graphics g) {
        int startPosX = 20;
        int startPosY = 20;
        int width = 30;
        int height = 30;
        int biasForDigitX = 13;
        int biasForDigitY = 20;

        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < countVariables+1; j++) {
                g.drawRect(startPosX, startPosY, width, height);
                g.drawString(Integer.toString(truthTable[i][j]), startPosX+biasForDigitX, startPosY+biasForDigitY);
                startPosX += width;
            }
            startPosX = 20;
            startPosY += height;
        }
    }
}
