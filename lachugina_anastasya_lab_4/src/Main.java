import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Methods method = new Methods();
        int lengthI = 16;
        int lengthJ = 5;
        int[][] array = {
                {0, 0, 0, 0, (int) (Math.random() * 2)},
                {0, 0, 0, 1, (int) (Math.random() * 2)},
                {0, 0, 1, 0, (int) (Math.random() * 2)},
                {0, 0, 1, 1, (int) (Math.random() * 2)},
                {0, 1, 0, 0, (int) (Math.random() * 2)},
                {0, 1, 0, 1, (int) (Math.random() * 2)},
                {0, 1, 1, 0, (int) (Math.random() * 2)},
                {0, 1, 1, 1, (int) (Math.random() * 2)},
                {1, 0, 0, 0, (int) (Math.random() * 2)},
                {1, 0, 0, 1, (int) (Math.random() * 2)},
                {1, 0, 1, 0, (int) (Math.random() * 2)},
                {1, 0, 1, 1, (int) (Math.random() * 2)},
                {1, 1, 0, 0, (int) (Math.random() * 2)},
                {1, 1, 0, 1, (int) (Math.random() * 2)},
                {1, 1, 1, 0, (int) (Math.random() * 2)},
                {1, 1, 1, 1, (int) (Math.random() * 2)},
        };
        method.print(array, lengthI, lengthJ);

        System.out.println("Чтобы вывести ДНФ, нажмите 1. Чтобы вывести КНФ, нажмите 0");
        int number = scanner.nextInt();
        switch (number) {
            case 1:
                System.out.print(method.disNF(array, lengthI, lengthJ));
                break;
            case 0:
                System.out.print(method.conNF(array, lengthI, lengthJ));
                break;
            default:
                System.out.print("Ошибка. Введите другое число");
                break;
        }
    }
}
