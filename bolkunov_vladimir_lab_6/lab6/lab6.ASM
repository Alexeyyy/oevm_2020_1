 format PE Console

entry main

include 'win32a.inc'

section '.idata' import data readable
library kernel, 'kernel32.dll',msvcrt, 'msvcrt.dll'
import kernel, ExitProcess
import msvcrt, printf, 'printf', scanf, 'scanf'


section 'main' code readable writable executable
inputFromat db '%d',0

inputX db 'X = ',0
X dd ?

inputY db 'Y = ',0
Y dd ?

outputSum db 'X + Y = %d', 0ah, 0
outputSub db 'X - Y = %d', 0ah, 0
outputMult db 'X * Y = %d', 0ah, 0
outputDiv db 'X / Y = %d', 0

main:
        push inputX
        call [printf]

        push X
        push inputFromat
        call [scanf]

        push inputY
        call [printf]

        push Y
        push inputFromat
        call [scanf]

        mov eax, [X]
        add eax, [Y]
        push eax
        push outputSum
        call [printf]

        mov eax, [X]
        sub eax, [Y]
        push eax
        push outputSub
        call [printf]

        mov eax, [X]
        imul eax, [Y]
        push eax
        push outputMult
        call [printf]

        mov eax, [X]
        cdq
        mov ebx, [Y]
        div ebx
        push eax
        push outputDiv
        call [printf]

        push ecx
        push inputFromat
        call [scanf]
        int 20h


