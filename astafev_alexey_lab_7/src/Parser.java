import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    Assembler assemblerCode;
    private String pascalCode;
    private String blockOfVars;
    private String blockOfCods;
    private ArrayList<String> arrayVarsString = new ArrayList<>();
    private ArrayList<String> arrayCode = new ArrayList<>();
    private ArrayList<String> arrayVars = new ArrayList<>();

    public Parser(String pascalCode, Assembler assemblerCode) {
        this.pascalCode = pascalCode;
        this.assemblerCode = assemblerCode;
    }

    public void parseVar(){
        Pattern patternOfVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherOfVariables = patternOfVariables.matcher(blockOfVars);

        while (matcherOfVariables.find()) {
            arrayVarsString.add(blockOfVars.substring(matcherOfVariables.start(), matcherOfVariables.end()));
        }
        parseVarsInteger();
    }

    public void parseVarsInteger() {
        Pattern patternOfVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherOfVariables;
        String var;

        for (String string : arrayVarsString) {

            matcherOfVariables = patternOfVariables.matcher(string);

            while (matcherOfVariables.find()) {
                var = string.substring(matcherOfVariables.start(), matcherOfVariables.end());
                if (!var.equals("integer")) { arrayVars.add(var); }
            }
        }
        assemblerCode.addToVars(arrayVars);
    }

    public void parseProgram() {
        Pattern patternOfVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherOfVariables = patternOfVariables.matcher(pascalCode);

        while (matcherOfVariables.find()) {
            blockOfVars = pascalCode.substring(matcherOfVariables.start(), matcherOfVariables.end());
        }

        Pattern patternOfCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherOfCode = patternOfCode.matcher(pascalCode);

        while (matcherOfCode.find()) {
            blockOfCods = pascalCode.substring(matcherOfCode.start(), matcherOfCode.end());
        }
        parseVar();
        parseCode();
    }

    public void parseCode(){
        Pattern patternOfCods = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherOfCods = patternOfCods.matcher(blockOfCods);
        while (matcherOfCods.find()) {
            arrayCode.add(blockOfCods.substring(matcherOfCods.start(), matcherOfCods.end()));
        }
        createCode();
    }

    public void createCode(){
        Pattern patternForWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternForRead = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternForOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        for (String string : arrayCode) {

            if(string.matches(patternForWrite.toString())){
                Matcher matcherForCode = patternForWrite.matcher(string);

                if (matcherForCode.find()) {
                    assemblerCode.addToCodeWrite(matcherForCode.group(1));
                }
            } else if(string.matches(patternForRead.toString())){
                Matcher matcherForCode = patternForRead.matcher(string);

                if (matcherForCode.find()) {
                    assemblerCode.addToCodeRead(matcherForCode.group(1));
                }
            } else if(string.matches(patternForOperation.toString())){
                Matcher matcherForCode = patternForOperation.matcher(string);

                if (matcherForCode.find()) {
                    assemblerCode.addToCodeOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            }
        }
    }
}
