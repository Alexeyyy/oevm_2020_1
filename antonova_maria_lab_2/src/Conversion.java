import java.util.Stack;

public class Conversion
{
    private int _origin_ss;
    private int _final_ss;
    private char[] _num_ch;

    private final int helpSymbol = 55;

    public Conversion (int origin_ss, int final_ss, char [] num_ch)
    {
        _origin_ss = origin_ss;
        _final_ss = final_ss;
        _num_ch = num_ch;
    }

    private int [] convertNumber ()
    {
        int [] num_int = new int[_num_ch.length];
        int a = _num_ch.length - 1;

        for (int i = 0; i < _num_ch.length; i++) {
            int buf = (int) _num_ch[i] - (int) '0';

            if (buf >= 0 && buf <= 9)
                num_int[a] = buf;
            else
                num_int[a] = buf + (int) '0' - helpSymbol;

            if (num_int[a] >= _origin_ss) {
                System.out.println("Ошибка");
                System.exit(0);
            }
            a--;
        }
        return num_int;
    }

    private int convertFromOriginToDecimal ( int [] num_int)
    {
        int res = 0;
        for (int i = num_int.length - 1; i >= 0; i--)
            res += num_int[i] * Math.pow(_origin_ss, i);
        return res;
    }

    public String formResultString ()
    {
        int decimal = convertFromOriginToDecimal(convertNumber());
        Stack<Integer> mod = new Stack<Integer>();
        do {
            int cur_mod = 0;
            cur_mod = decimal % _final_ss;
            decimal /= _final_ss;
            mod.push(cur_mod);
        } while (decimal >= _final_ss);
        mod.push(decimal);

        String result = "";
        int buff;
        do {
            buff = (int)mod.pop();
            if (buff >= 0 && buff <= 9)
                result += buff;
            else
                result += (char) (buff + helpSymbol);
        } while(!mod.empty());
        return result;
    }
}
