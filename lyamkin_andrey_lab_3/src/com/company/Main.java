package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        do {

            System.out.println("Введите исходную систему счисления (2-16)");
            int ns = scanner.nextInt();
            if (ns < 2 ||  ns > 16) {
                System.out.println("Основание системы счисления должно находится в пределах от 2 до 16!");
                continue;
            }

            System.out.println("Введите два целых числа");
            String val1 = scanner.next();
            String val2 = scanner.next();


            System.out.println("Выберите операцию:\nСложение (+)\nВычитание (-)\nУмножение (*)\nДеление (/)");
            String operation = scanner.next();


            System.out.println(NumberSystemConverter.convert(BinaryCalculator.calculate(val1, val2, ns, operation), 2, 10));
        }while(true);
    }
}
