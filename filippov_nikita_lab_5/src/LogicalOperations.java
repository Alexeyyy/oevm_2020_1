public enum LogicalOperations {
    DNF,
    KNF,
    Empty;

    public static LogicalOperations getOperations(String choice) {
        switch (choice) {
            case "ДНФ": {
                return LogicalOperations.DNF;
            }
            case "КНФ": {
                return LogicalOperations.KNF;
            }
            case "": {
                return LogicalOperations.Empty;
            }
        }
        return null;
    }
}

