package com.company;


import java.io.FileInputStream;
import java.io.IOException;

public class PascalGetter {

    private FileInputStream _fPascal = null;

    public PascalGetter(FileInputStream fPascal){
        _fPascal = fPascal;
    }

    public StringBuilder getStrsFromPascalDoc (){
        StringBuilder str = new StringBuilder();
        try {
            int ch = -1;
            while ((ch = _fPascal.read()) != -1) {
                str.append((char) ch);
            }
            _fPascal.close();
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return str;
    }
}
