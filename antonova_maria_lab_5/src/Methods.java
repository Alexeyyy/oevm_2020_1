package com.company;
import javax.swing.*;
import java.awt.*;

public class Methods {

    private int sizeI = 16;
    private int sizeJ = 5;

    public void draw(int func, int step, int array[][], Graphics g, TruthTable table) {

        int X = 10;
        int Y = 0;
        int width = 20;
        int height = 20;
        int otstup = 5;

        for(int i = 0; i < sizeJ-1; i++){
            g.drawRect( X + width * i , Y , width, height);
            g.drawString("x"+(i+1), X+ otstup + width * i, Y+otstup*3);
        }

        g.drawRect( X + width * (sizeJ-1) , Y , width, height);
        g.drawString("f", X + otstup + width * (sizeJ-1), Y+otstup*3);

        Y+=20;

        for(int i = 0; i < sizeI; i++){
            for(int j = 0; j < sizeJ; j++){
                g.drawRect( X + width * j , Y + height * i, width, height);
                g.drawString(array[i][j]+"", X + otstup + width * j, Y+otstup*3 + height *i);
            }
        }

        if (step>-1 && step < sizeI) {
            g.setColor(Color.red);
            if ((func + array[step][sizeJ-1]) == 2 || (func + array[step][sizeJ-1]) == 0) {
                g.setColor(Color.green);
            }
            g.fillRect(X + width * (sizeJ), Y + height * step, 5, height);
            g.setColor(Color.black);
            g.drawRect(X + width * (sizeJ), Y + height * step, 5, height);
        }
    }
}
