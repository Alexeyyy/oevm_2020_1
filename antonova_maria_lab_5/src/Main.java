package com.company;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Main {

    private JFrame frame;
    private JButton btnNextStep = new JButton("След.строка");
    private JButton btnDNF = new JButton("ДНФ");
    private JButton btnСNF = new JButton("КНФ");
    static JTextArea area1;

    DNF dnf = new DNF(table);
    CNF cnf = new CNF (table);
    String [] strs;

    private static int tWidth = 5;
    private static int tLength = 16;
    private static TruthTable table = new TruthTable(tLength, tWidth);

    public Main() {
        initialize();
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        table.fillTable();
    }

    static int step = -1;
    static int func = 0;

    private void initialize() {

        frame = new JFrame();
        frame.setBounds(100, 100, 300, 450);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel MyPanel = new MyPanel(table);
        MyPanel.setBounds(0, 40, 370, 450);
        MyPanel.setBackground(Color.white);
        frame.getContentPane().add(MyPanel);

        area1 = new JTextArea("");
        area1.setEditable(false);
        area1.setFont(new Font("Dialog", Font.PLAIN, 14));
        MyPanel.add(area1);

        btnNextStep.setEnabled(false);
        btnNextStep.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                step++;
                if (step > tLength-2)
                {
                    btnNextStep.setEnabled(false);
                }
                MyPanel.repaint();
            }
        });
        btnNextStep.setBounds(10, 10, 110, 20);
        btnNextStep.setBackground(Color.pink);
        frame.getContentPane().add(btnNextStep);

        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnNextStep.setEnabled(true);
                btnСNF.setEnabled(false);
                btnDNF.setEnabled(false);
                step = 0;
                func = 1;
                strs = dnf.formResult();
                area1.append("ДНФ:\n");
                for (int i = 0; i < tLength; i++){
                    area1.append(strs[i]);
                    area1.append("\n");
                }
                MyPanel.repaint();
            }
        });
        btnDNF.setBounds(130, 10, 70, 20);
        frame.getContentPane().add(btnDNF);

        btnСNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnNextStep.setEnabled(true);
                btnСNF.setEnabled(false);
                btnDNF.setEnabled(false);
                step = 0;
                func = 0;
                strs = cnf.formResult();
                area1.append("КНФ:\n");
                for (int i = 0; i < tLength; i++){
                    area1.append(strs[i]);
                    area1.append("\n");
                }
                MyPanel.repaint();
            }
        });
        btnСNF.setBounds(210, 10, 70, 20);
        frame.getContentPane().add(btnСNF);
    }
}
