package com.company;

public class TruthTable {
    int _tWidth;
    int _tLength;
    int [][] _truthTable;

    public TruthTable(int length, int width){
        _tWidth = width;
        _tLength = length;
        _truthTable = new int [_tLength][_tWidth];
    }

    public void fillTable (){
        fillVariables();
        fillFunction();
    }

    private void fillVariables (){
        for (int i = 0; i <_tLength; i++) {
            int decimal = i;
            int j = _tWidth - 2;
            do {
                int cur_mod = 0;
                cur_mod = decimal % 2;
                decimal /= 2;
                _truthTable[i][j] = cur_mod;
                j--;
            } while (decimal >= 2);
            _truthTable[i][j] = decimal;
        }
    }

    private void fillFunction ()
    {
        for (int i = 0; i <_tLength; i++) {
            _truthTable[i][_tWidth -1] = (int)(Math.random()*2);
        }
    }
}
