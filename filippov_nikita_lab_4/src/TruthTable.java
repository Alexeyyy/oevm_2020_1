import java.util.Random;

public class TruthTable {
    private int[][] truthTable;
    private Random random;

    public TruthTable() {
        random = new Random();
        this.truthTable = new int[][]{
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };
    }

    public int[][] getTruthTable() {
        return truthTable;
    }

    public String toString() {
        String output = "";
        for (int i = 0; i < truthTable.length; i++) {
            for (int j = 0; j < truthTable[i].length; j++) {
                output += truthTable[i][j] + " ";
            }
            output += "\n";
        }
        return output;
    }
}
