package sample;

public class MinimizingBooleanFunctions {
    private StringBuffer stringBuffer;
    private TruthTable truthTable;

    public void convertToDisjunctiveNormalForm(int[][] array) {
        truthTable = new TruthTable();
        stringBuffer = new StringBuffer();
        stringBuffer.append("(");
        int str = truthTable.getCountOfStrings();
        int col = truthTable.getCountOfColumns();

        for (int i = 0; i < str; i++) {
            if (array[i][col - 1] == 1) {
                for (int j = 0; j < col - 1; j++) {
                    if (array[i][j] == 0) {
                        if (j == col - 2) {
                            stringBuffer.append("-X").append(j + 1);
                        } else {
                            stringBuffer.append("-X").append(j + 1).append(" * ");
                        }
                    }
                    if (array[i][j] == 1) {
                        if (j == col - 2) {
                            stringBuffer.append("X").append(j + 1);
                        } else {
                            stringBuffer.append("X").append(j + 1).append(" * ");
                        }
                    }
                }
                if (i == str - 1) {
                    stringBuffer.append(" )");
                } else {
                    stringBuffer.append(" ) + ").append(" (");
                }
            }
        }
        System.out.println(stringBuffer);
    }

    public void convertToConjuctiveNormalForm(int[][] array) {
        truthTable = new TruthTable();
        stringBuffer = new StringBuffer();
        stringBuffer.append("(");
        int str = truthTable.getCountOfStrings();
        int col = truthTable.getCountOfColumns();

        for (int i = 0; i < str; i++) {
            if (array[i][col - 1] == 0) {
                for (int j = 0; j < col - 1; j++) {
                    if (array[i][j] == 0) {
                        if (j == col - 2) {
                            stringBuffer.append("X").append(j + 1);
                        } else {
                            stringBuffer.append("X").append(j + 1).append(" + ");
                        }
                    }
                    if (array[i][j] == 1) {
                        if (j == col - 2) {
                            stringBuffer.append("-X").append(j + 1);
                        } else {
                            stringBuffer.append("-X").append(j + 1).append(" + ");
                        }
                    }
                }
                if (i == str - 1) {
                    stringBuffer.append(" )");
                } else {
                    stringBuffer.append(" ) * ").append(" ( ");
                }
            }
        }
        System.out.println(stringBuffer);
    }
}
