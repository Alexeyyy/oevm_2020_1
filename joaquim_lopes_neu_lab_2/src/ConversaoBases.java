
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ной
 */
public class ConversaoBases {
    
     private static String converter(String n, int bi, int bc) {
        int j, i, dig, decimal;
        String numc, num;
        numc = num = "";
        //inicialização das variáveis
        dig = decimal = 0;
        j = n.length() - 1;

        if (bi < 2 || bi > 16 || bc < 2 || bc > 16) {
            return " ";
        }
        //converter para decimal
        for (i = 0; i < n.length(); i++, j--) {
            dig = Integer.parseInt(n.substring(i, (i + 1)), 16);
            if (dig > bi) {
                return " ";
            } else {
                decimal += dig * Math.pow(bi, j);
            }
        }

        //converter para a base pretendida
        while (decimal >= bc) {
            num += caracteres(decimal % bc);
            decimal /= bc;
        }
        num += caracteres(decimal % bc);

        //inverter a String
        for (i = num.length() - 1; i >= 0; i--) {
            numc += num.charAt(i);
        }
        return numc;
    }

    private static String caracteres(int d) {
        switch (d) {
            case 10:
                return "A";
            case 11:
                return "B";
            case 12:
                return "C";
            case 13:
                return "D";
            case 14:
                return "E";
            case 15:
                return "F";
            default:
                return String.valueOf(d);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

         String num;
        int baseI, baseC;
        Scanner ler = new Scanner(System.in);

        System.out.println("<Преобразование между базами>");
        System.out.print("\nВведите число для преобразования: ");
        num = ler.nextLine();
        while (!num.equals(" ")) {
            System.out.print("Укажите базу (2-16):  ");
            baseI = ler.nextInt();
            System.out.print("Укажите базу для конвертации (2-16): ");
            baseC = ler.nextInt();
            ler.nextLine();
            if (converter(num, baseI, baseC).equals(" ")) {
                System.out.println("Указанные базы неверны!");
            } else {
                System.out.println("Результат: " + converter(num, baseI, baseC));
            }
            System.out.print("\nВведите число для преобразования: ");
            num = ler.nextLine();
        }
    }
}
