package com.company;

public class PascalToFASM {

    private FASMTranslator translator;
    private FASMPattern fasmPattern;
    private String[] pascalText;

    private StringBuilder code;
    private StringBuilder data;

    boolean argumentIsUndefine = false;

    public PascalToFASM() {
        translator = new FASMTranslator();
        fasmPattern = new FASMPattern();
    }

    public String parse(String _pascalText) {

        code = new StringBuilder();
        data = new StringBuilder();

        pascalText = _pascalText.split("\n");

        int lineNumber = 0;
        int tempStrNumber = 0;

        if(!pascalText[0].contains("var"))
            return null;

        // Добавляем переменные.
        for (int i = 1; i < pascalText.length; i++) {

            if (pascalText[i].contains("begin")){
                lineNumber = i + 1;
                break;
            }

            if(pascalText[i].isEmpty())
                continue;

            String[] varLine = pascalText[i].split(":");
            if(!varLine[1].trim().contains("integer"))
                return "Данная версия программы поддерживает только переменные типа integer.";

            varLine = varLine[0].split(",");
            for (int j = 0; j < varLine.length; j++) {
                data.append(translator.getNotInitVar(varLine[j].trim()));
            }
        }

        // Парсим код
        for (int i = lineNumber; i < pascalText.length; i++) {

            if(pascalText[i].contains("writeln")){
                String arg = getArgument(pascalText[i]);
                if(argumentIsUndefine){
                    data.append(translator.getInitVar( "__tempStr"+ tempStrNumber, arg));
                    code.append(translator.getWriteLn("__tempStr"+tempStrNumber));
                    tempStrNumber++;
                }else{
                    code.append(translator.getWriteLn("["+arg+"]"));
                }
            } else if(pascalText[i].contains("write")){
                String arg = getArgument(pascalText[i]);
                if(argumentIsUndefine){
                    data.append(translator.getInitVar( "__tempStr"+ tempStrNumber, arg));
                    code.append(translator.getWrite("__tempStr"+tempStrNumber));
                    tempStrNumber++;
                }else{
                    code.append(translator.getWrite("["+arg+"]"));
                }
            }
            else if(pascalText[i].contains("readln")){
                code.append(translator.getReadLn(getArgument(pascalText[i])));
            }
            else if(pascalText[i].contains("+")){
                String[] operands = getOperands(pascalText[i]);
               code.append(translator.getOperation(operands[0], operands[1], operands[2], "+"));
            }else if(pascalText[i].contains("-")){
                String[] operands = getOperands(pascalText[i]);
                code.append(translator.getOperation(operands[0], operands[1], operands[2], "-"));
            }else if(pascalText[i].contains("*")){
                String[] operands = getOperands(pascalText[i]);
                code.append(translator.getOperation(operands[0], operands[1], operands[2], "*"));
            }else if(pascalText[i].contains("/")){
                String[] operands = getOperands(pascalText[i]);
                code.append(translator.getOperation(operands[0], operands[1], operands[2], "/"));
            }
        }
        fasmPattern.addInCodeSection(code.toString());
        fasmPattern.addInDataSection(data.toString());
        return fasmPattern.getFASMPatternStr();
    }
    private String[] getOperands(String expression){
        // Этот метод вернет массив, где первым элементом стоит название переменной результата, вторым - 1 оперенд, третим - 2й.
        int count = 0;
        String[] res = new String[3];
        String[] tempStrArr = expression.split(":=");
        res[count] = tempStrArr[0].trim();

        StringBuilder sb = new StringBuilder();
        char[] tempCharArr = tempStrArr[1].trim().toCharArray();
        for (int i = 0; i < tempCharArr.length; i++) {
            if(tempCharArr[i] == '+' || tempCharArr[i] == '-' ||tempCharArr[i] == '*' ||tempCharArr[i] == '/' ||tempCharArr[i] == ';'){
                count++;
                res[count] = sb.toString();
                sb = new StringBuilder();

                if(tempCharArr[i] != ';')
                     i++;
            }
            if(tempCharArr[i]!=' ')
                 sb.append(tempCharArr[i]);
        }
        return res;
    }

    private String getArgument(String strWithArg){
        StringBuilder sb = new StringBuilder();
        char[] arr = strWithArg.toCharArray();
        int i;
        for (i = 0; i < arr.length; i++) {
            if(arr[i]== 8216) { // Код символа '
                argumentIsUndefine = true;
                break;
            }
        }
        i++;
        for (; i < arr.length; i++) {
            if(arr[i] == 8217){

                return sb.toString();
            }
            sb.append(arr[i]);
        }

        for (i = 0; i < arr.length; i++) {
            if(arr[i]=='('){
                argumentIsUndefine = false;
                break;
            }

        }
        i++;
        for (; i < arr.length; i++) {
            if(arr[i] == ')') {
                return sb.toString();
            }
            sb.append(arr[i]);
        }

       return null;
    }
}
