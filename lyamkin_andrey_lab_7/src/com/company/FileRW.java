package com.company;

import java.io.*;

public class FileRW {

    public String read(String path){
        StringBuilder sb = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String current;
            while ((current = br.readLine())!=null){
                sb.append(current);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString().replaceAll(";", ";\n");
    }

    public void write(String data, String path){
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(path)))
        {
            bw.write(data);
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
}
