package com.company;

public class Main {

    public static void main(String[] args) {

        FileRW fileRW = new FileRW();
        PascalToFASM pascalToFASM = new PascalToFASM();

        String p = fileRW.read("pascal.pas");
        System.out.println(p);
        fileRW.write(pascalToFASM.parse(p), "fasm.asm");

    }
}
