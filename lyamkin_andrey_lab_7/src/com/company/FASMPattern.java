package com.company;

public class FASMPattern {

    // Это шаблон программы на Ассемблере.

    String FASMPatternStr;

    public FASMPattern(){
        FASMPatternStr = "format PE console\n" +
                "\n" +
                "entry start\n" +
                "\n" +
                "include 'win32a.inc'\n" +
                "\n" +
                "section '.data' data readable writable\n" +
                "\n" +
                "spaceStr db ' %d', 0\n" +
                "emptyStr db '%d', 0\n"+
                "writeLnStr db '%d', 0ah, 0   \n"+
                "NULL = 0\n"+

                "\n" +
                ";%.data% \n" +   // Это то место, ктоторое будет менятьс.
                "\n" +

                "section '.code' code readable executable\n" +
                "\n" +
                "         start:\n" +
                "         ;%.code% \n" +      // Это то место, ктоторое будет менятьс.
                "         finish:\n" +
                "\n" +
                "                call [getch]\n" +
                "                push NULL\n" +
                "                call [ExitProcess]\n" +
                "\n" +
                "section '.idata' import data readable\n" +
                "\n" +
                "        library kernel, 'kernel32.dll',\\\n" +
                "                msvcrt, 'msvcrt.dll'\n" +
                "\n" +
                "        import kernel,\\\n" +
                "               ExitProcess, 'ExitProcess'\n" +
                "\n" +
                "        import msvcrt,\\\n" +
                "               printf, 'printf',\\\n" +
                "               scanf, 'scanf',\\\n" +
                "               getch, '_getch'";
    }

    public void addInCodeSection(String data){
        FASMPatternStr = FASMPatternStr.replace(";%.code%", data + "\n ;%.code% \n ");
    }
    public void addInDataSection(String data){
        FASMPatternStr = FASMPatternStr.replace(";%.data%", data + "\n ;%.data% \n ");
    }

    public String getFASMPatternStr() {
        return FASMPatternStr;
    }
}
