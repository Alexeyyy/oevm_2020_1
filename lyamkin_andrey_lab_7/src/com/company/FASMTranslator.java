package com.company;

public class FASMTranslator {

    public String getNotInitVar(String varName) {
        return "        " + varName + " dd ?\n";
    }

    public String getInitVar(String varName, String value) {
        return "        " + varName + " db '" + value + "', 0\n";
    }

    // Важно - чтобы что-то вывести, это что-то должно быть объявленно в блоке .data
    public String getWrite(String argumentVarName) {
        return  "                push " + argumentVarName +  "\n" +
                "                call [printf]\n";
    }
    public String getWriteLn(String argumentVarName) {
        return
                "                push " + argumentVarName + "\n" +
                        "                push writeLnStr\n" +
                        "                call [printf]\n\n";
    }

    public String getReadLn(String argument) {

            return "                push " + argument + "\n" +
                    "                push spaceStr\n" +
                    "                call [scanf]\n\n";

    }
    public String getOperation(String res, String value1, String value2, String operator) {

        switch (operator) {
            case "+":
                return  "                mov eax, [" + value1 + "]\n" +
                        "                add eax, [" + value2 + "]\n" +
                        "                mov [" + res + "], eax\n\n";

            case "-":
                return  "                mov eax, [" + value1 + "]\n" +
                        "                sub eax, [" + value2 + "]\n" +
                        "                mov [" + res + "], eax\n\n";

            case "*":
                return  "                mov eax, [" + value1 + "]\n" +
                        "                imul eax, [" + value2 + "]\n" +
                        "                mov [" + res + "], eax\n\n";

            case "/":
                return  "                mov eax, [" + value1 + "]\n" +
                        "                mov ecx, [" + value2 + "]\n" +
                        "                div ecx\n" +
                        "                mov [" + res + "], eax\n\n";
        }
        return null;
    }

}
