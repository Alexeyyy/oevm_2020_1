format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable

spaceStr db ' %d', 0
emptyStr db '%d', 0
writeLnStr db '%d', 0ah, 0   
NULL = 0

        x dd ?
        y dd ?
        res1 dd ?
        res2 dd ?
        res3 dd ?
        res4 dd ?
        __tempStr0 db 'input x: ', 0
        __tempStr1 db 'input y: ', 0
        __tempStr2 db 'x + y = ', 0
        __tempStr3 db 'x - y = ', 0
        __tempStr4 db 'x * y = ', 0
        __tempStr5 db 'x / y = ', 0

 ;%.data% 
  

section '.code' code readable executable

         start:
                         push __tempStr0
                call [printf]
                push x
                push spaceStr
                call [scanf]

                push __tempStr1
                call [printf]
                push y
                push spaceStr
                call [scanf]

                mov eax, [x]
                add eax, [y]
                mov [res1], eax

                push __tempStr2
                call [printf]
                push [res1]
                push writeLnStr
                call [printf]

                mov eax, [x]
                sub eax, [y]
                mov [res2], eax

                push __tempStr3
                call [printf]
                push [res2]
                push writeLnStr
                call [printf]

                mov eax, [x]
                imul eax, [y]
                mov [res3], eax

                push __tempStr4
                call [printf]
                push [res3]
                push writeLnStr
                call [printf]

                mov eax, [x]
                mov ecx, [y]
                div ecx
                mov [res4], eax

                push __tempStr5
                call [printf]
                push [res4]
                push writeLnStr
                call [printf]


 ;%.code% 
  
         finish:

                call [getch]
                push NULL
                call [ExitProcess]

section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch, '_getch'