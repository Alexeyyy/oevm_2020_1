# Лабораторная работа №6 "Основы программирования на языке ассемблера" 

* Использовала Flat assembler для создания консольного приложения, которое складывает, вычитает, умножает и делит два введенных числа
* Запускается через proga.EXE файл 
* Тесты:

![Картинка](https://media.discordapp.net/attachments/691905536722993196/774349275986788412/unknown.png)  

[Видео](https://drive.google.com/file/d/12Jj-rBryRRoIRjQ8eVXv3cwMD8t1JMi8/view?usp=sharing)
