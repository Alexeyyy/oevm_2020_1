package com.company;

public class Main {

    public static void main(String[] args) {
        Interpreter interpreter = new Interpreter();
        Reader.readPascal("Pascal.pas", interpreter);
        Writer.writeAssembler(interpreter);
    }
}
