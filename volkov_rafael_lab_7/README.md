#Лабораторная работа №7 Изучение методов трансляции

##ПИбд-22 Волков Рафаэль

ТЗ:
Разработать транслятор программ Pascal-FASM. Предусмотреть
проверку синтаксиса и семантики исходной программы. Результатом
работы транслятора является программа для ассемблера FASM,
идентичная по функционалу исходной программе.

Результат программы:

_Исходный код Pascal_
![Пример1](https://sun9-52.userapi.com/impf/E-GCs0GmpvUQuBHLG-XTaGoyz0Qy_ShA8YfluA/N23mMn1EpGQ.jpg?size=567x297&quality=96&proxy=1&sign=343efdbefb369127b72ced42e1d975c4&type=album)


_Итоговый код Assembler_
![Пример2](https://sun9-3.userapi.com/impf/8wxA5WnwHL02bWpuQI6RacaVfS74-rdHcUb4ow/-ycZHdUNPrw.jpg?size=510x833&quality=96&proxy=1&sign=8a5eaf34ef326bb0f6e8addb1a45e439&type=album)

[Пример видео](https://drive.google.com/file/d/1BQNut9Za-in5TMuL7O36MIBNx90R4vzX/view?usp=sharing)