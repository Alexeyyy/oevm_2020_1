package bolkunov_vladimir_lab7;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите название входного файла:");
            String inputFilePath = scanner.nextLine();
            System.out.println("Введите название выходного файла:");
            String outputFilePath = scanner.nextLine();
            Translator translator = new Translator(inputFilePath, outputFilePath);
            translator.translate();
            System.out.println("Готово!");
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
