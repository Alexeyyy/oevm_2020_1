package bolkunov_vladimir_lab7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProgramModel {
    public Map<String, String> varMap;
    public ArrayList<String> commandsMap;

    public ProgramModel(String[] input){
        varMap = new HashMap<String, String>();
        commandsMap = new ArrayList<String>();
        splitCommands(input);
    }

    public boolean paranthesisCheck(String[] input){
        int beginCount = 0;
        int endCount = 0;
        int roundCount = 0;
        boolean quoteOpen = false;

        for (String str : input) {
            str = str.toLowerCase();
            if(str.contains("begin")){
                beginCount++;
            }
            if(str.contains("end")){
                endCount++;
            }

            for(char c : str.toCharArray()){
                if(c == '(') {
                    roundCount++;
                }else if(c == ')'){
                    roundCount--;
                }else if(c == '\''){
                    quoteOpen = !quoteOpen;
                }
            }
        }

        if(beginCount == endCount && roundCount == 0 && !quoteOpen) {
            return true;
        }else{
            return false;
        }
    }

    private void splitCommands(String[] input) {
        boolean var = false;
        boolean mainBeginFound = false;
        boolean mainEndFound = false;
        for (String str : input) {
            str = str.toLowerCase();
            if(str.strip().contains("var")) {
                var = true;
                continue;
            }
            if(!mainBeginFound && str.strip().contains("begin")){
                mainBeginFound = true;
                continue;
            }
            if(!mainEndFound && str.strip().contains("end.")){
                mainEndFound = true;
                continue;
            }

            for(String token : str.split(";")){
                if(var && !mainBeginFound && !mainEndFound){
                    String[] curVar = token.split(":");
                    for(String name : curVar[0].split(",")){
                        varMap.put(name.strip(), curVar[1].strip());
                    }
                }

                if(mainBeginFound && !mainEndFound){
                    commandsMap.add(token.strip());
                }
            }
        }
    }
}
