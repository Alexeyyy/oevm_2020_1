package bolkunov_vladimir_lab7;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ConverterFASM {
    private ProgramModel model;

    public ConverterFASM(ProgramModel programModel){
        model = programModel;
    }

    private static String[] getHeader() {
        return new String[]{
                "format PE console",
                "entry start",
                "include 'win32a.inc'",
                "section '.idata' import data readable",
                "\t library kernel, 'kernel32.dll', msvcrt, 'msvcrt.dll'",
                "\t import kernel, ExitProcess, 'ExitProcess'",
                "\t import msvcrt, printf, 'printf', scanf, 'scanf',getch, '_getch'",
                "section '.data' data readable writable",
                "\t spaceStr db '%d', 0",
                "\t dopStr db '%d', 0ah, 0"
        };
    }

    private static String[] getCodeHeader() {
        return new String[]{
                "section '.code' code readable executable",
                "\t start:"
        };
    }

    private static String[] getCodeFooter() {
        return new String[]{
                "call [getch]",
                "push NULL",
                "call [ExitProcess]"
        };
    }

    private HashMap<String, String> stringDefenitions = new HashMap<String, String>();
    private int stringID = -1;

    private String convertCommandStringToVar(String input) {
        input = input.replace("'","");
        input = input.replace("write(","");
        input = input.replace("writeln(","");
        input = input.replace("readln(","");
        input = input.replace(")","");

        if(!model.varMap.containsKey(input)) {
            stringID++;
            stringDefenitions.put(input,"string" + stringID);
            return "\t " + "string" + stringID + " db '" + input + "', 0";
        }
        return null;
    }

    private String convertVar(String name, String type) throws Exception {
        switch (type) {
            case "integer":
                return "\t " + name + " dd ?";
            default:
                throw new Exception("Ошибка, Неопознанный тип данных " + type);
        }
    }

    private String[] convertCommand(String command) {
        ArrayList<String> result = new ArrayList<String>();
        if (command.contains("writeln(")) {
            command = command.replace("'","");
            command = command.replace("writeln(", "");
            command = command.replace(")", "");
            if (stringDefenitions.containsKey(command))
                result.add("\t push " + stringDefenitions.get(command));
            else
                result.add("\t push [" + command+"]");
            result.add("\t push dopStr");
            result.add("\t call [printf]");

        } else if (command.contains("write(")) {
            command = command.replace("'","");
            command = command.replace("write(", "");
            command = command.replace(")", "");
            if (stringDefenitions.containsKey(command))
                result.add("\t push " + stringDefenitions.get(command));
            else
                result.add("\t push [" + command+"]");
            result.add("\t call [printf]");
        } else if (command.contains("readln(")) {
            command = command.replace("readln(", "");
            command = command.replace(")", "");
            result.add("\t push " + command);
            result.add("\t push spaceStr");
            result.add("\t call [scanf]");
        } else {
            if (command.contains("+")) {
                result.add("\t mov ecx, [" + command.split("\\+")[0].split("=")[1].strip() + "]");
                result.add("\t add ecx, [" + command.split("\\+")[1].strip() + "]");
                result.add("\t mov ["+command.split(":")[0].strip()+"], ecx");
            } else if (command.contains("-")) {
                result.add("\t mov ecx, [" + command.split("-")[0].split("=")[1].strip() + "]");
                result.add("\t sub ecx, [" + command.split("-")[1].strip() + "]");
                result.add("\t mov ["+command.split(":")[0].strip()+"], ecx");
            } else if (command.contains("*")) {
                result.add("\t mov ecx, [" + command.split("\\*")[0].split("=")[1].strip() + "]");
                result.add("\t imul ecx, [" + command.split("\\*")[1].strip() +  "]");
                result.add("\t mov ["+command.split(":")[0].strip()+"], ecx");
            } else if (command.contains("/")) {
                result.add("\t mov eax, [" + command.split("/")[0].split("=")[1].strip() + "]");
                result.add("\t mov ecx, [" + command.split("/")[1].strip() +   "]");
                result.add("\t div ecx");
                result.add("\t mov ["+command.split(":")[0].strip()+"], eax");
            }
        }
        return result.toArray(new String[0]);
    }

    public String[] convert() throws Exception {
        ArrayList<String> result = new ArrayList<String>();
        result.addAll(Arrays.asList(getHeader()));
        for (Map.Entry<String, String> entry : model.varMap.entrySet()) {
            result.add(convertVar(entry.getKey(), entry.getValue()));
        }
        for (String command : model.commandsMap) {
            if (command.contains("write(") || command.contains("writeln(")) {
                String value = convertCommandStringToVar(command);
                if(value != null) {
                    result.add(value);
                }
            }
        }
        result.addAll(Arrays.asList(getCodeHeader()));
        for (String command : model.commandsMap) {
            result.addAll(Arrays.asList(convertCommand(command)));
            result.add("");
        }
        result.addAll(Arrays.asList(getCodeFooter()));
        return result.toArray(new String[0]);
    }
}
