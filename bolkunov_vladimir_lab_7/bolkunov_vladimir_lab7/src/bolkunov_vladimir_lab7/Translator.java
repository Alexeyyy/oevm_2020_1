package bolkunov_vladimir_lab7;

import java.io.*;
import java.util.ArrayList;

public class Translator {
    private final String defaultInputFilePath = "program.pas";
    private final String defaultOutputFilePath = "program.asm";

    private BufferedReader reader;
    private BufferedWriter writer;

    private boolean canExecute = true;

    public Translator(String inputFilePath, String outputFilePath) {
        try {
            reader = new BufferedReader(new FileReader(inputFilePath));
            writer = new BufferedWriter(new FileWriter(outputFilePath));
        } catch (Exception ex) {
            try {
                reader = new BufferedReader(new FileReader(defaultInputFilePath));
                writer = new BufferedWriter(new FileWriter(defaultOutputFilePath));
            } catch (Exception e) {
                System.out.println("Ошибка, файл не найден");
                canExecute = false;
            }
        }
    }

    private String[] read() throws IOException {
        ArrayList<String> inputStrings = new ArrayList<String>();
        String str = null;
        while ((str = reader.readLine()) != null){
            inputStrings.add(str);
        }
        return inputStrings.toArray(new String[0]);
    }

    private void write(String[] tokens) throws IOException {
        for (String token : tokens) {
            writer.write(token);
            writer.newLine();
        }
        writer.flush();
    }
    //ДОБАВИТЬ ПРОВЕРКУ СКОБОК
    public void translate() throws Exception {
        if (canExecute) {
            try {
                String[] input = read();
                ProgramModel model = new ProgramModel(input);
                if(!model.paranthesisCheck(input)){
                    throw new Exception("Код неверно отформатирован");
                }
                ConverterFASM converterFASM = new ConverterFASM(model);
                String[] convertedTokens = converterFASM.convert();
                write(convertedTokens);
            }catch (IOException e){
                System.out.println("Ошибка, ввода-вывода");
            }catch (Exception e){
                System.out.println("Ошибка, "+e.getMessage());
            }
        } else {
            System.out.println("Ошибка, файл не найден");
        }
    }


    protected void finalize() {
        try {
            reader.close();
            writer.close();
        }catch (Exception ex) {}
    }
}
