package com;

public class Converter {
    public static int decimalNum(char[] str1, int fss1){
        int znach = 0;
        int num = 0;
        for (int i = 0; i < str1.length; i++) {
            if(str1[i] >= '0' && str1[i] <= '9') {
                num = str1[i] - '0';
            }
            else if (str1[i] >= 'A' && str1[i] <= 'F'){
                num = str1[i] - 'A' + 10;
            }
            znach += num * Math.pow(fss1, str1.length - 1 - i);
        }
        return znach;
    }

    public static String convertTOFinalNumSys(char[] chislo, int fss, int fns) {
        int tenSS = decimalNum(chislo, fss);
        String rez = "";
        int ost;
        do {
            ost = tenSS % fns;
            tenSS = tenSS / fns;
            if (ost < 10)
                rez = ost + rez;
            else {
                rez = (char) (ost + 'A' - 10) + rez;
            }
        }
        while (tenSS / fns != 0);
        if(tenSS > 0 && tenSS < 10)
            rez = tenSS + rez;
        else if (tenSS > 10){
            rez = (char) (tenSS + 'A' - 10) + rez;
        }
        return rez;
    }
}