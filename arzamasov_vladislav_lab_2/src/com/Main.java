package com;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner scannerIss = new Scanner(System.in);
        System.out.println("Введите исходную систему счисления:");
        int fistSS = scannerIss.nextInt();

        Scanner scannerFss = new Scanner(System.in);
        System.out.println("Введите конечную систему счисления:");
        int finalSS = scannerFss.nextInt();

        System.out.println("Введите число в исходной системе счисления:");
        Scanner scanner = new Scanner(System.in);
        char[] str = scanner.nextLine().toCharArray();

        System.out.println("Число в конечной системе счисления:");
        System.out.print(Converter.convertTOFinalNumSys(str, fistSS, finalSS));
    }
}