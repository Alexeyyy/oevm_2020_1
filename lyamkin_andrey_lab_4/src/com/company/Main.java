package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

       int[][] truthTable = new int[16][5];

        while (true){
            System.out.println("Введите 1 для ДНФ, 2 для КНФ: ");
            String in = scanner.next();

            System.out.println("\nСлучайно заполненная таблица истинности:\n");
            fillTable(truthTable);
            printTable(truthTable);

            if(in.equals("1")){
                System.out.println("ДНФ: ");
                System.out.println(NormalForm.toDNF(truthTable));
            }else if(in.equals("2")){
                System.out.println("КНФ: ");
                System.out.println(NormalForm.toKNF(truthTable));
            }else{
                System.out.println("Сорре, неккоректный ввод.");
            }
        }

    }
    private static void printTable(int[][] table){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("| X1| X2| X3| X4| F |\n");
        for (int i = 0; i < table.length; i++) {
            stringBuilder.append("|");
            for (int j = 0; j < table[i].length; j++) {
                stringBuilder.append(" ").append(table[i][j]).append(" |");
            }
            stringBuilder.append(" \n");
        }
        System.out.print(stringBuilder);
    }
    private static void fillTable(int[][] table){

        int setValues = 0;
        for (int i = 0; i < table.length; i++) {
            char[] binSetValues = toBinaryNotation(setValues);
            for (int j = 0; j < table[i].length - 1; j++) {
                table[i][j] = binSetValues[j] - '0';
            }
            table[i][table[i].length-1] = (int)(Math.random()*2);
            setValues++;
        }
    }
    private static char[] toBinaryNotation(int num){

        StringBuilder sb = new StringBuilder();
        int remains = num;
        while (remains > 0){
            sb.append(remains%2);
            remains/=2;
        }
        while(sb.length()<4){
            sb.append("0");
        }
        return sb.reverse().toString().toCharArray();
    }
}
