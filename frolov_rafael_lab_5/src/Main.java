import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Main {

    private JFrame frame;
    private JButton btnGenerateRandom = new JButton("Random");
    private JButton btnDNF = new JButton("DNF");
    private JButton btnKNF = new JButton("KNF");
    private JTextArea txtFunction = new JTextArea();
    private MinimalizationBooleanFunction MBF = new MinimalizationBooleanFunction();
    private JPanel MyPanel = new MyPanel(MBF);

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Main() {
        initialize();
    }

    private void initialize() {

        frame = new JFrame();
        frame.setBounds(100, 100, 700, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        MyPanel.setBounds(0, 0, 170, 600);
        frame.getContentPane().add(MyPanel);

        txtFunction.setEditable(false);
        txtFunction.setBounds(490, 10, 200, 600);
        frame.getContentPane().add(txtFunction);

        btnGenerateRandom.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                MBF.randomFilling();
                btnDNF.setEnabled(true);
                btnKNF.setEnabled(true);
                txtFunction.setText("");
                MyPanel.repaint();
            }
        });
        btnGenerateRandom.setBounds(180, 10, 90, 30);
        frame.getContentPane().add(btnGenerateRandom);

        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = new String(MBF.creatureDNF());
                txtFunction.append(str);
                MyPanel.repaint();
                btnDNF.setEnabled(false);
            }
        });
        btnDNF.setBounds(280, 10, 90, 30);
        frame.getContentPane().add(btnDNF);

        btnKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = new String(MBF.creatureKNF());
                txtFunction.append(str);
                MyPanel.repaint();
                btnKNF.setEnabled(false);
            }
        });
        btnKNF.setBounds(380, 10, 90, 30);
        frame.getContentPane().add(btnKNF);
    }


}

