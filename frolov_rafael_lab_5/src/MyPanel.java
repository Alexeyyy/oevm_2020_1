import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
    MinimalizationBooleanFunction MBF;
    public MyPanel( MinimalizationBooleanFunction MBF) {
        this.MBF = MBF;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        MBF.draw(g);
    }
}
