### Лабораторная работа №5, выполнил Аюпов Артур из группы ИСЭбд-21

**Ссылка на видео:** https://drive.google.com/file/d/1G7gnqBbLrm5Le0PtMlUahzn7zUlkECvE/view?usp=sharing

### 1. Как запустить
Запустить лабу можно найди файл **Main.java** и запустив его в компиляторе. 

### 2. Какие технологии использовали
Среду разработки **IntelliJ IDEA Community** и несколько стандартных библиотек ***java***.

### 3. Что она делает
В этой лабораторной работе я визуализировал прошлую лабораторную работу. Строки, которые использовались в формуле, которую вы выбрали, подсвечиваются красным.

### 4. Примеры
#### Пример работы KNF формулы
https://drive.google.com/file/d/1IwY99q9oTuMlH7cDJxX6q0j4TmvrHA0Y/view?usp=sharing
#### Пример работы DNF формулы
https://drive.google.com/file/d/1_Gu6CN2VHvwF4-ILxK6cj1c3bRKoLrfY/view?usp=sharing
