package com.company;
import java.awt.event.*;
import javax.swing.*;

public class Main {

    static int N = new Variables().getPropertyValueInt("N");
    static int M = new Variables().getPropertyValueInt("M");
    HelperClass methods = new HelperClass();
    StringBuilder strBuilder = new StringBuilder();
    static int[][] truthTable = new int[N][M + 1];
    JFrame frame;
    JButton buttonCreate;
    JButton buttonDNF;
    JButton buttonKNF;
    JTextArea output = new JTextArea();
    public JPanel MyPanel = new MyPanel(truthTable);
    static boolean dnf = false;
    static boolean knf = false;

    public Main() {

        frame = new JFrame();
        frame.setSize(555, 666);
        frame.setTitle("Минимизация булевых функций");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.getContentPane().setLayout(null);
        output.setEditable(false);
        output.setBounds(222, 55, 150, 333);


        buttonCreate = new JButton("Создать");
        buttonCreate.setBounds(20, 10, 100, 30);
        buttonCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MyPanel.setBounds(0, 0, 170, 600);
                frame.getContentPane().add(MyPanel);
                buttonCreate.setVisible(false);
                buttonCreate.setEnabled(false);
                buttonDNF.setEnabled(true);
                buttonKNF.setEnabled(true);
                buttonDNF.setVisible(true);
                buttonKNF.setVisible(true);
                output.setVisible(true);
                MyPanel.repaint();
            }
        });
        frame.add(buttonCreate);

        buttonDNF = new JButton("ДНФ");
        buttonDNF.setBounds(222, 10, 70, 30);
        buttonDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                output.setText(strBuilder.append(methods.convertToDisjunctiveNormalForm(truthTable)).toString());
                buttonDNF.setEnabled(false);
                buttonKNF.setVisible(false);
                dnf = true;
                MyPanel.repaint();
            }
        });


        buttonKNF = new JButton("КНФ");
        buttonKNF.setBounds(300, 10, 70, 30);
        buttonKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                output.setText(strBuilder.append(methods.convertToConjunctiveNormalForm(truthTable)).toString());
                buttonDNF.setVisible(false);
                buttonKNF.setEnabled(false);
                knf = true;
                MyPanel.repaint();
            }
        });

        buttonDNF.setEnabled(false);
        buttonKNF.setEnabled(false);
        frame.add(buttonDNF);
        frame.add(buttonKNF);
        frame.add(output);
        buttonDNF.setVisible(false);
        buttonKNF.setVisible(false);
        output.setVisible(false);
    }

    public static void main(String[] args) {
        HelperClass.outputAndFillTable(truthTable);
        new Main();
    }
}