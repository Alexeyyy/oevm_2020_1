package com.company;

import java.awt.*;

public class OperationClass {
    private int[][] table;
    private int widthTable;
    private int heightTable;
    private char symbolForm;
    private boolean start = false;

    public OperationClass(int[][] table) {
        this.table = table;
        heightTable = table.length;
        widthTable = table[0].length - 1; // не трогаем последний столбец
    }

    public void filling() {

        for (int i = 0; i < heightTable; i++) {
            for (int j = 0; j < widthTable; j++) {
                table[i][j] = (int) (Math.random() * 2);
            }
        }
    }


    public String output() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < heightTable; i++) {
            for (int j = 0; j < widthTable; j++) {
                stringBuilder.append(table[i][j]);
                stringBuilder.append(" ");
            }
            stringBuilder.append('\n');
        }
        return new String(stringBuilder);
    }

    public void runForm(boolean variationForm) {
        start = true;
        if (variationForm) {
            symbolForm = '*';
            knfForm();
        } else {
            symbolForm = '+';
            dnfForm();
        }
    }

    private void knfForm() {
        boolean step;
        for (int i = 0; i < heightTable; i++) {
            step = true;
            for (int j = 0; j < widthTable; j++) {
                if (table[i][j] == 0) {
                    step = false;
                }
            }
            if (step) {
                table[i][widthTable] = 1;
            } else {
                table[i][widthTable] = 0;
            }
        }
    }

    private void dnfForm() {
        boolean step;
        for (int i = 0; i < heightTable; i++) {
            step = false;
            for (int j = 0; j < widthTable; j++) {
                if (table[i][j] == 1) {
                    step = true;
                }
            }
            if (step) {
                table[i][widthTable] = 1;
            } else {
                table[i][widthTable] = 0;
            }
        }
    }

    public void draw(Graphics g) {
        if (!start) {
            return;
        }

        if(symbolForm == '*') {
            g.setColor(getColor(0));
            g.fillRect(10,10,50,10);
            g.setColor(Color.BLACK);
            g.drawString("KNF", 25,20);
        }
        else {
            g.setColor(getColor(1));
            g.fillRect(10,10,50,10);
            g.setColor(Color.BLACK);
            g.drawString("DNF", 25,20);
        }


        widthTable++;
        char stepHelpChar;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < heightTable; i++) {
            stepHelpChar = symbolForm;
            for (int j = 0; j < widthTable; j++) {
                if (j == widthTable - 2) {
                    stepHelpChar = '=';
                } else if (j == widthTable - 1) {
                    stepHelpChar = ' ';
                }
                stringBuilder.append(table[i][j]);
                stringBuilder.append(" ");
                stringBuilder.append(stepHelpChar);
                stringBuilder.append(" ");

                g.setColor(getColor(table[i][j]));
                g.fillRect(20 * j + 15, 20 * i + 25, 15, 15);
            }
            g.setColor(Color.BLACK);
            g.drawString(stringBuilder.toString(), 20, 20 * i + 40);
            stringBuilder.delete(0, stringBuilder.length());
        }
        widthTable--;
        start = !start;
    }

    private Color getColor(int number) {
        if (number == 0) {
            return Color.GREEN;
        }
        return Color.YELLOW;
    }

    public boolean getStart() {
        return start;
    }

}
